package com.cpac.cpacvoip.delegate;

public interface OnAdapterClickListener {
    public void onClick(int position);
}
