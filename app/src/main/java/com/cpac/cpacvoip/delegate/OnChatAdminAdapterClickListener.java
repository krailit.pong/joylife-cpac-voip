package com.cpac.cpacvoip.delegate;

import com.quickblox.chat.model.QBChatMessage;

public interface OnChatAdminAdapterClickListener {
    public void onClick(int position, QBChatMessage message);
}
