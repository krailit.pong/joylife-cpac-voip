package com.cpac.cpacvoip.delegate;

public interface OnRoomChatListener {
    void sendAudioPath(String path);
}
