package com.cpac.cpacvoip.delegate;

import com.quickblox.chat.model.QBChatMessage;

public interface OnGroupChatAdapterClickListener {
    public void onClick(int position, QBChatMessage message);
}
