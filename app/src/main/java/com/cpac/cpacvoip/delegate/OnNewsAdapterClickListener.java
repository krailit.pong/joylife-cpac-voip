package com.cpac.cpacvoip.delegate;

public interface OnNewsAdapterClickListener {
    void onClick(int position, String url);
}
