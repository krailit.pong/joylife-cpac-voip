package com.cpac.cpacvoip.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.adapter.PageAdapter;
import com.cpac.cpacvoip.model.ChatRoomBus;
import com.cpac.cpacvoip.model.NotificationCountBus;
import com.cpac.cpacvoip.util.KAlert;
import com.cpac.cpacvoip.util.RxBus;
import com.rahimlis.badgedtablayout.BadgedTabLayout;

public class HomeFragment extends BaseFragment {
    
    private PageAdapter pageAdapter;
    private int selectedIndex = 0;
    private BadgedTabLayout tabLayout;
    private ViewPager viewpager;
    private Button btnSos;
    
    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    public HomeFragment() {
        // Required empty public constructor
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        return view;
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setup(view);
    }
    
    private void setup(View view) {
        btnSos = view.findViewById(R.id.btn_sos);
        tabLayout = view.findViewById(R.id.home_tablayout);
        viewpager = view.findViewById(R.id.home_viewpager);
        pageAdapter = new PageAdapter(getChildFragmentManager());
        viewpager.setAdapter(pageAdapter);
        tabLayout.setupWithViewPager(viewpager);
        tabLayout.addOnTabSelectedListener(onTabSelectedListener);
//        tabLayout.setTabTextColors(getResources().getColor(R.color.grey),
//                getResources().getColor(R.color.colorPrimary));
    
        btnSos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KAlert.showSos(requireContext());
            }
        });

        RxBus.getInstance().addObserver(NotificationCountBus.class, (o, arg) -> {
            if (arg != null && tabLayout != null ) {
                Activity activity = getActivity();
                if (isAdded() && activity != null) {
                    requireActivity().runOnUiThread(() -> {
                        tabLayout.setBadgeText(((NotificationCountBus) arg).getNotificationPage(),"N");
                    });
                }
            }
        });
//        tabLayout.setBadgeText(0,"N");
    }
    
    private TabLayout.OnTabSelectedListener onTabSelectedListener
            = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            tabLayout.setBadgeText(tab.getPosition(),null);
        }
    
        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            tabLayout.setBadgeText(tab.getPosition(),null);
        }
    
        @Override
        public void onTabReselected(TabLayout.Tab tab) {
        }
    };

}
