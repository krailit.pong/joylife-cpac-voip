package com.cpac.cpacvoip.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.cpac.cpacvoip.R;


public class ContactAdminFragment extends BaseFragment {
    
    private ImageButton btnBack;
    private ImageButton btnCall;
    
    public static ContactAdminFragment newInstance() {
        Bundle args = new Bundle();
        ContactAdminFragment fragment = new ContactAdminFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    
    public ContactAdminFragment() {
        // Required empty public constructor
    }
    
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_admin, container, false);
        return view;
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        btnBack = view.findViewById(R.id.btn_contact_admin_back);
        btnCall = view.findViewById(R.id.btn_contact_admin_call);
    
        btnBack.setOnClickListener(v -> getMainActivity().onBackPressed());
        btnCall.setOnClickListener(v -> getMainActivity().startVideoCall());
    }
    
}
