package com.cpac.cpacvoip.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.adapter.ImportantNoticeAdapter;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;
import com.github.barteksc.pdfviewer.PDFView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class ImportantNoticeFragment extends Fragment {
    
    private ArrayList<String> impModels = new ArrayList<>();
    private ImportantNoticeAdapter impAdapter;
    
    public static ImportantNoticeFragment newInstance() {
        Bundle args = new Bundle();
        ImportantNoticeFragment fragment = new ImportantNoticeFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    public ImportantNoticeFragment() {
        // Required empty public constructor
    }
    
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_important_notice, container, false);
        return view;
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        RecyclerView rvImp = view.findViewById(R.id.rv_imp);
//        impAdapter = new ImportantNoticeAdapter();
//        impAdapter.setOnAdapterClickListener(onAdapterClickListener);
//        rvImp.setAdapter(impAdapter);
//        rvImp.setLayoutManager(new LinearLayoutManager(getContext()));

        PDFView pdfView = view.findViewById(R.id.pdfView);
        pdfView.fromAsset("CPACsmart_talk_App_v2.pdf").load();
    }
    
    private OnAdapterClickListener onAdapterClickListener = new OnAdapterClickListener() {
        @Override
        public void onClick(int position) {
            // trigger....
        }
    };
}
