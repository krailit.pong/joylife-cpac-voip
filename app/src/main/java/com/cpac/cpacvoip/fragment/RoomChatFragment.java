package com.cpac.cpacvoip.fragment;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.activity.GroupChatActivity;
import com.cpac.cpacvoip.activity.MainActivity;
import com.cpac.cpacvoip.adapter.GroupChatAdapter;
import com.cpac.cpacvoip.adapter.GroupUserAdapter;
import com.cpac.cpacvoip.delegate.OnGroupChatAdapterClickListener;
import com.cpac.cpacvoip.delegate.OnRoomChatListener;
import com.cpac.cpacvoip.model.ChatRoomModel;
import com.cpac.cpacvoip.model.PlaylistModel;
import com.cpac.cpacvoip.util.AudioRecorderz;
import com.cpac.cpacvoip.util.Constant;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.ui.kit.chatmessage.adapter.media.recorder.AudioRecorder;
import com.quickblox.users.model.QBUser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class RoomChatFragment extends BaseFragment{
    
    private RecyclerView rvGroupChat;
    private RecyclerView rvPeopleChat;
    private GroupChatAdapter groupChatAdapter;
    private GroupUserAdapter groupUserAdapter;
    private ImageButton btnBack;
    private ImageButton btnContact;
    private TextView tvRoomTitle;
    private TextView tvProvince;
    private ChatRoomModel chatRoomModel;
    private Boolean isMain = false;
    private ImageView imvRoomMic;
    private AudioRecorderz audioRecorderz;
    private AudioRecorder.ConfigurationBuilder qbAudioRecorder;
    private Boolean isRecording = false;
    private OnRoomChatListener onRoomChatListener;
    private TextView tvRoomPeople;
    private MediaPlayer mPlayer;
    private ArrayList<PlaylistModel> mediaPlaylist = new ArrayList<>();
    private CountDownTimer recordCountdown;
    
    public static RoomChatFragment newInstance(Boolean isMain, ChatRoomModel model) {
        Bundle args = new Bundle();
        RoomChatFragment fragment = new RoomChatFragment();
        args.putBoolean("isMain", isMain);
        args.putSerializable(Constant.KEY_CHATROOM, model);
        fragment.setArguments(args);
        return fragment;
    }
    
    public RoomChatFragment() {
        // Required empty public constructor

    }
    
    public void setOnRoomChatListener(OnRoomChatListener onRoomChatListener) {
        this.onRoomChatListener = onRoomChatListener;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_room, container, false);
        isMain = getArguments().getBoolean("isMain");
        try {
            chatRoomModel = (ChatRoomModel) getArguments().getSerializable(Constant.KEY_CHATROOM);
        } catch (Exception ignored) {}
        return view;
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mPlayer = new MediaPlayer();
        audioRecorderz = new AudioRecorderz(requireContext());
        qbAudioRecorder = AudioRecorder.newBuilder();
        
        
        rvGroupChat = view.findViewById(R.id.rv_group_chat_room);
        rvPeopleChat = view.findViewById(R.id.rv_people_chat_room);
        tvRoomPeople = view.findViewById(R.id.tv_chat_room_people);
        tvRoomTitle = view.findViewById(R.id.tv_room_title);
        tvProvince = view.findViewById(R.id.tv_room_title3);
        imvRoomMic = view.findViewById(R.id.imv_room_mic);
        btnBack = view.findViewById(R.id.btn_room_back);
        btnContact = view.findViewById(R.id.btn_room_contact);
        
        btnBack.setOnClickListener(v -> getActivity().onBackPressed());
        btnContact.setOnClickListener(v -> {

            if (isMain)
                ((MainActivity) getActivity()).attachFragment(ContactAdminFragment.newInstance());
            else
                ((GroupChatActivity) getActivity()).showChooseUser(v);
        });
        
        imvRoomMic.setOnClickListener(v -> {
//                if(mPlayer != null && mPlayer.isPlaying()) {
//                    shortToast(getString(R.string.alert_mic_process));
//                    return;
//                }
            if(!isRecording) {
                recordCountdown = new CountDownTimer(Constant.MAX_SOUND_AUTO, 1000) {

                    public void onTick(long millisUntilFinished) {
                        Log.w("CPAC", "onTick: millis => " + millisUntilFinished);
                    }

                    public void onFinish() {
                        if (isRecording) {
                            startOrStopVoiceRecord();
                        }
                    }
                }.start();
            }
            startOrStopVoiceRecord();


        });
    
        if (chatRoomModel != null) {
            tvRoomTitle.setText(chatRoomModel.getRoomName());
//            tvProvince.setText(Prefz.extractDTO().getDepartmentName());
        }
        
        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
        groupChatAdapter = new GroupChatAdapter(requireActivity());
        rvGroupChat.setAdapter(groupChatAdapter);
        rvGroupChat.setLayoutManager(layoutManager);
    
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(requireContext());
        layoutManager2.setOrientation(LinearLayoutManager.HORIZONTAL);
        groupUserAdapter = new GroupUserAdapter();
        rvPeopleChat.setAdapter(groupUserAdapter);
        rvPeopleChat.setLayoutManager(layoutManager2);
        
        groupChatAdapter.setOnAdapterClickListener(new OnGroupChatAdapterClickListener() {
            @Override
            public void onClick(int position, QBChatMessage message) {
                if(mediaPlaylist.isEmpty()) {
                    
                    if(groupChatAdapter.getSelectedPlayMedia() == position) {
                        stopPlaying();
                    } else {
                        stopPlaying();
                        playMedia(position, message);
                    }
                }
            }
        });
    }
    
    @Override
    public void onDetach() {
        super.onDetach();
        try {
            audioRecorderz.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void setPeopleSize(int size) {
        tvRoomPeople.setText("กำลังออนไลน์ " + size + " คน");
        groupUserAdapter.setSizes(size);
    }
    
    public void addMessage(QBChatMessage message) {
        groupChatAdapter.addData(message);
        scrollMessageListDown();

        // Auto play the latest received
        Collection<QBAttachment> qbAttachments = message.getAttachments();
        if (qbAttachments.size() > 0 && !message.getSenderId().equals(QBChatService.getInstance().getUser().getId())) {
            playMedia(groupChatAdapter.getItemCount()-1, message);
        }
    }
    
    public void addMessages(ArrayList<QBChatMessage> messages) {

        groupChatAdapter.addDatas(messages);
        scrollMessageListDown();

        // Auto play the latest recorded
        if (messages.size() > 0) {
            QBChatMessage chatMessage = messages.get(messages.size()-1);
            if (!chatMessage.getBody().equals("")) {
                Collection<QBAttachment> qbAttachments = chatMessage.getAttachments();
                QBUser user = QBChatService.getInstance().getUser();
                if (user != null) {
                   if (qbAttachments.size() > 0
                           && !chatMessage.getSenderId().equals(user.getId()))
                   playMedia(messages.size()-1, chatMessage);
                }
            }
        }

    }
    
    public void addReconnectMessages(ArrayList<QBChatMessage> messages) {
        groupChatAdapter.addDatas(messages);
        scrollMessageListDown();
    }
    
    private void scrollMessageListDown() {
        rvGroupChat.scrollToPosition(groupChatAdapter.getItemCount() - 1);
    }
    
    private void startOrStopVoiceRecord() {
        try {
            if(isRecording) {
                if(recordCountdown != null) {
                    recordCountdown.cancel();
                }
                shortToast(getString(R.string.alert_stop_rec));
                audioRecorderz.stop();
                isRecording = false;
                imvRoomMic.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.colorPrimary));
                // TODO: Next Update.
                onRoomChatListener.sendAudioPath(audioRecorderz.getPath());
                runPlaylist();
                return;
            }
            shortToast(getString(R.string.alert_start_rec));
            isRecording = true;
            audioRecorderz.start();
            imvRoomMic.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.grey));
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.alert_mic_fail), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void shortToast(String message) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
    }
    
    
    public void playMedia(int position, QBChatMessage message) {
        if(groupChatAdapter.getSelectedPlayMedia() == position) {
            stopPlaying();
            return;
        } else if(isRecording || (mPlayer != null && mPlayer.isPlaying())) {
            mediaPlaylist.add(new PlaylistModel(message, position));
            return;
        }
        stopPlaying();
        if(message.getAttachments() == null && message.getAttachments().size() <= 0) {
            return;
        }
    
        Log.w("CPAC", "playMedia: play position " + position);
        groupChatAdapter.setSelectedPlayMedia(position);
        groupChatAdapter.notifyDataSetChanged();
    
        mPlayer = new MediaPlayer();
        try {
            QBAttachment attachment = message.getAttachments().iterator().next();
            //downloadFileFromId(attachment.getId());
            if (attachment.getUrl() != null) {
                mPlayer.setDataSource(attachment.getUrl());
            }else {
                mPlayer.setDataSource(Constant.QBPATH_URL + attachment.getId());
            }

            Log.d("mPlayer", "Group URL: " + attachment.getUrl());
            Log.d("mPlayer", "Group ID: " + Constant.QBPATH_URL + attachment.getId());

            mPlayer.prepareAsync();
            showProgressDialog();

            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    hideProgressDialog();
                    mPlayer.start();
                }
            });
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    hideProgressDialog();
                    removeQueuePlaylist(message.getId());
                    stopPlaying();
                    runPlaylist();
                }
            });
        } catch (Exception e) {
            Log.e("mPlayer", "prepare() failed");
            hideProgressDialog();
        }
        
    }
    
    private void stopPlaying() {
        if (mPlayer != null) {
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
    
            groupChatAdapter.setSelectedPlayMedia(-1);
            groupChatAdapter.notifyDataSetChanged();
        }
    }
    
    private void removeQueuePlaylist(String msgId) {
        if(!mediaPlaylist.isEmpty()) {
            String playingId = mediaPlaylist.get(0).message.getId();
            if (playingId.equals(msgId)) {
                mediaPlaylist.remove(0);
            }
        }
    }
    
    private void runPlaylist() {
        if(!mediaPlaylist.isEmpty()) {
            new Handler().postDelayed(() -> {
                playMedia( mediaPlaylist.get(0).position, mediaPlaylist.get(0).message);
            }, 500);
            
        }
    }
}
