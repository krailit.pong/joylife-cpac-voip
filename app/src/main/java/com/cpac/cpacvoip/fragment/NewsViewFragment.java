package com.cpac.cpacvoip.fragment;


import android.app.AlertDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bumptech.glide.Glide;
import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.adapter.NewsAdapter;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;
import com.cpac.cpacvoip.delegate.OnNewsAdapterClickListener;
import com.cpac.cpacvoip.model.NewsBus;
import com.cpac.cpacvoip.model.NewsModel;
import com.cpac.cpacvoip.util.Constant;
import com.cpac.cpacvoip.util.Prefz;
import com.cpac.cpacvoip.util.RxBus;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.ArrayList;

public class NewsViewFragment extends BaseFragment {
    
    private ArrayList<NewsModel> newsModels = new ArrayList<>();
    private NewsAdapter newsAdapter;
    private SwipeRefreshLayout swipeRefresh;
    private FrameLayout fl_notice_news;


    public static NewsViewFragment newInstance() {
        Bundle args = new Bundle();
        NewsViewFragment fragment = new NewsViewFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    public NewsViewFragment() {
        // Required empty public constructor
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_news, container, false);
        return view;
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        fl_notice_news = view.findViewById(R.id.fl_notice_news);
        swipeRefresh = view.findViewById(R.id.swipe_refresh_news);
        RecyclerView rvNews = view.findViewById(R.id.rv_news);
        newsAdapter = new NewsAdapter(requireContext());
        newsAdapter.setOnAdapterClickListener(onAdapterClickListener);
        rvNews.setAdapter(newsAdapter);
        rvNews.setLayoutManager(new LinearLayoutManager(getContext()));

//        getMainActivity().getHomeViewModel()
//                .getNewsModelLive()
//                .observe(this, newsModelz -> {
//                    newsModels = newsModelz;
//                    newsAdapter.setDataList(newsModels);
//                });

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.setRefreshing(false);
            getMainActivity().getHomeViewModel().requestNews(Prefz.getDriverNo());
        });
        
        RxBus.getInstance().addObserver(NewsBus.class ,(o, arg) -> {
            newsModels = ((NewsBus) arg).getDataList();
            if (newsModels != null) {
                if (newsModels.size() > 0) {
                    fl_notice_news.setVisibility(View.GONE);
                    newsAdapter.setDataList(newsModels);
                }else {

                    fl_notice_news.setVisibility(View.VISIBLE);
                }
            }else {
                fl_notice_news.setVisibility(View.VISIBLE);
            }
        });
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private OnNewsAdapterClickListener onAdapterClickListener = new OnNewsAdapterClickListener() {
        @Override
        public void onClick(int position, String url) {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
            View mView = getLayoutInflater().inflate(R.layout.photo_view, null);
            PhotoView photoView = mView.findViewById(R.id.photoViewer);
            mBuilder.setView(mView);
            AlertDialog mDialog = mBuilder.create();
            mDialog.show();

            Log.d("CPAC", "url = " + url);

            Glide.with(getContext())
                    .load(url)
                    .placeholder(R.drawable.empty)
                    .into(photoView);
        }
    };
}
