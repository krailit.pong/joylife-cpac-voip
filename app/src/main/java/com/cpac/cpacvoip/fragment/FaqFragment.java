package com.cpac.cpacvoip.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.adapter.FaqAdapter;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;
import com.cpac.cpacvoip.model.FaqModel;
import com.github.barteksc.pdfviewer.PDFView;

import java.util.ArrayList;

public class FaqFragment extends Fragment {
    
    private ArrayList<FaqModel> faqModels = new ArrayList<>();
    private FaqAdapter faqAdapter;
    
    
    public static FaqFragment newInstance() {
        Bundle args = new Bundle();
        FaqFragment fragment = new FaqFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    public FaqFragment() {
        // Required empty public constructor
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
        return view;
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        RecyclerView rvFaq = view.findViewById(R.id.rv_faq);
//        faqAdapter = new FaqAdapter();
//        faqAdapter.setOnAdapterClickListener(onAdapterClickListener);
//        rvFaq.setAdapter(faqAdapter);
//        rvFaq.setLayoutManager(new LinearLayoutManager(getContext()));
//
//        // Mockupz Data...
//        faqModels.add(new FaqModel());
//        faqModels.add(new FaqModel());
//        faqModels.add(new FaqModel());
//        faqModels.add(new FaqModel());
//        faqAdapter.setDataList(faqModels);

        PDFView pdfView = view.findViewById(R.id.pdfView);
        pdfView.fromAsset("CPACsmarttalk_App_FAQ1.pdf").load();
        
    }
    
    private OnAdapterClickListener onAdapterClickListener = new OnAdapterClickListener() {
        @Override
        public void onClick(int position) {
            // trigger....
        }
    };
}
