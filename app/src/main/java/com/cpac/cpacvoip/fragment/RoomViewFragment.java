package com.cpac.cpacvoip.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.activity.GroupChatActivity;
import com.cpac.cpacvoip.adapter.RoomAdapter;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;
import com.cpac.cpacvoip.model.ChatRoomBus;
import com.cpac.cpacvoip.model.ChatRoomModel;
import com.cpac.cpacvoip.model.ChatRoomUpdateBus;
import com.cpac.cpacvoip.model.LoginChatRoomBus;
import com.cpac.cpacvoip.model.LoginChatRoomModel;
import com.cpac.cpacvoip.model.OrgChartChainDto;
import com.cpac.cpacvoip.qb.utils.SharedPrefsHelper;
import com.cpac.cpacvoip.services.APIHelper;
import com.cpac.cpacvoip.util.Prefz;
import com.cpac.cpacvoip.util.RxBus;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.listeners.QBRosterListener;
import com.quickblox.chat.listeners.QBSubscriptionListener;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.util.ArrayList;
import java.util.Collection;

import static com.cpac.cpacvoip.util.Constant.KEY_LOGIN_TIME;

public class RoomViewFragment extends BaseFragment implements QBSubscriptionListener, QBRosterListener {

    private static String TAG = "RoomView";
    private ArrayList<ChatRoomModel> chatRoomModels = new ArrayList<>();
    private RoomAdapter roomAdapter;
    private FirebaseAnalytics mFirebaseAnalytics;
    private QBRoster chatRoster;

    public static RoomViewFragment newInstance() {
        Bundle args = new Bundle();
        RoomViewFragment fragment = new RoomViewFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    public RoomViewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_room, container, false);
        return view;
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView rvRoom = view.findViewById(R.id.rv_room_fragment);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());

        roomAdapter = new RoomAdapter();
        roomAdapter.setOnAdapterClickListener(onAdapterClickListener);
        rvRoom.setAdapter(roomAdapter);
        rvRoom.setLayoutManager(new LinearLayoutManager(getContext()));

//        getMainActivity().getHomeViewModel()
//                .getChatRoomModelLive()
//                .observe(this, chatRoomModelz -> {
//                    chatRoomModels = chatRoomModelz;
//                    roomAdapter.setDataList(chatRoomModels);
//                });


        chatRoster = QBChatService.getInstance().getRoster();
        if (chatRoster != null) {
            chatRoster.addSubscriptionListener(this);
        }


        RxBus.getInstance().addObserver(ChatRoomBus.class, (o, arg) -> {
//            chatRoomModels = model;
            chatRoomModels = ((ChatRoomBus) arg).getDataList();
            roomAdapter.setDataList(chatRoomModels);

            for (ChatRoomModel model : chatRoomModels) {
                if (model == null) {
                    continue;
                }

                if (!model.getQbAdminId().equals("")
                        && model.getQbAdminId() != null
                        && chatRoster != null) {
                    friendManagement(model.getQbAdminId());
                }
            }
        });

        RxBus.getInstance().addObserver(LoginChatRoomBus.class, (o, arg) -> {
            chatRoomModels.clear();

            ArrayList<LoginChatRoomModel> loginChatRoomModels = ((LoginChatRoomBus) arg).getDataList();
            Log.w(TAG, "LoginChatRoomBus addObserver: " + loginChatRoomModels.size());

            for (LoginChatRoomModel model : loginChatRoomModels) {
                if (model == null) {
                    continue;
                }

//                Log.i("CPAC", model.getRoomName() + " check room_id = " + !model.getRoomid().equals(""));
                if (!model.getUrserid().equals("") && !model.getRoomid().equals("")) {
//                    Log.i(TAG, "check room_id PASSES");
                    ChatRoomModel roomModel = new ChatRoomModel();
                    roomModel.setRoomid(model.getRoomid());
                    roomModel.setRoomName(model.getRoomName());
                    roomModel.setUserId(model.getUrserid());

//                    if (!model.getQbAdminId().equals("")
//                            && model.getQbAdminId() != null
//                            && chatRoster != null) {
//                        friendManagement(model.getQbAdminId());
//                    }

                    chatRoomModels.add(roomModel);
                }
            }

            Log.i("CPAC", "roomAdapter setDataList From Observer.");
            roomAdapter.setDataList(chatRoomModels);
        });
    
        RxBus.getInstance().addObserver(ChatRoomUpdateBus.class, (o, arg) -> {
            String dialogId = ((ChatRoomUpdateBus) arg).getDialogId();
            try {
                for (ChatRoomModel model: chatRoomModels) {
                    if(dialogId.equals(model.getRoomid())) {
                        model.setHasNewMessage(true);
                    }
                }
                Prefz.saveDialogUnread(chatRoomModels);
                if(roomAdapter != null) {
                    requireActivity().runOnUiThread(() -> {
                        roomAdapter.notifyDataSetChanged();
                    });
                }
            } catch(Exception e) {
//                e.printStackTrace();
                Crashlytics.logException(e);
            }
        });






        this.callLoginToGetRoomList();
    }

    private void friendManagement(String adminId) {
        Log.i(TAG, "friendManagement " + adminId);
        try {
            int qbAdminId = Integer.parseInt(adminId);
            if (!chatRoster.contains(qbAdminId)) {
                if (!adminId.equals("")) {
                    Log.d(TAG,"friendManagement createEntry = " + qbAdminId);
                    chatRoster.createEntry(qbAdminId, null);
                }

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, Prefz.getDriverNo());
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, Prefz.extractDTO().getDriverName());
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "friend_request");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
            }else {
                if (!adminId.equals("")) {
                    Log.d(TAG,"friendManagement confirmSubscription = " + qbAdminId);
                    chatRoster.confirmSubscription(qbAdminId);
                }
            }
        } catch (SmackException.NotConnectedException | SmackException.NotLoggedInException | XMPPException | SmackException.NoResponseException e) {
            Crashlytics.logException(e);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume RoomViewFragment");

        chatRoster = QBChatService.getInstance().getRoster();
        if (chatRoster != null) {
            chatRoster.addSubscriptionListener(this);
        }

        new Handler().postDelayed(this::reloadChatList, 2000);
    }

    public void reloadChatList() {
        if (Prefz.isSameDay()) {
            if (roomAdapter != null) {
                boolean bUserEmpty = SharedPrefsHelper.getInstance().getQbUser() != null;
                Log.i(TAG, "onResume roomAdapter count "  + roomAdapter.getItemCount());
                Log.i(TAG, "onResume SharedPrefsHelper.getInstance().getQbUser() != null "  + bUserEmpty);

                if (bUserEmpty && roomAdapter.getItemCount() < 4) {
                    QBUser u = SharedPrefsHelper.getInstance().getQbUser();
//                    Log.i("CPAC", "onResume u.getLogin() = " + u.getLogin());
//                    Log.i("CPAC", "onResume getDriverNo = " + "driver" + Prefz.extractDTO().getDriverNo());
//                    Log.i("CPAC", "onResume check roomAdapter");
                    if (u.getLogin().equals("driver" + Prefz.extractDTO().getDriverNo()) && roomAdapter.getItemCount() < 4) {
                        String dateFromPrefs = SharedPrefsHelper.getInstance().get(KEY_LOGIN_TIME + "_" + Prefz.extractDTO().getDriverNo());
                        if (dateFromPrefs != null) {
                            Log.i(TAG, "onResume found date = " + dateFromPrefs);
                            this.callLoginToGetRoomList();
                        }
                    }
                }
            }else {
                Log.i(TAG, "onResume Do Nothing");
            }
        }
    }

    private void callLoginToGetRoomList() {
        OrgChartChainDto dtoModel = Prefz.extractDTO();
        if (dtoModel != null) {
            Log.i(TAG, "callLoginToGetRoomList");
            dtoModel.setFbToken(Prefz.getFBToken());
            APIHelper.apiLoginToGetRoomList(dtoModel);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private OnAdapterClickListener onAdapterClickListener = new OnAdapterClickListener() {
        @Override
        public void onClick(int position) {
            //
            ChatRoomModel model = chatRoomModels.get(position);
            if (model.getUserId() != null && !model.getRoomid().equals("")) {
                GroupChatActivity.start(getContext(), model);

                model.setHasNewMessage(false);
                roomAdapter.notifyDataSetChanged();
            }else {
//                KAlert.show(getContext(),"" , "ไม่สามารถเข้าร่วมห้องได้", null);
            }

            Prefz.saveDialogUnread(chatRoomModels);
        }
    };

    @Override
    public void subscriptionRequested(int i) {
        if (chatRoster != null) {
            try {
                Log.d(TAG,"Listener confirmSubscription = " + i);
                chatRoster.confirmSubscription(i);
            } catch (SmackException.NotConnectedException e) {
                Crashlytics.logException(e);
            } catch (SmackException.NotLoggedInException e) {
                Crashlytics.logException(e);
            } catch (XMPPException e) {
                Crashlytics.logException(e);
            } catch (SmackException.NoResponseException e) {
                Crashlytics.logException(e);
            }
        }
    }

    @Override
    public void entriesDeleted(Collection<Integer> collection) {
        Log.d(TAG, "entriesDeleted = " + collection);
    }

    @Override
    public void entriesAdded(Collection<Integer> collection) {
        Log.d(TAG, "entriesAdded = " + collection);

    }

    @Override
    public void entriesUpdated(Collection<Integer> collection) {
        Log.d(TAG, "entriesUpdated = " + collection);
    }

    @Override
    public void presenceChanged(QBPresence qbPresence) {
        Log.d(TAG, "presenceChanged = " + qbPresence.getStatus());
    }
}
