package com.cpac.cpacvoip.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cpac.cpacvoip.R;

public class IncomingCallFragment extends Fragment {
    
    public static IncomingCallFragment newInstance() {
        Bundle args = new Bundle();
        IncomingCallFragment fragment = new IncomingCallFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    public IncomingCallFragment() {
        // Required empty public constructor
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_incoming_call, container, false);
        return view;
    }
    
    
    
}
