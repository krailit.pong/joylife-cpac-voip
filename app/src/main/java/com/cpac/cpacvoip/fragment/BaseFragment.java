package com.cpac.cpacvoip.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.activity.MainActivity;

public class BaseFragment extends Fragment {
    
    private ProgressDialog progressDialog;
    
    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }
    
    public void showProgressDialog() {
        showProgressDialog(R.string.dlg_loading);
        new Handler().postDelayed(this::hideProgressDialog, 20000);
    }

    public void showProgressDialog(long mockTime) {
        showProgressDialog(R.string.dlg_loading);
        new Handler().postDelayed(this::hideProgressDialog, mockTime);
    }

    @Override
    public void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    public void showProgressDialog(@StringRes int messageId) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(requireContext());
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            
            // Disable the back button
            DialogInterface.OnKeyListener keyListener = (dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK;
            progressDialog.setOnKeyListener(keyListener);
        }
        
        progressDialog.setMessage(getString(messageId));
        
        progressDialog.show();
        
    }
    
    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
//                && !this.getMainActivity().isFinishing()) {
            progressDialog.dismiss();
        }
    }
}
