package com.cpac.cpacvoip.fragment;


import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.adapter.NoticeAdapter;
import com.cpac.cpacvoip.adapter.NoticeCollectionAdapter;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;
import com.cpac.cpacvoip.model.BroadcastBus;
import com.cpac.cpacvoip.model.BroadcastModel;
import com.cpac.cpacvoip.model.RequestBroadcastPlay;
import com.cpac.cpacvoip.services.APIHelper;
import com.cpac.cpacvoip.util.DateExt;
import com.cpac.cpacvoip.util.Prefz;
import com.cpac.cpacvoip.util.RxBus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.cpac.cpacvoip.util.Constant.URL_MEDIA;


public class NoticeViewFragment extends BaseFragment {
    
    private NoticeAdapter noticeAdapter;
    private NoticeCollectionAdapter noticeCollAdapter;
    private FrameLayout flNoDataToday;
    private FrameLayout flNoticeOld;
    private TextView tvNoticeTodayTitle;
    private RecyclerView rvNoticeList;
    private RecyclerView rvNoticeColl;
    private SwipeRefreshLayout swipeRefresh;
    private ArrayList<BroadcastModel> broadcastModels = new ArrayList<>();
    private MediaPlayer mPlayer;
    private int selectedPlayMedia = -1;
    
    public static NoticeViewFragment newInstance() {
        Bundle args = new Bundle();
        NoticeViewFragment fragment = new NoticeViewFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    public NoticeViewFragment() {
        // Required empty public constructor
    }
    
    
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_view_notice, container, false);
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        flNoDataToday = view.findViewById(R.id.fl_notice_not_today);
        flNoticeOld = view.findViewById(R.id.fl_notice_old);
        tvNoticeTodayTitle = view.findViewById(R.id.tv_notice_today_title);
        swipeRefresh = view.findViewById(R.id.swipe_refresh_notice);
        rvNoticeColl = view.findViewById(R.id.rv_notice_coll);
        rvNoticeList = view.findViewById(R.id.rv_notice_list);
        
        // List...
        noticeAdapter = new NoticeAdapter(requireContext());
        //noticeAdapter.setDataList(broadcastModels);
        noticeAdapter.setOnAdapterClickListener(onAdapterClickListener);
        rvNoticeList.setAdapter(noticeAdapter);
        rvNoticeList.setLayoutManager(new LinearLayoutManager(getContext()));
    
        // Collection...
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        noticeCollAdapter = new NoticeCollectionAdapter(requireContext());
        //noticeCollAdapter.setDataList(broadcastModels);
        noticeCollAdapter.setOnAdapterClickListener(onCollClickListener);
        rvNoticeColl.setAdapter(noticeCollAdapter);
        rvNoticeColl.setLayoutManager(gridLayoutManager);
        
        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.setRefreshing(false);
            getMainActivity().getHomeViewModel().requestBroadcast(Prefz.getDriverNo());
        });

        RxBus.getInstance().addObserver(BroadcastBus.class, (o, arg) -> {
            broadcastModels = ((BroadcastBus) arg).getDataList();
            if (broadcastModels != null) {
                if (!broadcastModels.isEmpty()) {
                    Prefz.checkBroadcastPlay(broadcastModels);
                    List<BroadcastModel> listToday = filterToday();
                    if (listToday != null) {
                        checkNoBroadcastToday(listToday.size());
                    }

                    List<BroadcastModel> listBeforeToday = filterBeforeToday();
                    if (listBeforeToday != null) {
                        checkNoBroadcastOld(listBeforeToday.size());
                    }

                    noticeAdapter.setDataList(listBeforeToday);
                    noticeCollAdapter.setDataList(listToday);
                    Activity activity = getActivity();
                    if (noticeCollAdapter != null && activity != null) {
                        activity.runOnUiThread(() -> {
                            assert listToday != null;
                            if(listToday.size() > 0) {
                                prepareMediaToday(listToday);
                            }
                        });

//                        new Handler().postDelayed(() -> {
//                            if(listToday.size() > 0) {
//                                prepareMediaToday(listToday);
//                            }
//                        }, 500);
                    }
                }else {
                    checkNoBroadcastToday(0);
                    checkNoBroadcastOld(0);
                }
            }else {
                checkNoBroadcastToday(0);
                checkNoBroadcastOld(0);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private OnAdapterClickListener onAdapterClickListener = new OnAdapterClickListener() {
        @Override
        public void onClick(int position) {
            //
            prepareMediaPosition(position, false);
        }
    };
    private OnAdapterClickListener onCollClickListener = new OnAdapterClickListener() {
        @Override
        public void onClick(int position) {
            //
            prepareMediaPosition(position, true);
        }
    };
    
    private List<BroadcastModel> filterToday() {
        ArrayList<BroadcastModel> list = new ArrayList<>();
        for(BroadcastModel model : broadcastModels) {
            Date fromModel = DateExt.fromString(model.getLogtime());
            if(fromModel.after(DateExt.getToday())) {
                list.add(model);
            }
        }
        checkNoBroadcastToday(list.size());
        
        return list;
    }
    
    private List<BroadcastModel> filterBeforeToday() {
        ArrayList<BroadcastModel> list = new ArrayList<>();
        for(BroadcastModel model : broadcastModels) {
            Date fromModel = DateExt.fromString(model.getLogtime());
            if(fromModel.before(DateExt.getToday())) {
                list.add(model);
            }
        }
        return list;
    }
    
    private void checkNoBroadcastToday(int size) {
        if(size <= 0) {
            /// show layout
            rvNoticeColl.setVisibility(View.GONE);
            flNoDataToday.setVisibility(View.VISIBLE);
        } else {
            rvNoticeColl.setVisibility(View.VISIBLE);
            flNoDataToday.setVisibility(View.GONE);
        }
    }


    private void checkNoBroadcastOld(int size) {
        if(size <= 0) {
            /// show layout
            rvNoticeList.setVisibility(View.GONE);
            flNoticeOld.setVisibility(View.VISIBLE);
        } else {
            rvNoticeList.setVisibility(View.VISIBLE);
            flNoticeOld.setVisibility(View.GONE);
        }
    }

    public void prepareMediaToday(List<BroadcastModel> listToday) {
//        stopPlaying();
        int position = 0;
        BroadcastModel model = listToday.get(position);
        if (!model.getIsPlay()) {
//            Log.i("MediaPlayer", "prepareMediaToday isPlay() = " + model.getIsPlay());
            setPlay(model);
            noticeCollAdapter.setOnPlayAudio(position);
            selectedPlayMedia = position;
            startMediaPlayer(model);
        }else {
            noticeCollAdapter.setOnPlayAudio(-1);
            selectedPlayMedia = -1;
        }
    }
    
    public void prepareMediaPosition(int position, Boolean isToday) {
        stopPlaying();
        List<BroadcastModel> listToday = filterToday();
        List<BroadcastModel> listBeforeToday = filterBeforeToday();
        BroadcastModel model;
        if(isToday) {
            model = listToday.get(position);
        } else {
            model = listBeforeToday.get(position);
        }
        if(selectedPlayMedia == position) {
            setPlay(model);
            
            if(isToday) noticeCollAdapter.setOnPlayAudio(-1);
            else noticeAdapter.setOnPlayAudio(-1);
            selectedPlayMedia = -1;
            return;
        }

        if(isToday) noticeCollAdapter.setOnPlayAudio(position);
        else noticeAdapter.setOnPlayAudio(position);
        
        selectedPlayMedia = position;
        startMediaPlayer(model);
    }

    private void startMediaPlayer(BroadcastModel model) {
        mPlayer = new MediaPlayer();
        showProgressDialog();
        String urlString = URL_MEDIA + model.getBroadcastFilename();
        try {
            mPlayer.setDataSource(urlString);
            Log.d("MediaPlayer", "Prepare player: " + urlString);
            mPlayer.setOnPreparedListener(mp -> {
                hideProgressDialog();
                Log.d("MediaPlayer", "Start Play ");
//                new Handler().postDelayed(() -> {
                if (mp != null)
                    if (!mp.isPlaying())
                        mp.start();
//                }, 1000);

            });

            mPlayer.setOnCompletionListener(mp -> {
                Log.d("MediaPlayer", "Play Complete");
                setPlay(model);
                stopPlaying();
                selectedPlayMedia = -1;
                noticeCollAdapter.setOnPlayAudio(-1);
                noticeAdapter.setOnPlayAudio(-1);

                APIHelper.PostPlayBroadcast(new RequestBroadcastPlay(Prefz.getDriverNo(), model.getBroadcastNo()));
            });

            mPlayer.setOnErrorListener((mediaPlayer, i, i1) -> {
                Log.d("MediaPlayer", "Play Error " + i + "," + i1);
                stopPlaying();
                startMediaPlayer(model);
                return false;
            });

            mPlayer.prepareAsync();
        } catch (Exception e) {
            Log.e("MediaPlayer", "error " + e.getLocalizedMessage());
            hideProgressDialog();
        }
    }
    
    private void stopPlaying() {
        try {
            if (mPlayer != null) {
                mPlayer.stop();
                mPlayer.reset();
                mPlayer.release();
                mPlayer = null;
            }
        } catch(Exception e) {
            hideProgressDialog();
            e.printStackTrace();
        }
    }
    
    private void setPlay(BroadcastModel model) {
        model.setPlay(true);
        Prefz.saveBroadcastPlay(broadcastModels);
    }
    
}
