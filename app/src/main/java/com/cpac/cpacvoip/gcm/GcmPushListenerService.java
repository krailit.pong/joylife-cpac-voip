package com.cpac.cpacvoip.gcm;

import android.os.Bundle;
import android.util.Log;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.activity.MainActivity;
import com.cpac.cpacvoip.util.NotificationUtils;
import com.quickblox.messages.services.gcm.QBGcmPushListenerService;

public class GcmPushListenerService extends QBGcmPushListenerService {
    @Override
    public void sendPushMessage(Bundle data, String from, String message) {
        super.sendPushMessage(data, from, message);
        Log.v("CPAC", "From: " + from);
        Log.v("CPAC", "Message: " + message);
    
        NotificationUtils.showNotification(this, MainActivity.class,
                "CPAC", message, R.drawable.ic_notifications, 1);
    }
}
