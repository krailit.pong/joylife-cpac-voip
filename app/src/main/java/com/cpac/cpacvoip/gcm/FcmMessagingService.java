package com.cpac.cpacvoip.gcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.activity.MainActivity;
import com.cpac.cpacvoip.model.BroadcastBus;
import com.cpac.cpacvoip.model.BroadcastModel;
import com.cpac.cpacvoip.model.ChatRoomUpdateBus;
import com.cpac.cpacvoip.model.NewsBus;
import com.cpac.cpacvoip.model.NewsModel;
import com.cpac.cpacvoip.model.NotificationCountBus;
import com.cpac.cpacvoip.qb.activity.CallActivity;
import com.cpac.cpacvoip.qb.services.CallService;
import com.cpac.cpacvoip.qb.utils.Consts;
import com.cpac.cpacvoip.qb.utils.SharedPrefsHelper;
import com.cpac.cpacvoip.services.APIHelper;
import com.cpac.cpacvoip.util.Prefz;
import com.cpac.cpacvoip.util.RxBus;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.Map;

import rx.Subscriber;

import static com.cpac.cpacvoip.util.Constant.KEY_RESET;

public class FcmMessagingService extends FirebaseMessagingService {
    
    private static final String TAG = "Notification";
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        requestBroadcast();
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]
        
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "Notification From: " + remoteMessage.getFrom());
        
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Notification data: " + remoteMessage.getData());
            Map<String, String> mapz = remoteMessage.getData();

            if (mapz.get("message_id") != null) {
                // For Chat Tab
                RxBus.getInstance().post(new ChatRoomUpdateBus(mapz.get("dialog_id")));
                sendNotificationChat(getString(R.string.noti_new_chat), mapz.get("message"));
                RxBus.getInstance().post(new NotificationCountBus(1));
            }else if (mapz.get("message") != null) {
                // For Broadcast and News tab
                String message = mapz.get("message");
                assert message != null;
                if (message.equals("new_news")) {
                    String title = mapz.get("title");
                    sendNotification(getString(R.string.noti_news), title);
                    requestNews();
                    RxBus.getInstance().post(new NotificationCountBus(2));
                }

                if (message.equals("new_broadcast")) {
                    String title = mapz.get("title");
                    sendNotification(getString(R.string.noti_broadcast), title);
                    requestBroadcast();
                    RxBus.getInstance().post(new NotificationCountBus(0));
                }

                if (message.equals("test_push")) {
                    sendNotification(getString(R.string.noti_broadcast), "-");
                    requestBroadcast();
                    RxBus.getInstance().post(new NotificationCountBus(0));
                }

                if (message.contains("is calling you")) {
                    if (Prefz.getDriverNo() != null) {
                        Log.i("CPAC", "Notification has driverNo " + Prefz.getDriverNo());

                        QBUser qbUser = SharedPrefsHelper.getInstance().getQbUser();
                        if (qbUser != null) {
                            Intent tempIntent = new Intent(this, CallService.class);
                            PendingIntent pendingIntent = PendingIntent.getService(this, 0, tempIntent, 0);
                            CallService.start(this, qbUser, pendingIntent);
//                            CallActivity.start(this, true);
                        }

//                        sendBroadcastToMainAcitivity(this);
                    }
                }
            }
        }
    }

    private void sendBroadcastToMainAcitivity(Context context) {
        Intent intetReset = new Intent(KEY_RESET);
        context.sendBroadcast(intetReset);
    }

    // [END receive_message]
    
    
    // [START on_new_token]
    
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
        
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
        Prefz.saveFBToken(token);
    }
    // [END on_new_token]
    
    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        // [START dispatch_job]
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(MyJobService.class)
//                .setTag("my-job-tag")
//                .build();
//        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }
    
    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }
    
    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }
    
    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String title, String messageBody) {
        if (title == null) {
            title = "CPAC";
        }
        if (messageBody == null) {
            messageBody = "Message";
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
            PendingIntent.FLAG_NO_CREATE);
//                PendingIntent.FLAG_ONE_SHOT);
        
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_notifications)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setDefaults(NotificationCompat.DEFAULT_ALL);
        
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setPriority(NotificationManager.IMPORTANCE_HIGH);
            NotificationChannel channel = new NotificationChannel(channelId,
                    "แจ้งเตือนสำหรับประกาศ/ข่าวสาร",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setShowBadge(true);
            notificationManager.createNotificationChannel(channel);
        }
        
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void sendNotificationChat(String title, String messageBody) {
        if (title == null) {
            title = "CPAC";
        }
        if (messageBody == null) {
            messageBody = "Message";
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        String channelId = getString(R.string.chat_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_notifications)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setDefaults(NotificationCompat.DEFAULT_ALL);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setPriority(NotificationManager.IMPORTANCE_HIGH);
            NotificationChannel channel = new NotificationChannel(channelId,
                    "แจ้งเตือนไปยังห้องสนทนา",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setShowBadge(true);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
    
    private void requestBroadcast() {
        APIHelper.GetBroadcast(Prefz.getDriverNo())
                //.doOnTerminate(() -> context.hideProgressDialog())
                .subscribe(new Subscriber<ArrayList<BroadcastModel>>() {
                    @Override
                    public void onCompleted() {
                    
                    }
                
                    @Override
                    public void onError(Throwable e) {
                        Crashlytics.log(Log.ERROR, "GetBroadcast", e.getLocalizedMessage());
                    }
                
                    @Override
                    public void onNext(ArrayList<BroadcastModel> broadcastModel) {
                        Log.w(TAG, "onNext: Broadcast model =>> " + broadcastModel);
                        RxBus.getInstance().post(new BroadcastBus(broadcastModel));
                    }
                });
    }

    private void requestNews() {

        APIHelper.GetNews(Prefz.getDriverNo())
                .subscribe(new Subscriber<ArrayList<NewsModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Crashlytics.log(Log.ERROR, "GetNews", e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(ArrayList<NewsModel> newsModels) {
                        Log.w(TAG, "onNext: News model =>> " + newsModels);
                        //newsModelLive.setValue(newsModels);
                        RxBus.getInstance().post(new NewsBus(newsModels));
                    }
                });
    }
}
