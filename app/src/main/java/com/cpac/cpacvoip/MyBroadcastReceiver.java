package com.cpac.cpacvoip;

import android.app.Activity;
import android.app.Application;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.cpac.cpacvoip.activity.MainActivity;
import com.cpac.cpacvoip.model.LoginChatRoomModel;
import com.cpac.cpacvoip.model.OrgChartChainDto;
import com.cpac.cpacvoip.qb.services.CallService;
import com.cpac.cpacvoip.qb.utils.Consts;
import com.cpac.cpacvoip.qb.utils.SharedPrefsHelper;
import com.cpac.cpacvoip.services.APIHelper;
import com.cpac.cpacvoip.services.ExitActivity;
import com.cpac.cpacvoip.util.Constant;
import com.cpac.cpacvoip.util.Mockupz;
import com.cpac.cpacvoip.util.Prefz;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.SubscribePushStrategy;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.messages.services.QBPushManager;
import com.quickblox.messages.services.SubscribeService;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;

import rx.Subscriber;

import static com.cpac.cpacvoip.util.Constant.KEY_RESET;

public class MyBroadcastReceiver extends BroadcastReceiver {

    private QBUser userForSave;
    private OrgChartChainDto dtoModel;

    @Override
    public void onReceive(Context context, Intent intent) {
//        if(intent.getAction().equalsIgnoreCase("cpac.joylife.voip.login")){
        if(intent.getAction().equalsIgnoreCase("cpac.joylife.voip.login")) {
            String jsonStr = intent.getStringExtra("data");
            Log.w("CPAC", "onReceive: class =>>> " + jsonStr);
            
            OrgChartChainDto dto = new Gson().fromJson(jsonStr, OrgChartChainDto.class);
            if (dto != null) {
                Prefz.clear_logout();
                dtoModel = dto;

//                new Handler().postDelayed(() -> MainActivity.startWithReceiver(context, dto), 500);
                new Handler().postDelayed(() -> MainActivity.prepareStartWithReceiver(dto), 500);
                new Handler().postDelayed(() -> {
//                    sendBroadcastToMainAcitivity(context);
                    signupStarter(context);
                }, 1000);

            }
            
        }
    }

    private void sendBroadcastToMainAcitivity(Context context) {
        Intent intetReset = new Intent(KEY_RESET);
        context.sendBroadcast(intetReset);
    }
<<<<<<< HEAD
=======

    private void signupStarter(Context context) {
        QBSettings.getInstance().setEnablePushNotification(true);
        QBSettings.getInstance().setSubscribePushStrategy(SubscribePushStrategy.ALWAYS);
        SubscribeService.subscribeToPushes(context, true);

        /*
         * STABLE
         */
//            ExitActivity.exitApplication(context);

        QBUser user = Mockupz.getQBUserLogin();
        if(dtoModel != null) {
            user.setLogin("driver" + dtoModel.getDriverNo());
            user.setEmail("driver" + dtoModel.getDriverNo() + "@commuone.com");
            user.setFullName(dtoModel.getDriverName());
        }

        QBUsers.signUp(user).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                Log.i("CPAC", "onSuccess: Created new user");
                StringifyArrayList<String> userTags = new StringifyArrayList<>();
                userTags.add(Constant.ROOM_NAME);
                qbUser.setPassword(Constant.USER_PASSW);
                qbUser.setTags(userTags);
                signInStarter(qbUser, context);
            }

            @Override
            public void onError(QBResponseException e) {
                if (e.getHttpStatusCode() == Consts.ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                    Log.w("CPAC", "onError: Has exist user");
                    new Handler().postDelayed(() -> signInStarter(user, context), 100);
                } else {
                    Crashlytics.log(Log.ERROR, "QBUsers.signUp", e.getLocalizedMessage());
                }
            }
        });
    }

    private void signInStarter(QBUser user, Context context) {
        //showProgressDialog();
        Log.i("CPAC", "Do QBUsers.signIn");
        QBUsers.signIn(user).performAsync( new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                /* Tie Fix */
                qbUser.setPassword(user.getPassword());
                qbUser.setId(user.getId());
                userForSave = qbUser;
                loginStarter(qbUser, context);

                FirebaseMessaging.getInstance().subscribeToTopic("overall")
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Log.d("TAG", "subscribeToTopic success.");
                            }else {
                                Log.d("TAG", "subscribeToTopic Failed.");

                            }
                        });
            }

            @Override
            public void onError(QBResponseException e) {
                Crashlytics.log(Log.ERROR, "QBUsers.signIn", e.getLocalizedMessage());
                Log.w("CPAC", "onError: Signin error => " + e.getLocalizedMessage() + ", code => " + e.getHttpStatusCode());
            }
        });
    }


    public void loginStarter(QBUser qbUser, Context context) {
        if (dtoModel != null) {
            dtoModel.setQbUserId(qbUser.getId().toString());
            String firebaseToken = FirebaseInstanceId.getInstance().getToken();
            dtoModel.setFbToken(firebaseToken);
            Prefz.saveFBToken(firebaseToken);
            Prefz.saveDTOObject(dtoModel);
            saveUserData(userForSave);

//            Intent tempIntent = new Intent(context, CallService.class);
//            PendingIntent pendingIntent = PendingIntent.getService(context, 0, tempIntent, 0);
//            CallService.start(context, qbUser, pendingIntent);

            APIHelper.Login(dtoModel).subscribe(new Subscriber<ArrayList<LoginChatRoomModel>>() {
                @Override
                public void onCompleted() {
                    Log.w("CPAC", "onNext: Send login Complete ...");
                }

                @Override
                public void onError(Throwable e) {
                    Log.w("CPAC", "onNext: Send login Error ..." + e.getLocalizedMessage());
                    ExitActivity.restartActivity(context);
                }

                @Override
                public void onNext(ArrayList<LoginChatRoomModel> list) {
                    Log.w("CPAC", "onNext: APIHelper.Login Success... " + list.size());
                    sendBroadcastToMainAcitivity(context);
                    new Handler().postDelayed(() -> ExitActivity.restartActivity(context), 1000);
                }
            });
        }else {
            Log.e("CPAC", "loginToChat: dtoModel null");
        }
    }

    private void saveUserData(QBUser qbUser) {
        if(qbUser != null) {
            SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
            sharedPrefsHelper.saveQbUser(qbUser);
            try {
                sharedPrefsHelper.save(Consts.PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
            } catch (Exception ignored) {}
        }
    }
>>>>>>> parent of ce92f00... Update 2.0.2
}
