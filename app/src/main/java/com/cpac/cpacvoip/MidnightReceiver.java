package com.cpac.cpacvoip;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.cpac.cpacvoip.services.ExitActivity;
import com.cpac.cpacvoip.util.Constant;
import com.cpac.cpacvoip.util.DateExt;
import com.cpac.cpacvoip.util.Prefz;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class MidnightReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        String calCheck = DateExt.toTimeHMString(calendar.getTime());
        String currentTime = DateExt.toTimeHMString(new Date());
//        Log.w("CPAC", "Midnight: Checking");

        if(intent.getExtras() != null &&
                intent.getExtras().getInt("requestcode") == Constant.REQUEST_ALARM_MIDNIGHT &&
                calCheck.equals(currentTime)
        ) {
            Log.w("CPAC", "Midnight: Logout and delete userqb.");
            Prefz.clear_logout();
            ExitActivity.exitApplication(context);
        }
    }
}