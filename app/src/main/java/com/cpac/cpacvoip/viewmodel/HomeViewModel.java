package com.cpac.cpacvoip.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.Bundle;
import android.util.Log;

import com.cpac.cpacvoip.activity.MainActivity;
import com.cpac.cpacvoip.model.AdminIdResponse;
import com.cpac.cpacvoip.model.BroadcastBus;
import com.cpac.cpacvoip.model.BroadcastModel;
import com.cpac.cpacvoip.model.ChatRoomBus;
import com.cpac.cpacvoip.model.ChatRoomModel;
import com.cpac.cpacvoip.model.NewsBus;
import com.cpac.cpacvoip.model.NewsModel;
import com.cpac.cpacvoip.model.RequestRoomId;
import com.cpac.cpacvoip.qb.db.QbUsersDbManager;
import com.cpac.cpacvoip.services.APIHelper;
import com.cpac.cpacvoip.util.Constant;
import com.cpac.cpacvoip.util.RxBus;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import rx.Subscriber;

public class HomeViewModel {
    private final String TAG = "CPAC";
    private MainActivity context;
    
    private MutableLiveData<ArrayList<BroadcastModel>> broadcastModelLive = new MutableLiveData<>();
    private MutableLiveData<ArrayList<ChatRoomModel>> chatRoomModelLive = new MutableLiveData<>();
    
    public HomeViewModel(MainActivity context) {
        this.context = context;
        loadUsersByTag();
    }
    
    public void requestBroadcast(String driverNo) {
        //context.showProgressDialog();
        if (driverNo == null) {
            return;
        }
        APIHelper.GetBroadcast(driverNo)
                //.doOnTerminate(() -> context.hideProgressDialog())
                .subscribe(new Subscriber<ArrayList<BroadcastModel>>() {
            @Override
            public void onCompleted() {
            
            }
    
            @Override
            public void onError(Throwable e) {
            
            }
    
            @Override
            public void onNext(ArrayList<BroadcastModel> broadcastModel) {
                Log.w(TAG, "onNext: Broadcast model =>> " + broadcastModel);
                broadcastModelLive.setValue(broadcastModel);
                RxBus.getInstance().post(new BroadcastBus(broadcastModel));
            }
        });
    }
    
    public void requestNews(String driverNo) {
        if (driverNo == null) {
            return;
        }
        APIHelper.GetNews(driverNo)
                .doOnTerminate(() -> context.hideProgressDialog())
                .subscribe(new Subscriber<ArrayList<NewsModel>>() {
                    @Override
                    public void onCompleted() {
                    
                    }
                
                    @Override
                    public void onError(Throwable e) {
                    
                    }
                
                    @Override
                    public void onNext(ArrayList<NewsModel> newsModels) {
                        Log.w(TAG, "onNext: News model =>> " + newsModels);
                        //newsModelLive.setValue(newsModels);
                        RxBus.getInstance().post(new NewsBus(newsModels));
                    }
                });
    }
    
    public void requestChatRoom() {
        APIHelper.GetChatRoom()
                .doOnTerminate(() -> context.hideProgressDialog())
                .subscribe(new Subscriber<ArrayList<ChatRoomModel>>() {
                    @Override
                    public void onCompleted() {
                    
                    }
                    
                    @Override
                    public void onError(Throwable e) {
                        
                        /// Mockup
                        ArrayList<ChatRoomModel> chatRoomModels = new ArrayList<>();
                        ChatRoomModel model = new ChatRoomModel();
                        model.setRoomid("scg31595");
                        model.setRoomName("ห้องสนทนา Dispatch BK");
//                        model.setUserId("77181647"); // UserTest1
//                        model.setUserId("75268952"); // Admin2
                        model.setUserId("75267450"); // Admin1
                        chatRoomModels.add(model);
                        chatRoomModelLive.setValue(chatRoomModels);
                        RxBus.getInstance().post(new ChatRoomBus(chatRoomModels));
                    }
                    
                    @Override
                    public void onNext(ArrayList<ChatRoomModel> chatRoomModels) {
                        Log.w(TAG, "onNext: Broadcast model =>> " + chatRoomModels);
                        chatRoomModelLive.setValue(chatRoomModels);
                        RxBus.getInstance().post(new ChatRoomBus(chatRoomModels));
                    }
                });
    }
    
    public void requestChatRoom(RequestRoomId request) {
        APIHelper.PostChatRoom(request)
                .doOnTerminate(() -> context.hideProgressDialog())
                .subscribe(new Subscriber<AdminIdResponse>() {
                    @Override
                    public void onCompleted() {
                    
                    }
                    
                    @Override
                    public void onError(Throwable e) {
                        //
                    }
                    
                    @Override
                    public void onNext(AdminIdResponse adminIdResponse) {
                        Log.w(TAG, "onNext: Broadcast chat room model =>> " + adminIdResponse);
//                        chatRoomModelLive.setValue(chatRoomModels);
//                        RxBus.getInstance().post(new LoginChatRoomBus(chatRoomModels));
                    }
                });
    }
    
    public LiveData<ArrayList<BroadcastModel>> getBroadcastModelLive() {
        return broadcastModelLive;
    }
    
    public void loadUsersByTag() {
        QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder();
        List<String> tags = new LinkedList<>();
        tags.add(Constant.ROOM_NAME);
        
        QBUsers.getUsersByTags(tags, requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
//                QbUsersHolder.getInstance().putUsers(qbUsers);
                QbUsersDbManager.getInstance(context).saveAllUsers(qbUsers,false);
            }
    
            @Override
            public void onError(QBResponseException e) {
        
            }
        });
    }
}
