package com.cpac.cpacvoip.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import com.cpac.cpacvoip.R;

public class BaseActivity extends AppCompatActivity {
    
    private ProgressDialog progressDialog;
    private ProgressDialog progressDialogCountdown;

    
    public void showProgressDialog() {
        showProgressDialog(R.string.dlg_loading);
    }

    public void showProgressDialog(@StringRes int messageId) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            
            // Disable the back button
            DialogInterface.OnKeyListener keyListener = (dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK;
            progressDialog.setOnKeyListener(keyListener);
        }
        
        progressDialog.setMessage(getString(messageId));
        
        progressDialog.show();
        
    }
    
    public void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void showProgressDialogCountdown(String message) {
        if (progressDialogCountdown == null) {
            progressDialogCountdown = new ProgressDialog(this);
            progressDialogCountdown.setIndeterminate(true);
            progressDialogCountdown.setCancelable(false);
            progressDialogCountdown.setCanceledOnTouchOutside(false);

            // Disable the back button
            DialogInterface.OnKeyListener keyListener = (dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK;
            progressDialogCountdown.setOnKeyListener(keyListener);
        }

//        progressDialog.setMessage(getString(R.string.dlg_loading_countdown) + " .." + countdown);
        progressDialogCountdown.setMessage(message);

        progressDialogCountdown.show();
    }

    public void updateDialogCountdown(String message) {
        progressDialogCountdown.setMessage(message);
    }

    public void hideProgressDialogCountdown() {
        if (progressDialogCountdown != null) {
            progressDialogCountdown.dismiss();
        }
    }
}
