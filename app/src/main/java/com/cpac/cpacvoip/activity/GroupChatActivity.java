package com.cpac.cpacvoip.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.delegate.OnRoomChatListener;
import com.cpac.cpacvoip.fragment.RoomChatFragment;
import com.cpac.cpacvoip.gcm.GooglePlayServicesHelper;
import com.cpac.cpacvoip.model.AdminIdResponse;
import com.cpac.cpacvoip.model.ChatRoomModel;
import com.cpac.cpacvoip.model.RequestRoomId;
import com.cpac.cpacvoip.qb.activity.BaseActivity;
import com.cpac.cpacvoip.qb.activity.ListUserActivity;
import com.cpac.cpacvoip.qb.manager.DialogsManager;
import com.cpac.cpacvoip.qb.utils.GcmConsts;
import com.cpac.cpacvoip.qb.utils.SharedPrefsHelper;
import com.cpac.cpacvoip.qb.utils.VerboseQbChatConnectionListener;
import com.cpac.cpacvoip.qb.utils.chat.ChatHelper;
import com.cpac.cpacvoip.qb.utils.dialog.QbChatDialogMessageListenerImp;
import com.cpac.cpacvoip.services.APIHelper;
import com.cpac.cpacvoip.util.Constant;
import com.cpac.cpacvoip.util.KAlert;
import com.cpac.cpacvoip.util.Mockupz;
import com.cpac.cpacvoip.util.Prefz;
import com.cpac.cpacvoip.util.Utilz;
import com.google.gson.JsonObject;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.listeners.QBChatDialogParticipantListener;
import com.quickblox.chat.listeners.QBSubscriptionListener;
import com.quickblox.chat.listeners.QBSystemMessageListener;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.muc.DiscussionHistory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import okhttp3.ResponseBody;
import rx.Observer;
import rx.Subscriber;

import static com.cpac.cpacvoip.qb.utils.chat.ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
import static com.quickblox.chat.model.QBAttachment.AUDIO_TYPE;
import static com.quickblox.videochat.webrtc.QBRTCClient.TAG;

public class GroupChatActivity extends BaseActivity implements DialogsManager.ManagingDialogsCallbacks,
        OnRoomChatListener {
    
//    enum UserType {
//        USER1, USER2, USER3, USER4
//    }
//
//    private UserType type = UserType.USER1;
    private QBUser userForSave;
    private ChatRoomModel chatRoomModel;
    
    
    /**
     *  Quickblox chat listener
     * */
    private ActionMode currentActionMode;
    //    private SwipyRefreshLayout setOnRefreshListener;
    private QBRequestGetBuilder requestBuilder;
    private Menu menu;
    private int skipRecords = 0;
    private boolean isProcessingResultInProgress;
    
    private BroadcastReceiver pushBroadcastReceiver;
    private GooglePlayServicesHelper googlePlayServicesHelper;
    //    private DialogsAdapter dialogsAdapter;
    private QBChatDialogMessageListener allDialogsMessagesListener;
    private SystemMessagesListener systemMessagesListener;
    private QBSystemMessagesManager systemMessagesManager;
    private QBIncomingMessagesManager incomingMessagesManager;
    private DialogsManager dialogsManager;
    private QBChatDialog qbChatDialog;
    private GroupChatMessageListener groupChatMessageListener;
    private ConnectionListener chatConnectionListener;
    private RoomChatFragment fragment;
    
    public static void start(Context context, ChatRoomModel model) {
        Intent intent = new Intent(context, GroupChatActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(Constant.KEY_CHATROOM, model);
        context.startActivity(intent);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            try {
                chatRoomModel = (ChatRoomModel) extras.getSerializable(Constant.KEY_CHATROOM);
            } catch (Exception ignored) {
            
            }
        } else {
            showAlertFinished(getString(R.string.alert_chatroom_not_found) ,true);
        }
        fragment = RoomChatFragment.newInstance(false, chatRoomModel);
        fragment.setOnRoomChatListener(this);
        attachFragment(fragment);
        
        setupQBChat();

        LocalBroadcastManager.getInstance(this).registerReceiver(pushBroadcastReceiver,
                new IntentFilter(GcmConsts.ACTION_NEW_GCM_EVENT));
    }
    
    private void setupQBChat() {
        initQBChat();
        registerQbChatListeners();
    
        // TODO: Next update.
        try {
            loadGroupDialog();
        } catch (Exception e) {
            e.printStackTrace();
            hideProgressDialog();
        }
        
    }
    
    public void attachFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_fragment_group_chat, fragment);
//        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    public void showChooseUser(View view) {
        if(chatRoomModel == null) {
            showAlertFinished(getString(R.string.alert_chatroom_not_found),false);
            return;
        }

        showProgressDialog();
        APIHelper.PostChatRoom(new RequestRoomId(chatRoomModel.getRoomid()))
                .doOnTerminate(this::hideProgressDialog)
                .subscribe(new Subscriber<AdminIdResponse>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {}

                    @Override
                    public void onNext(AdminIdResponse adminIdResponse) {
                        Log.w(TAG, "onNext: Broadcast chat room model =>> " + adminIdResponse.getAdminId());
                        String adminId = adminIdResponse.getAdminId();

                        if (adminId.equals("")){
                            shortToast(R.string.alert_admin_not_found_callcenter);
                        }else {
                            QBRoster chatRoster = QBChatService.getInstance().getRoster();
                            chatRoster.addSubscriptionListener(i -> {
                                try {
                                    chatRoster.confirmSubscription(Integer.parseInt(adminId));
                                } catch (SmackException.NotConnectedException e) {
                                    e.printStackTrace();
                                } catch (SmackException.NotLoggedInException e) {
                                    e.printStackTrace();
                                } catch (XMPPException e) {
                                    e.printStackTrace();
                                } catch (SmackException.NoResponseException e) {
                                    e.printStackTrace();
                                }
                            });

                            try {
                                chatRoster.createEntry(Integer.parseInt(adminId), null);
                            } catch (XMPPException e) {
                                e.printStackTrace();
                            } catch (SmackException.NotLoggedInException e) {
                                e.printStackTrace();
                            } catch (SmackException.NotConnectedException e) {
                                e.printStackTrace();
                            } catch (SmackException.NoResponseException e) {
                                e.printStackTrace();
                            }

                            createDialog(adminId);
                        }
                    }
                });
    }

    /**
     * QB Chat Function
     * */
    
    private void initQBChat() {
        googlePlayServicesHelper = new GooglePlayServicesHelper();
        
        pushBroadcastReceiver = new PushBroadcastReceiver();
        
        allDialogsMessagesListener = new AllDialogsMessageListener();
        systemMessagesListener = new SystemMessagesListener();
        
        dialogsManager = new DialogsManager();
        
        requestBuilder = new QBRequestGetBuilder();
    
        requestBuilder.setSkip(0);
        requestBuilder.setLimit(CHAT_HISTORY_ITEMS_PER_PAGE);
//        requestBuilder.sortDesc(CHAT_HISTORY_ITEMS_SORT_FIELD);
    }
    
    private void registerQbChatListeners() {
        incomingMessagesManager = QBChatService.getInstance().getIncomingMessagesManager();
        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();
        
        if (incomingMessagesManager != null) {
            incomingMessagesManager.addDialogMessageListener(allDialogsMessagesListener != null ? allDialogsMessagesListener : new AllDialogsMessageListener());
        }
        
        if (systemMessagesManager != null) {
            systemMessagesManager.addSystemMessageListener((systemMessagesListener != null) ? systemMessagesListener : new SystemMessagesListener());
        }
        
        dialogsManager.addManagingDialogsCallbackListener(this);
    }
    
    private void unregisterQbChatListeners() {
        if (incomingMessagesManager != null) {
            incomingMessagesManager.removeDialogMessageListrener(allDialogsMessagesListener);
        }
        
        if (systemMessagesManager != null) {
            systemMessagesManager.removeSystemMessageListener(systemMessagesListener);
        }
        
        dialogsManager.removeManagingDialogsCallbackListener(this);
    }
    
    private void settingGroupChat(QBChatDialog qbChatDialog) throws SmackException.NotConnectedException, XMPPException.XMPPErrorException, SmackException.NoResponseException {
        this.qbChatDialog = qbChatDialog;
        qbChatDialog.initForChat(QBChatService.getInstance());
        groupChatMessageListener = new GroupChatMessageListener();
        qbChatDialog.addMessageListener(groupChatMessageListener);
        qbChatDialog.addParticipantListener(participantListener);
        ChatHelper.getInstance().join(this.qbChatDialog, null);

//        DiscussionHistory discussionHistory = new DiscussionHistory();
//        discussionHistory.setMaxStanzas(0);
//        this.qbChatDialog.join(discussionHistory, new QBEntityCallback() {
//            @Override
//            public void onSuccess(Object o, Bundle bundle) {
//                initChatConnectionListener();
//                loadGroupChatHistory(false);
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//
//            }
//        });

        int size = 0;
        if (qbChatDialog.getOccupants() != null) {
            size = qbChatDialog.getOccupants().size();
        }else {
            if (qbChatDialog.requestOnlineUsers() != null) {
                size = qbChatDialog.requestOnlineUsers().size();
            }
        }

        Log.w("CPAC", "settingGroupChat: Group Chat members => " + size);

        fragment.setPeopleSize(size);
        initChatConnectionListener();
        loadGroupChatHistory(false);
    }
    
    private void initChatConnectionListener() {
        chatConnectionListener = new VerboseQbChatConnectionListener() {
            @Override
            public void reconnectionSuccessful() {
                super.reconnectionSuccessful();
//                skipPagination = 0;
                if (qbChatDialog.getType() == QBDialogType.GROUP) {
                    // checkAdapterInit = false;
                    // Join active room if we're in Group Chat
                    runOnUiThread(() -> joinGroupChat());
                }
            }
        };
    }
    
    private void showAlertFinished(String title, boolean isFinished) {
        KAlert.show(this, "การเชื่อมต่อล้มเหลว", title, (dialog, which) -> {
            if(isFinished) {
                GroupChatActivity.this.finish();
            }
        });
    }
    
    private void loadGroupDialog() throws Exception {
        showProgressDialog();
        Log.w("CPAC", "loadGroupDialog: with " + chatRoomModel.getRoomid());
//        ChatHelper.getInstance().getDialogs();
        ChatHelper.getInstance().getDialogById(chatRoomModel.getRoomid(), new QBEntityCallback<QBChatDialog>() {
            @Override
            public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {
                hideProgressDialog();
                isProcessingResultInProgress = false;
                try {
                    settingGroupChat(qbChatDialog);
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                } catch (XMPPException.XMPPErrorException e) {
                    e.printStackTrace();
                } catch (SmackException.NoResponseException e) {
                    e.printStackTrace();
                }
                Log.d("Group", "Dialog onSuccess " + qbChatDialog.getDialogId());
            }
    
            @Override
            public void onError(QBResponseException e) {
                hideProgressDialog();
                // TODO : For Test
//                shortToast(R.string.alert_cannot_join_group);
                Log.e("Group", "Dialog onError " + e.getLocalizedMessage());

                forceJoinGroupChatByApi(chatRoomModel.getRoomid());
            }
        });
    }

    private void forceJoinGroupChatByApi(String roomId) {
        String driverNo = Prefz.getDriverNo();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("driver_no", driverNo);
        jsonObject.addProperty("room_id", roomId);
        showProgressDialog();
        APIHelper.ForceJoinGroupChat(jsonObject).subscribe(new Observer<ResponseBody>() {
            @Override
            public void onCompleted() {
                Log.d("Group", "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Group", "onError " + e.getLocalizedMessage());
                hideProgressDialog();
            }

            @Override
            public void onNext(ResponseBody responseBody) {
                Log.d("Group", "onNext");
                new Handler().postDelayed(() -> {
                    hideProgressDialog();
                    try {
                        loadGroupDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }, 5000);
            }
        });
    }

    
    private void createDialog(String adminId) {
        final ArrayList<QBUser> selectedUsers = getAdminUser(adminId);

        if (selectedUsers == null) {
            shortToast(R.string.alert_admin_not_found_callcenter);
            return;
        }

        showProgressDialog();
        ChatHelper.getInstance().createDialogWithSelectedUsers(selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        hideProgressDialog();
                        isProcessingResultInProgress = false;
                        ListUserActivity.start(GroupChatActivity.this, false, chatRoomModel, dialog,adminId);
//                        dialogsManager.sendSystemMessageAboutCreatingDialog(systemMessagesManager, dialog);
//                        ChatActivity.startForResult(DialogsActivity.this, REQUEST_DIALOG_ID_FOR_UPDATE, dialog);
//                        ProgressDialogFragment.hide(getSupportFragmentManager());

                    }
                    
                    @Override
                    public void onError(QBResponseException e) {
                        hideProgressDialog();
                        isProcessingResultInProgress = false;
                        //shortToast("Error, Can't open chat.");
                        showAlertFinished(e.getLocalizedMessage(),false);
//                        ProgressDialogFragment.hide(getSupportFragmentManager());
//                        showErrorSnackbar(R.string.dialogs_creation_error, null, null);
                    }
                }
        );
    }
    
    private ArrayList<QBUser> getAdminUser(String adminId) {
        try {
            SharedPrefsHelper shared = SharedPrefsHelper.getInstance();
            QBUser currentUser = shared.getQbUser();
            if(currentUser == null) {
                showAlertFinished(getString(R.string.alert_require_checkin),true);
                return null;
            }
            ArrayList<QBUser> qbList = new ArrayList<>();
        
            StringifyArrayList<String> userTags = new StringifyArrayList<>();
            userTags.add("scg35171");
            userTags.add("scg31596");
            userTags.add(Constant.ROOM_NAME);
        
            // Admin
            QBUser adminUser = new QBUser();
            if(adminId != null) {
                adminUser.setId(Integer.parseInt(adminId));

                QBRoster chatRoster = QBChatService.getInstance().getRoster();
                chatRoster.confirmSubscription(Integer.parseInt(adminId));
            } else {
//                adminUser = Mockupz.getQBAdminToCall();
                showAlertFinished(getString(R.string.alert_admin_not_found),true);
                return null;
            }

            adminUser.setTags(userTags);
            adminUser.setId(Integer.parseInt(adminId));
            adminUser.setFullName(adminUser.getFullName());
            adminUser.setLogin(adminUser.getLogin());

            adminUser.setTags(userTags);

            qbList.add(adminUser);
            qbList.add(currentUser);
        
            Log.i("CPAC", "getAdminUser: with Id => " + adminUser.getId());
            return qbList;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return null;
        } catch (XMPPException e) {
            e.printStackTrace();
            return null;
        } catch (SmackException.NotLoggedInException e) {
            e.printStackTrace();
            return null;
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
            return null;
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        googlePlayServicesHelper.checkPlayServicesAvailable(this);
        ChatHelper.getInstance().addConnectionListener(chatConnectionListener);

    }
    
    @Override
    protected void onPause() {
        super.onPause();
        ChatHelper.getInstance().removeConnectionListener(chatConnectionListener);
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(pushBroadcastReceiver);
    }
    
    @Override
    protected void onDestroy() {
        unregisterQbChatListeners();
        try {
            if (ChatHelper.getInstance() !=null) {
                if (qbChatDialog != null) {
                    ChatHelper.getInstance().leaveChatDialog(qbChatDialog);
                }
            }
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
    
    
    /* ** QBChat Group ** */
    
    private void joinGroupChat() {
//        progressBar.setVisibility(View.VISIBLE);
        ChatHelper.getInstance().join(qbChatDialog, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle b) {
//                if (snackbar != null) {
//                    snackbar.dismiss();
//                }
                loadGroupChatHistory(true);
            }
            
            @Override
            public void onError(QBResponseException e) {
//                progressBar.setVisibility(View.GONE);
//                snackbar = showErrorSnackbar(R.string.connection_error, e, null);
            }
        });
    }
    
    private void loadGroupChatHistory(boolean isFromReconnect) {
        ChatHelper.getInstance().loadChatHistory(qbChatDialog, 0, new QBEntityCallback<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> messages, Bundle args) {
                Collections.reverse(messages);
                if(isFromReconnect) {
                    fragment.addReconnectMessages(messages);
                } else {
                    fragment.addMessages(messages);
                }
                scrollMessageListDown();
            }
            
            @Override
            public void onError(QBResponseException e) {
                Log.e("CPAC", "onError: load group chat error " + e.getLocalizedMessage() );
            }
        });
    }
    
    private void configChatMessage(String text, QBAttachment attachment) {
        QBChatMessage chatMessage = new QBChatMessage();
        if (attachment != null) {
            chatMessage.addAttachment(attachment);
        } else {
            chatMessage.setBody(text);
        }
        chatMessage.setSaveToHistory(true);
        chatMessage.setDateSent(System.currentTimeMillis() / 1000);
        chatMessage.setMarkable(true);
        

        if (qbChatDialog != null) {
            if (qbChatDialog.isJoined()) {
                sendMessage(chatMessage);
            }else {
                DiscussionHistory discussionHistory = new DiscussionHistory();
                discussionHistory.setMaxStanzas(0);
                qbChatDialog.join(discussionHistory, new QBEntityCallback() {
                    @Override
                    public void onSuccess(Object o, Bundle bundle) {
                        sendMessage(chatMessage);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        shortToast(e.getLocalizedMessage());
                    }
                });
            }
        }else {
            shortToast(R.string.toast_cant_send_voice);
        }

    }

    private void sendMessage(QBChatMessage chatMessage) {
        try {
            qbChatDialog.sendMessage(chatMessage);
        } catch (SmackException.NotConnectedException e) {
            Log.w("CPAC", e);
            shortToast(R.string.toast_cant_send_voice);
        }
    }
    public void showMessage(QBChatMessage message) {
        fragment.addMessage(message);
        scrollMessageListDown();
    }
    
    private void scrollMessageListDown() {
//        rvChat.scrollToPosition(chatWithAdminAdapter.getItemCount() - 1);
    }
    
    private QBChatMessage addChatProperties(QBChatMessage qbChatMessage, QBChatDialog dialog){
        qbChatMessage.setDialogId(dialog.getDialogId());
//        qbChatMessage.setProperty(Consts.PROPERTY_OCCUPANTS_IDS, QbDialogUtils.getOccupantsIdsStringFromList(dialog.getOccupants()));
//        qbChatMessage.setProperty(Consts.PROPERTY_DIALOG_TYPE, String.valueOf(dialog.getType().getCode()));
//        qbChatMessage.setProperty(Consts.PROPERTY_DIALOG_NAME, String.valueOf(dialog.getName()));

//        qbChatMessage.setProperty(Consts.PROPERTY_NOTIFICATION_TYPE, "Creating_Dialog");
        
        return qbChatMessage;
    }
    
    @Override
    public void sendAudioPath(String path) {
        showProgressDialog();
        final QBAttachment attachment = new QBAttachment(AUDIO_TYPE);
        try {
            File file = new File(path);
            QBContent.uploadFileTask(file, true, null, null).performAsync(new QBEntityCallback<QBFile>() {
                @Override
                public void onSuccess(QBFile qbFile, Bundle bundle) {
                    hideProgressDialog();
                    // attach a photo
                    String url = qbFile.getPublicUrl();
                    attachment.setId(qbFile.getUid());
                    attachment.setUrl(url);
                    attachment.setDuration(Utilz.getDuration(file));
                    attachment.setSize(Integer.parseInt(String.valueOf(file.length()/1024)));
                    attachment.setContentType("audio/mp3");
//                    attachment.setName(Prefz.getDriverNo() + new Date().getTime());
                    attachment.setName("โดย " + Prefz.extractDTO().getDriverName());
                    configChatMessage(null, attachment);
                }
            
                @Override
                public void onError(QBResponseException e) {
                    // error
                    shortToast("Error, Can't send audio file.");
                    hideProgressDialog();
                }
            });
        } catch (Exception e) {
            hideProgressDialog();
        }
    }
    
    
    private class GroupChatMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
            Log.w("CPAC", "processMessage: message receive =>>> " + qbChatMessage.getBody());
            qbChatMessage = addChatProperties(qbChatMessage, qbChatDialog);
            showMessage(qbChatMessage);

//            int size = qbChatDialog.getOccupants().size();
//            try {
//                size = qbChatDialog.requestOnlineUsers().size();
//            } catch (XMPPException.XMPPErrorException | SmackException.NotConnectedException | SmackException.NoResponseException e) {
//                e.printStackTrace();
//            }
//            fragment.setPeopleSize(size);
        }
    }


    public QBChatDialogParticipantListener participantListener = new QBChatDialogParticipantListener() {
        @Override
        public void processPresence(String s, QBPresence qbPresence) {
            int size = 0;
//                    qbChatDialog.getOccupants().size();
            try {
                size = qbChatDialog.requestOnlineUsers().size();
            } catch (XMPPException.XMPPErrorException | SmackException.NotConnectedException | SmackException.NoResponseException e) {
                e.printStackTrace();
            }
            fragment.setPeopleSize(size);
        }
    };



    /* **START** Dialogs Manager ManagingDialogsCallbacks */
    
    @Override
    public void onDialogCreated(QBChatDialog chatDialog) {
    
    }
    
    @Override
    public void onDialogUpdated(String chatDialog) {
    
    }
    
    @Override
    public void onNewDialogLoaded(QBChatDialog chatDialog) {
    
    }
    /* **END** Dialogs Manager ManagingDialogsCallbacks */
    
    
    private class PushBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent

//            String message = intent.getStringExtra(GcmConsts.EXTRA_GCM_MESSAGE);
//            Log.v(TAG, "Received broadcast " + intent.getAction() + " with data: " + message);
//            requestBuilder.setSkip(skipRecords = 0);
//            loadDialogsFromQb(true, true);
        }
    }
    
    private class SystemMessagesListener implements QBSystemMessageListener {
        @Override
        public void processMessage(final QBChatMessage qbChatMessage) {
            dialogsManager.onSystemMessageReceived(qbChatMessage);
        }
        
        @Override
        public void processError(QBChatException e, QBChatMessage qbChatMessage) {
        
        }
    }
    
    private class AllDialogsMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(final String dialogId, final QBChatMessage qbChatMessage, Integer senderId) {
            if (!senderId.equals(ChatHelper.getCurrentUser().getId())) {
                dialogsManager.onGlobalMessageReceived(dialogId, qbChatMessage);
            }
        }
    }
}
