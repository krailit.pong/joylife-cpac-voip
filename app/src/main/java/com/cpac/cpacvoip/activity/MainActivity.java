package com.cpac.cpacvoip.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.fragment.FaqFragment;
import com.cpac.cpacvoip.fragment.HomeFragment;
import com.cpac.cpacvoip.fragment.ImportantNoticeFragment;
import com.cpac.cpacvoip.gcm.GooglePlayServicesHelper;
import com.cpac.cpacvoip.model.ChatRoomUpdateBus;
import com.cpac.cpacvoip.model.LoginChatRoomModel;
import com.cpac.cpacvoip.model.OrgChartChainDto;
import com.cpac.cpacvoip.qb.manager.DialogsManager;
import com.cpac.cpacvoip.qb.services.CallService;
import com.cpac.cpacvoip.qb.utils.Consts;
import com.cpac.cpacvoip.qb.utils.GcmConsts;
import com.cpac.cpacvoip.qb.utils.SharedPrefsHelper;
import com.cpac.cpacvoip.qb.utils.chat.ChatHelper;
import com.cpac.cpacvoip.qb.utils.dialog.QbChatDialogMessageListenerImp;
import com.cpac.cpacvoip.services.APIHelper;
import com.cpac.cpacvoip.services.BroadcastService;
import com.cpac.cpacvoip.services.ExitActivity;
import com.cpac.cpacvoip.util.Constant;
import com.cpac.cpacvoip.util.KAlert;
import com.cpac.cpacvoip.util.Mockupz;
import com.cpac.cpacvoip.util.Prefz;
import com.cpac.cpacvoip.util.RxBus;
import com.cpac.cpacvoip.viewmodel.HomeViewModel;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.quickblox.auth.session.QBSession;
import com.quickblox.auth.session.QBSessionManager;
import com.quickblox.auth.session.QBSessionParameters;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.listeners.QBSubscriptionListener;
import com.quickblox.chat.listeners.QBSystemMessageListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.SubscribePushStrategy;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.messages.services.QBPushManager;
import com.quickblox.messages.services.SubscribeService;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import rx.Subscriber;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECEIVE_BOOT_COMPLETED;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.MICROPHONE;
import static com.cpac.cpacvoip.util.Constant.KEY_DTO_OBJECT;
import static com.cpac.cpacvoip.util.Constant.KEY_RESET;

public class MainActivity extends com.cpac.cpacvoip.activity.BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        DialogsManager.ManagingDialogsCallbacks, QBSubscriptionListener {

    private HomeViewModel homeViewModel;
    private FrameLayout flNavHeader;
    private ImageView imvNavHeader;
    private TextView tvNavHeaderName;
    private TextView tvCar;
    private TextView tvTel;
    private TextView tv4;
    private TextView tv5;
    private TextView tv6;

    private ImageView btn_exit;

    private QBUser userForSave;
    private QBRequestGetBuilder requestBuilder;
    private BroadcastReceiver pushBroadcastReceiver;
    private GooglePlayServicesHelper googlePlayServicesHelper;
    private QBChatDialogMessageListener allDialogsMessagesListener;
    private SystemMessagesListener systemMessagesListener;
    private QBSystemMessagesManager systemMessagesManager;
    private QBIncomingMessagesManager incomingMessagesManager;
    private DialogsManager dialogsManager;

    private OrgChartChainDto dtoModel;

    public static void startWithReceiver(Context context, OrgChartChainDto dto) {
        Prefz.saveDTOObject(dto);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        Prefz.saveLoginTime();

        new Handler().postDelayed(() -> {
            Intent intetReset = new Intent(KEY_RESET);
            context.sendBroadcast(intetReset);
        }, 2000);


        // To Press Home
//        new Handler().postDelayed(() -> {
//            Intent setIntent = new Intent(Intent.ACTION_MAIN);
//            setIntent.addCategory(Intent.CATEGORY_HOME);
//            setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(setIntent);
//        }, 1000);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            QBSettings.getInstance().setEnablePushNotification(true);
//            QBSettings.getInstance().setSubscribePushStrategy(SubscribePushStrategy.ALWAYS);
//            SubscribeService.subscribeToPushes(context, true);

            /*
             * STABLE
             */
//            ExitActivity.exitApplication(context);

            QBUser qbUser = SharedPrefsHelper.getInstance().getQbUser();
            if (qbUser != null) {
                createCallService(qbUser);
            }
        }
    };

    public static void prepareStartWithReceiver(OrgChartChainDto dto) {
        Prefz.saveDTOObject(dto);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        Prefz.saveLoginTime();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        homeViewModel = new HomeViewModel(this);

        registerReceiver(mMessageReceiver, new IntentFilter(KEY_RESET));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        toolbar.inflateMenu(R.menu.main);
        toolbar.setNavigationIcon(R.drawable.ic_menu_txt);
        toolbar.setNavigationOnClickListener(v -> {
            drawer.openDrawer(Gravity.START);
        });
        toolbar.setOnMenuItemClickListener(menuItem -> true);

        MenuItem versionMenuItem = toolbar.getMenu().findItem(R.id.item_version);
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            versionMenuItem.setTitle("v" + version);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        View headerView = navigationView.getHeaderView(0);

        tvNavHeaderName = headerView.findViewById(R.id.tv_nav_header_name);
        tvCar = headerView.findViewById(R.id.tv_nav_header_car);
        tvTel = headerView.findViewById(R.id.tv_nav_header_tel);
        tv4 = headerView.findViewById(R.id.tv_nav_header_4);
        tv5 = headerView.findViewById(R.id.tv_nav_header_5);
        tv6 = headerView.findViewById(R.id.tv_nav_header_6);
        flNavHeader = headerView.findViewById(R.id.fl_nav_header);
        imvNavHeader = headerView.findViewById(R.id.imv_nav_header);
        imvNavHeader.setOnClickListener(v -> KAlert.showChoose(MainActivity.this, (dialog, which) -> {
            if (which == 0) {
                startTakePicture();
            } else {
                startChooseImage();
            }
        }));
        btn_exit = drawer.findViewById(R.id.btn_exit);
        btn_exit.setOnClickListener(view -> showAlertLogout());

        attachFragmentNoBack(HomeFragment.newInstance());
        QBSessionManager.getInstance().addListener(new QBSessionManager.QBSessionListener() {
            @Override
            public void onSessionCreated(QBSession qbSession) {
                Log.i("SESSION", "onSessionCreated");
                shortToast("สร้าง Session เรียบร้อย");
            }

            @Override
            public void onSessionUpdated(QBSessionParameters qbSessionParameters) {
                Log.i("SESSION", "onSessionUpdated");
                shortToast("เชื่อมต่อ Session สำเร็จ");
            }

            @Override
            public void onSessionDeleted() {
                Log.i("SESSION", "onSessionDeleted");
            }

            @Override
            public void onSessionRestored(QBSession qbSession) {
                Log.i("SESSION", "onSessionRestored");
            }

            @Override
            public void onSessionExpired() {
                Log.i("SESSION", "onSessionExpired");
                shortToast("Session หมดอายุ");
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, 1);
//                QBSessionManager.getInstance().createActiveSession("", calendar.getTime());

                QBSessionManager.getInstance().createActiveSession(Prefz.getFBToken()+Prefz.getDriverNo(), calendar.getTime());
            }

            @Override
            public void onProviderSessionExpired(String s) {
                Log.i("SESSION", "onProviderSessionExpired");
            }
        });

        setup();
    }

    private void shortToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void attachFragmentNoBack(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_fragment, fragment);
        transaction.commit();
    }

    public void attachFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_fragment, fragment);
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    public void startChooseImage() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, Constant.REQUEST_IMAGE_GALLERY);
    }

    public void startTakePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, Constant.REQUEST_IMAGE_CAPTURE);
        }
    }

    public void startVideoCall() {
        Intent intent = new Intent(this, GroupChatActivity.class);
        startActivity(intent);
    }

    private void setupQBChat() {
        initQBChat();
        registerQbChatListeners();
    }

    private void setup() {
//        Log.i("CPAC", "QBSessionManager Token: " + QBSessionManager.getInstance().getToken());
        boolean isNeedCheck = false;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            dtoModel = (OrgChartChainDto) extras.getSerializable(KEY_DTO_OBJECT);
        }
        
        if(dtoModel == null) {
            dtoModel = Prefz.extractDTO();
        }
    
        if(dtoModel != null) {
            tvNavHeaderName.setText(dtoModel.getDriverName());
            QBUser user = Mockupz.getQBUserLogin();
            user.setLogin("driver" + dtoModel.getDriverNo());
            user.setEmail("driver" + dtoModel.getDriverNo() + "@commuone.com");
            user.setFullName(dtoModel.getDriverName());
            signUp(user);
        } else {
            isNeedCheck = true;
        }
        
        if(isNeedCheck) {
            if (SharedPrefsHelper.getInstance().getQbUser() != null) {
                Log.i("CPAC", "Do SignIn");
                QBUser user = SharedPrefsHelper.getInstance().getQbUser();
                signIn(user);
            } else {
                //signIn(Mockupz.getQBUserLogin());
                Log.i("CPAC", "Do Logout");
                showAlertFinished("Setup");
            }
        }
        setupQBChat();
        Intent broadcast = new Intent(getApplicationContext(), BroadcastService.class);
        startService(broadcast);
        
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, MICROPHONE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE,
                    READ_EXTERNAL_STORAGE,
                    CAMERA,
                    MICROPHONE,
                    RECORD_AUDIO,
                    RECEIVE_BOOT_COMPLETED,
                    ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, 1);
        }
    }
    
    @SuppressLint("SetTextI18n")
    private void setNavHeader(QBUser user) {
        if(user.getFullName() != null) {
            tvNavHeaderName.setText(user.getFullName());
        }

        if (dtoModel != null) {
            tvNavHeaderName.setText(dtoModel.getTruckNo() +" : " + dtoModel.getDriverName());
            tvCar.setText("รหัสคนขับ: " + dtoModel.getDriverNo());
            tvTel.setText(dtoModel.getDepartmentName());
            tv4.setText(dtoModel.getDispatchName());
            tv5.setText("โรงงาน: " + dtoModel.getPlantName());
            tv6.setText("ผู้รับเหมา: " + dtoModel.getVendorName());
        }
    }
    
    private void showAlertFinished(String title) {
        KAlert.show(this, title, getString(R.string.alert_require_checkin), (dialog, which) -> MainActivity.this.finish());
    }

    private void showAlertMidnight() {
        KAlert.show(this, null, getString(R.string.alert_force_logout), (dialog, which) -> {
            Prefz.clear_logout();
            ExitActivity.exitApplication(this);
        });

    }

    private void showAlertLogout() {
        KAlert.showLogout(this, null, getString(R.string.alert_logout_title), (dialog, which) -> {
            Prefz.clear_logout();
            ExitActivity.exitApplication(this);
        });
    }
    
    /*
     * QB Session.
     */
    private void signUp(final QBUser user) {
        runOnUiThread(this::showProgressDialog);
        Log.i("CPAC", "Do QBUsers.signUp");
        QBUsers.signUp(user).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                Log.i("CPAC", "onSuccess: Created new user");
                StringifyArrayList<String> userTags = new StringifyArrayList<>();
                userTags.add(Constant.ROOM_NAME);
                qbUser.setPassword(Constant.USER_PASSW);
                qbUser.setTags(userTags);

//                signIn(qbUser);

                // To Fix Join Chatroom
                signInForNewUser(qbUser);
            }

            @Override
            public void onError(QBResponseException e) {
                if (e.getHttpStatusCode() == Consts.ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                    Log.w("CPAC", "onError: Has exist user");
                    new Handler().postDelayed(() -> signIn(user), 2000);
                } else {
                    Crashlytics.log(Log.ERROR, "QBUsers.signUp", e.getLocalizedMessage());
                    runOnUiThread(() -> hideProgressDialog() );
                    showAlertFinished("Sign-Up");
                }
            }
        });
    }

    private void signInForNewUser(QBUser user) {
        //showProgressDialog();
        Log.i("CPAC", "Do QBUsers.signIn New User");
        QBUsers.signIn(user).performAsync( new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {

                /* Tie Fix */
                qbUser.setPassword(user.getPassword());
                qbUser.setId(user.getId());
                setNavHeader(qbUser);
                userForSave = qbUser;
                loginToChat(qbUser);

                showProgressDialogCountdown(getString(R.string.dlg_loading_countdown));
                new CountDownTimer(20000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        long seconds = millisUntilFinished / 1000;
                        String countdown = String.format("%02d", seconds / 60) + ":" + String.format("%02d", seconds % 60);
                        updateDialogCountdown(getString(R.string.dlg_loading_countdown) + "... " + countdown);
                    }

                    public void onFinish() {
                        hideProgressDialogCountdown();
                        ExitActivity.exitApplication(getApplicationContext());
                    }
                }.start();
            }

            @Override
            public void onError(QBResponseException e) {
                runOnUiThread(() -> hideProgressDialog() );
                Crashlytics.log(Log.ERROR, "QBUsers.signIn", e.getLocalizedMessage());
                Log.w("CPAC", "onError: Signin error => " + e.getLocalizedMessage() + ", code => " + e.getHttpStatusCode());
                showAlertFinished("Sign In");
                //shortToast("error => " + e.getLocalizedMessage());
            }
        });
    }

    private void signIn(QBUser user) {
        //showProgressDialog();
        Log.i("CPAC", "Do QBUsers.signIn");
        QBUsers.signIn(user).performAsync( new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {

                /* Tie Fix */
                qbUser.setPassword(user.getPassword());
                qbUser.setId(user.getId());
                setNavHeader(qbUser);
                userForSave = qbUser;
                loginToChat(qbUser);

                setProfileImage(Prefz.getProgileImage());
                homeViewModel.requestBroadcast(Prefz.getDriverNo());
                homeViewModel.requestNews(Prefz.getDriverNo());

                QBPushManager.getInstance().addListener(new QBPushManager.QBSubscribeListener() {
                    @Override
                    public void onSubscriptionCreated() {
                        Log.d("TAG", "onSubscriptionCreated");
                    }

                    @Override
                    public void onSubscriptionError(final Exception e, int resultCode) {
                        Log.d("TAG", "onSubscriptionError" + e);
                        if (resultCode >= 0) {
                            Log.d("TAG", "Google play service exception" + resultCode);
                        }
                        Log.d("TAG", "onSubscriptionError " + e.getMessage());
                    }

                    @Override
                    public void onSubscriptionDeleted(boolean b) {
                        Log.d("TAG", "onSubscriptionDelete " + b);
                    }
                });

                FirebaseMessaging.getInstance().subscribeToTopic("overall")
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Log.d("TAG", "subscribeToTopic success.");
                            }else {
                                Log.d("TAG", "subscribeToTopic Failed.");

                            }
                        });
            }

            @Override
            public void onError(QBResponseException e) {
                runOnUiThread(() -> hideProgressDialog() );
                Crashlytics.log(Log.ERROR, "QBUsers.signIn", e.getLocalizedMessage());
                Log.w("CPAC", "onError: Signin error => " + e.getLocalizedMessage() + ", code => " + e.getHttpStatusCode());
                showAlertFinished("Sign In");
                //shortToast("error => " + e.getLocalizedMessage());
            }
        });
    }

    public void loginToChat(QBUser qbUser) {

        if (dtoModel != null) {
            dtoModel.setQbUserId(qbUser.getId().toString());

            String firebaseToken = FirebaseInstanceId.getInstance().getToken();
            Log.i("CPAC", "loginToChat: firebase token => " + firebaseToken );

            dtoModel.setFbToken(firebaseToken);

            Prefz.saveFBToken(firebaseToken);
            Prefz.saveDTOObject(dtoModel);

            APIHelper.apiLogin(dtoModel);

            // Start Background Call Service
            saveUserData(userForSave);
            createCallService(qbUser);
        }else {
            Log.e("CPAC", "loginToChat: dtoModel null");
        }
    }

    private void createCallService(QBUser qbUser) {
        Log.i("CPAC", "createCallService with " + qbUser.getFullName());
//        Intent tempIntent = new Intent(this, CallService.class);
//        PendingIntent pendingIntent = createPendingResult(Consts.EXTRA_LOGIN_RESULT_CODE, tempIntent, 0);
//        CallService.start(this, qbUser, pendingIntent);
    }

    private void saveUserData(QBUser qbUser) {
        if(qbUser != null) {
            SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
            sharedPrefsHelper.saveQbUser(qbUser);
            try {
                sharedPrefsHelper.save(Consts.PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
            } catch (Exception ignored) {}
        }
    }


    /**
     * QB Chat Function
     * */

    private void initQBChat() {
        googlePlayServicesHelper = new GooglePlayServicesHelper();

        pushBroadcastReceiver = new PushBroadcastReceiver();

        allDialogsMessagesListener = new AllDialogsMessageListener();
        systemMessagesListener = new SystemMessagesListener();

        dialogsManager = new DialogsManager();

        requestBuilder = new QBRequestGetBuilder();
    }

    private void registerQbChatListeners() {
        incomingMessagesManager = QBChatService.getInstance().getIncomingMessagesManager();
        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();

        if (incomingMessagesManager != null) {
            incomingMessagesManager.addDialogMessageListener(allDialogsMessagesListener != null
                    ? allDialogsMessagesListener : new AllDialogsMessageListener());
        }

        if (systemMessagesManager != null) {
            systemMessagesManager.addSystemMessageListener(systemMessagesListener != null
                    ? systemMessagesListener : new SystemMessagesListener());
        }

        dialogsManager.addManagingDialogsCallbackListener(this);
    }

    private void unregisterQbChatListeners() {
        if (incomingMessagesManager != null) {
            incomingMessagesManager.removeDialogMessageListrener(allDialogsMessagesListener);
        }

        if (systemMessagesManager != null) {
            systemMessagesManager.removeSystemMessageListener(systemMessagesListener);
        }

        dialogsManager.removeManagingDialogsCallbackListener(this);
    }



    private void actionNoti() {
//        Intent intent = new Intent(this, GroupChatActivity.class);
//        startActivity(intent);

//        ChatRoomModel model = new ChatRoomModel();
//        model.setRoomid("scg31595");
//        model.setRoomName("ห้องสนทนา Dispatch BK");
//        model.setUserId("75267450");
//        GroupChatActivity.start(this, model);

//        attachFragment(RoomChatFragment.newInstance());
    }

    public HomeViewModel getHomeViewModel() {
        return homeViewModel;
    }


    /*
    * Override method
    * */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem versionMenuItem = menu.findItem(R.id.item_version);
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            versionMenuItem.setTitle(version);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_noti) {
//            actionNoti();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_main) {
            attachFragmentNoBack(HomeFragment.newInstance());
        } else if (id == R.id.nav_important_notice) {
            attachFragmentNoBack(ImportantNoticeFragment.newInstance());
        } else if (id == R.id.nav_faq) {
            attachFragmentNoBack(FaqFragment.newInstance());
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        }, 100);
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        boolean bIsSameDay = Prefz.isSameDay();
        Log.v("CPAC", "onResume MainActivity bIsSameDay = " + bIsSameDay);

        // Force to subscribe to push
        QBSettings.getInstance().setEnablePushNotification(true);
        QBSettings.getInstance().setSubscribePushStrategy(SubscribePushStrategy.ALWAYS);
        SubscribeService.subscribeToPushes(this, true);
        googlePlayServicesHelper.checkPlayServicesAvailable(this);


        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        //V1.5.5
//        if (Prefz.isSameDay()) {
//            Log.v("CPAC", "onResume getQbUser = " + SharedPrefsHelper.getInstance().getQbUser());
//            if (SharedPrefsHelper.getInstance().getQbUser() != null) {
//                QBUser u = SharedPrefsHelper.getInstance().getQbUser();
//                Log.v("CPAC", "onResume Check New Punch-In ? " + (!u.getLogin().equals("driver" + Prefz.extractDTO().getDriverNo())));
//                if (!u.getLogin().equals("driver" + Prefz.extractDTO().getDriverNo())) {
//                    showAlertFinished("New Punch-In Found.");
//                }
//            }else {
//                OrgChartChainDto dtoModel = Prefz.extractDTO();
//                dtoModel.setFbToken(Prefz.getFBToken());
//                APIHelper.apiLogin(dtoModel);
//            }
//        }else {

//        if (!bIsSameDay) {
//            showAlertMidnight();
//        }

        if (!isNetworkAvailable()) {
            showAlertFinished(getString(R.string.alert_no_internet));
        }

//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(pushBroadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
        unregisterReceiver(mMessageReceiver);
        unregisterQbChatListeners();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Consts.EXTRA_LOGIN_RESULT_CODE) {

            boolean isLoginSuccess = data.getBooleanExtra(Consts.EXTRA_LOGIN_RESULT, false);
            String errorMessage = data.getStringExtra(Consts.EXTRA_LOGIN_ERROR_MESSAGE);

            runOnUiThread(this::hideProgressDialog);
            if (isLoginSuccess) {
                //ListUserActivity.start(this, false, chatRoomModel);
                saveUserData(userForSave);
//                createDialog(getAdminUser());
//                signInCreatedUser(userForSave, false);
            } else {
                hideProgressDialog();
                //shortToast(errorMessage);
//                Toast.makeText(mContext,getString(R.string.login_chat_login_error) + errorMessage,Toast.LENGTH_LONG).show();
//                userNameEditText.setText(userForSave.getFullName());
//                chatRoomNameEditText.setText(userForSave.getTags().get(0));
            }
        }
        if(requestCode == Constant.REQUEST_IMAGE_GALLERY && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                setProfileImage(bitmap);

            } catch (IOException e) {
                Log.i("TAG", "Some exception " + e);
            } finally {
                if(imvNavHeader.getDrawable() == null){
                    flNavHeader.setVisibility(View.VISIBLE);
                }
            }
        }
        if (requestCode == Constant.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            setProfileImage(imageBitmap);
        }
    }
    
    
    private void setProfileImage(Bitmap bitmap) {
        if (bitmap != null) {
            imvNavHeader.setImageBitmap(bitmap);
            flNavHeader.setVisibility(View.GONE);
            Prefz.saveProfileImage(bitmap);
        }
    }
    
    /* **START** Dialogs Manager ManagingDialogsCallbacks */
    
    @Override
    public void onDialogCreated(QBChatDialog chatDialog) {
    
    }
    
    @Override
    public void onDialogUpdated(String chatDialog) {
        // dialog id
        RxBus.getInstance().post(new ChatRoomUpdateBus(chatDialog));
    }
    
    @Override
    public void onNewDialogLoaded(QBChatDialog chatDialog) {
    
    }
    /* **END** Dialogs Manager ManagingDialogsCallbacks */

    @Override
    public void subscriptionRequested(int userId) {
        Log.i("MainAct", "subscriptionRequested :" + userId);
        QBRoster chatRoster = QBChatService.getInstance().getRoster();
        if (chatRoster != null) {
            try {
                chatRoster.confirmSubscription(userId );
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            } catch (SmackException.NotLoggedInException e) {
                e.printStackTrace();
            } catch (XMPPException e) {
                e.printStackTrace();
            } catch (SmackException.NoResponseException e) {
                e.printStackTrace();
            }
        }
    }

    private class PushBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra(GcmConsts.EXTRA_GCM_MESSAGE);
            Log.v("CPAC", "Received broadcast " + intent.getAction() + " with data: " + message);
        }
    }
    
    private class SystemMessagesListener implements QBSystemMessageListener {
        @Override
        public void processMessage(final QBChatMessage qbChatMessage) {
            Log.w("CPAC", "processMessage: SystemMessagesListener =>> msg: " + qbChatMessage.getBody());
            dialogsManager.onSystemMessageReceived(qbChatMessage);
        }
        
        @Override
        public void processError(QBChatException e, QBChatMessage qbChatMessage) {
        
        }
    }
    
    private class AllDialogsMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(final String dialogId, final QBChatMessage qbChatMessage, Integer senderId) {
            Log.w("CPAC", "processMessage: AllDialogsMessageListener, MainActivity =>> msg: " + qbChatMessage.getBody());
            if (!senderId.equals(ChatHelper.getCurrentUser().getId())) {
                dialogsManager.onGlobalMessageReceived(dialogId, qbChatMessage);
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
