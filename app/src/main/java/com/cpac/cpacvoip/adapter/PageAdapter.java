package com.cpac.cpacvoip.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cpac.cpacvoip.fragment.NewsViewFragment;
import com.cpac.cpacvoip.fragment.NoticeViewFragment;
import com.cpac.cpacvoip.fragment.RoomViewFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PageAdapter extends FragmentPagerAdapter {
    private final NoticeViewFragment lf1 = NoticeViewFragment.newInstance();
    private final RoomViewFragment lf2 = RoomViewFragment.newInstance();
    private final NewsViewFragment lf3 = NewsViewFragment.newInstance();


    private List<Fragment> tabs = Arrays.asList(
            lf1,
            lf2,
            lf3
    );
    
    public PageAdapter(FragmentManager fm) {
        super(fm);
        
    }
    
    public void setTabs(ArrayList<Fragment> list) {
        tabs = list;
    }
    
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0: return "ประกาศ";
            case 1: return "ห้องสนทนา";
            case 2: return "ข่าวสาร";
            default: return "";
        }
    }
    
    @Override
    public Fragment getItem(int i) {
        return (i<tabs.size()) ? tabs.get(i) : new Fragment();
    }
    
    @Override
    public int getCount() {
        return tabs.size();
    }
}
