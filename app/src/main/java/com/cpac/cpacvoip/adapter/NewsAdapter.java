package com.cpac.cpacvoip.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;
import com.cpac.cpacvoip.delegate.OnChatAdminAdapterClickListener;
import com.cpac.cpacvoip.delegate.OnNewsAdapterClickListener;
import com.cpac.cpacvoip.model.NewsModel;
import com.cpac.cpacvoip.util.Constant;
import com.cpac.cpacvoip.util.DateExt;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.ArrayList;
import java.util.Date;

public class NewsAdapter extends RecyclerView.Adapter {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private Context context;
    
    private ArrayList<SectionDataModel> dataList = new ArrayList<>();
    OnNewsAdapterClickListener listener;

    public NewsAdapter(Context context) {
        this.context = context;
    }
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if(viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_header_news, viewGroup, false);
            return new NewsHeaderViewHolder(view);
        } else {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_list_news, viewGroup, false);
            return new NewsViewHolder(view);
        }
    }
    
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        SectionDataModel sectionData = dataList.get(i);
        if(viewHolder instanceof NewsHeaderViewHolder) {
            NewsHeaderViewHolder holderHeader = (NewsHeaderViewHolder)viewHolder;
            holderHeader.tvTitle.setText(sectionData.sectionTitle);
        }
        if(viewHolder instanceof NewsViewHolder) {
            NewsViewHolder holder = (NewsViewHolder) viewHolder;
            NewsModel item = sectionData.newsModel;
            holder.tvTitle.setText(item.getNewsTopic());
            holder.tvDetail.setText(item.getNewsContent());

            String time = DateExt.fromNewsStringToHM(item.getLogtime());
            if (time != null) {
                holder.tvTime.setText(time);
            }

            String adminName = item.getAdminName();
            if (time != null) {
                holder.tvAdminName.setText(adminName);
            }

            if (item.getNewsPic() != null && !item.getNewsPic().isEmpty()) {
                holder.imvNews.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(Constant.URL_NEWS + item.getNewsPic())
                        .into(holder.imvNews);

                holder.imvNews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (listener != null) {
                            listener.onClick(i, Constant.URL_NEWS + item.getNewsPic());
                        }

                    }
                });
            } else {
                holder.imvNews.setVisibility(View.GONE);
                Glide.clear(holder.imvNews);
            }
        }
    }
    
    @Override
    public int getItemViewType(int position) {
        return (isPositionHeader(position)) ? TYPE_HEADER : TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return dataList.get(position).isSection;
    }
    
    @Override
    public int getItemCount() {
        return dataList.size();
    }
    
    public void setOnAdapterClickListener(OnNewsAdapterClickListener listener) {
        this.listener = listener;
    }
    
    public void setDataList(ArrayList<NewsModel> list) {
        if (list == null) {
            return;
        }
        dataList.clear();
        dataList = new ArrayList<>();
        Date dateTemp = null;
        for(NewsModel newsModel : list) {
            String dateTitle = DateExt.custom("yyyy-MM-dd HH:mm", "dd MMMM yyyy", newsModel.getLogtime());
            Date dateModel = DateExt.fromNewsStringToDate(newsModel.getLogtime());
            if (dateTemp == null || !dateTemp.equals(dateModel)) {
                
                dataList.add(new SectionDataModel(null,true,dateTitle));
                dateTemp = dateModel;
            }
            dataList.add(new SectionDataModel(newsModel, false, null));
        }
//        this.dataList = dataList;
        notifyDataSetChanged();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {
        public View view;
        TextView tvTitle;
        TextView tvDetail;
        TextView tvTime;
        TextView tvAdminName;
        ImageView imvNews;
        NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvTitle = itemView.findViewById(R.id.tv_item_news_title);
            tvDetail = itemView.findViewById(R.id.tv_item_news_detail);
            tvTime = itemView.findViewById(R.id.tv_item_news_time);
            tvAdminName = itemView.findViewById(R.id.tv_item_news_admin_name);
            imvNews = itemView.findViewById(R.id.imv_item_news_picture);
        }
        
    }
    
    class NewsHeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        NewsHeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tvTitle = itemView.findViewById(R.id.tv_item_header_news_title);
        }
    }
    
    class SectionDataModel {
        NewsModel newsModel;
        Boolean isSection;
        String sectionTitle;
        
        SectionDataModel(NewsModel newsModel, Boolean isSection, String title) {
            this.newsModel = newsModel;
            this.isSection = isSection;
            this.sectionTitle = title;
        }
    }
}
