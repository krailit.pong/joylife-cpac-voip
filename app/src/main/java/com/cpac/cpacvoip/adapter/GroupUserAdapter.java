package com.cpac.cpacvoip.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;

import java.util.ArrayList;

public class GroupUserAdapter extends RecyclerView.Adapter {
    
    private ArrayList<String> dataList = new ArrayList<>();
    private OnAdapterClickListener listener;
    private int sizes = 0;
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_list_user_group_chat, viewGroup, false);
        return new GroupUserViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
//        String model = dataList.get(i);
//        GroupUserViewHolder holder = (GroupUserViewHolder) viewHolder;
//        holder.view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listener.onClick(i);
//            }
//        });
        GroupUserViewHolder holder = (GroupUserViewHolder) viewHolder;
        String number = String.valueOf(i+1);
        holder.tvNum.setText(number);
    }
    
    @Override
    public int getItemCount() {
        return sizes;//dataList.size();
    }
    
    public void setSizes(int size) {
        this.sizes = size;
        notifyDataSetChanged();
    }
    
//    public void setDataList(ArrayList<String> list) {
//        this.dataList = list;
//        notifyDataSetChanged();
//    }
    
    public void setOnAdapterClickListener(OnAdapterClickListener listener) {
        this.listener = listener;
    }
    
    class GroupUserViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView tvNum;
        GroupUserViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvNum = view.findViewById(R.id.tv_item_user_group_chat_);
        }
        
    }
}
