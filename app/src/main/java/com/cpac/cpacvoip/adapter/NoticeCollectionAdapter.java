package com.cpac.cpacvoip.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;
import com.cpac.cpacvoip.model.BroadcastModel;

import java.util.Collections;
import java.util.List;

import static android.graphics.PorterDuff.Mode.SRC_IN;

public class NoticeCollectionAdapter extends RecyclerView.Adapter {
    
    private Context context;
    private List<BroadcastModel> dataList = Collections.emptyList();
    private OnAdapterClickListener listener;
    private int isPlayIndex = -1;
    
    public NoticeCollectionAdapter(Context context) {
        this.context = context;
    }
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_coll_notice, viewGroup, false);
        return new NoticeCollectionViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {
        BroadcastModel broadcastModel = dataList.get(i);
        NoticeCollectionViewHolder holder = (NoticeCollectionViewHolder) viewHolder;
        holder.tvSender.setText(broadcastModel.getAdminName());
        holder.tvTopic.setText(broadcastModel.getBroadcastTopic());

        if(isPlayIndex == i) {
            holder.imvIcon.setImageResource(R.drawable.ic_pause_notice);
        } else {
            holder.imvIcon.setImageResource(R.drawable.ic_play_notice);
        }
    
        if(broadcastModel.getIsPlay()) {
            holder.tvTopic.setTextColor(ContextCompat.getColor(context, R.color.grey));
            holder.tvSender.setTextColor(ContextCompat.getColor(context, R.color.grey));
            holder.imvIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), SRC_IN);
            holder.imvWave.setColorFilter(ContextCompat.getColor(context, R.color.grey), SRC_IN);
            holder.flCircle.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_circle_grey_40));
        } else {
            holder.tvTopic.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            holder.tvSender.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            holder.imvIcon.setColorFilter(ContextCompat.getColor(context, R.color.white), SRC_IN);
            holder.imvWave.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), SRC_IN);
            holder.flCircle.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_circle_blue_40));
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(i);
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return dataList.size();
    }
    
    public void setDataList(List<BroadcastModel> list) {
        this.dataList = list;
        notifyDataSetChanged();
    }
    
    public void setOnAdapterClickListener(OnAdapterClickListener listener) {
        this.listener = listener;
    }
    
    public void setOnPlayAudio(int isPlayIndex) {
        this.isPlayIndex = isPlayIndex;
        notifyDataSetChanged();
    }
    
    class NoticeCollectionViewHolder extends RecyclerView.ViewHolder {
        TextView tvTopic;
        ImageView imvIcon;
        ImageView imvWave;
        TextView tvSender;
        FrameLayout flCircle;
    
        NoticeCollectionViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTopic = itemView.findViewById(R.id.tvTopic);
            tvSender = itemView.findViewById(R.id.tv_item_coll_notice_sender_name);
            imvIcon = itemView.findViewById(R.id.icon_play_pause_coll_notice);
            flCircle = itemView.findViewById(R.id.fl_play_pause_coll_notice);
            imvWave = itemView.findViewById(R.id.icon_coll_notice_wave);
        }
        
    }
}
