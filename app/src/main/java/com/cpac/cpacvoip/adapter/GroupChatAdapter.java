package com.cpac.cpacvoip.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.delegate.OnGroupChatAdapterClickListener;
import com.cpac.cpacvoip.qb.db.QbUsersDbManager;
import com.cpac.cpacvoip.qb.utils.dialog.QbUsersHolder;
import com.cpac.cpacvoip.util.Constant;
import com.cpac.cpacvoip.util.DateExt;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;

public class GroupChatAdapter extends RecyclerView.Adapter {
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private Context context;
    private ArrayList<QBChatMessage> dataList = new ArrayList<>();
    private OnGroupChatAdapterClickListener listener;
    private MediaPlayer mPlayer;
    private int selectedPlayMedia = -1;
    
    public GroupChatAdapter(Context context) {
        this.context = context;
    }
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        if (type == VIEW_TYPE_MESSAGE_RECEIVED) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_chat_admin_audio, viewGroup, false);
            return new GroupChatReceivedViewHolder(view);
        } else if (type == VIEW_TYPE_MESSAGE_SENT) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_chat_client_audio, viewGroup, false);
            return new GroupChatSendViewHolder(view);
        }
    
        return null;
    }
    
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        QBChatMessage message = dataList.get(i);
//        holder.view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listener.onClick(i);
//            }
//        });
        
        switch (viewHolder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_RECEIVED:
                GroupChatReceivedViewHolder holder = (GroupChatReceivedViewHolder) viewHolder;
                holder.bind(message);
                if(selectedPlayMedia == i) {
                    holder.imvPlay.setImageResource(R.drawable.ic_pause_notice);
                } else {
                    holder.imvPlay.setImageResource(R.drawable.ic_play_notice);
                }
                holder.imvPlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onClick(i, dataList.get(i));
                        }
                    }
                });
                break;
            case VIEW_TYPE_MESSAGE_SENT:
                GroupChatSendViewHolder holder2 = (GroupChatSendViewHolder) viewHolder;
                holder2.bind(message);
                if(selectedPlayMedia == i) {
                    holder2.imvClientPlay.setImageResource(R.drawable.ic_pause_notice);
                } else {
                    holder2.imvClientPlay.setImageResource(R.drawable.ic_play_notice);
                }
                holder2.imvClientPlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onClick(i, dataList.get(i));
                        }
                    }
                });
                break;
                default:break;
        }
    }
    
    @Override
    public int getItemCount() {
        return dataList.size();
    }
    
    @Override
    public int getItemViewType(int position) {
        QBChatMessage model = dataList.get(position);
        if(isReceivedMsg(model)) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_SENT;
        }
    }
    
    public void addData(QBChatMessage msg) {
        this.dataList.add(msg);
//        notifyDataSetChanged();
        this.notifyItemInserted(dataList.size() - 1);
    }
    
    public void addDatas(ArrayList<QBChatMessage> messages) {
        this.dataList.addAll(messages);
    
        this.notifyItemInserted(dataList.size() - 1);
//        notifyDataSetChanged();
    }
    
    public void addReconnectDatas(ArrayList<QBChatMessage> messages) {
        this.dataList.clear();
        this.addDatas(messages);
    }
    
    public void setDataList(ArrayList<QBChatMessage> list) {
        this.dataList = list;
        notifyDataSetChanged();
    }
    
    public int getSelectedPlayMedia() {
        return selectedPlayMedia;
    }
    
    public void setSelectedPlayMedia(int selectedPlayMedia) {
        this.selectedPlayMedia = selectedPlayMedia;
    }
    
    public void setOnAdapterClickListener(OnGroupChatAdapterClickListener listener) {
        this.listener = listener;
    }
    
    class GroupChatReceivedViewHolder extends RecyclerView.ViewHolder {
        View view;
        LinearLayout llAudio;
        TextView tvAudioTime;
        TextView tvAudioUser;
        LinearLayout llLeft;
        TextView tvLeftTime;
        TextView tvLeftUser;
        TextView tvLeftBody;
        ImageView imvPlay;
        ImageView imvAttach;
        
        GroupChatReceivedViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            llAudio = view.findViewById(R.id.ll_item_chat_audio);
            tvAudioTime = view.findViewById(R.id.tv_item_chat_audio_time);
            tvAudioUser = view.findViewById(R.id.tv_item_chat_audio_user);
            imvPlay = view.findViewById(R.id.imv_chat_audio_play);
            
            llLeft = view.findViewById(R.id.ll_item_chat_left);
            tvLeftTime = view.findViewById(R.id.tv_item_chat_left_time);
            tvLeftUser = view.findViewById(R.id.tv_item_chat_left_user);
            tvLeftBody = view.findViewById(R.id.tv_item_chat_left_body);
            imvAttach = view.findViewById(R.id.imv_item_chat_left_attach);
        }
        
        void bind(QBChatMessage message) {
            String senderName = "";
            try {
                if (message.getSenderId() != null) {
                    senderName = QbUsersHolder.getInstance().getUserById(message.getSenderId()).getFullName();
                }else {
                    // if message.getSenderId()==null then it will surely be this user.
                    senderName = QbUsersDbManager.getInstance(context).getUserById(message.getSenderId()).getFullName();
                }
            } catch (Exception e) {}
            long timeSent = message.getDateSent();
            tvLeftTime.setText(DateExt.toTimeHMFromQBDateSent(timeSent));
            tvAudioTime.setText(DateExt.toTimeHMFromQBDateSent(timeSent));
            if(senderName != null && !senderName.isEmpty()) {
                tvLeftUser.setText(senderName);
                tvAudioUser.setText(senderName);
            } else {
                tvLeftUser.setText("");
                tvAudioUser.setText("");
            }
            if(message.getBody() != null && message.getBody() != "null") {
                llLeft.setVisibility(View.VISIBLE);
                tvLeftBody.setText(message.getBody());
            } else {
                llLeft.setVisibility(View.GONE);
                tvLeftBody.setText("");
            }
            
            if(message.getAttachments() != null && message.getAttachments().size() > 0) {
            
//                setAttachment(message);
    
                for (QBAttachment attachment : message.getAttachments()) {
                    String fileId = attachment.getId();
                    String type = attachment.getType();
                    if (type.contains("audio")) {
                        llAudio.setVisibility(View.VISIBLE);
//                        try {
//
//                        } catch (Exception e) {
//                            llAudio.setVisibility(View.GONE);
//                        }
                    } else {
                        llAudio.setVisibility(View.GONE);
                    }
    
                    if ((type.contains("photo") || type.contains("image")) && attachment.getId() != null) {
                        imvAttach.setVisibility(View.VISIBLE);
                        try {
                            Glide.with(context).load(Constant.QBPATH_URL + attachment.getId()).into(imvAttach);
                        } catch (Exception e) {}
                    } else {
                        imvAttach.setVisibility(View.GONE);
                    }
                }
                
            } else {
                llAudio.setVisibility(View.GONE);
                imvAttach.setVisibility(View.GONE);
            }
            
        }
        
    }
    
    class GroupChatSendViewHolder extends RecyclerView.ViewHolder {
        View view;
        
        TextView tvRightTime;
        TextView tvRightUser;
        ImageView imvClientPlay;
        GroupChatSendViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
//            llRight = view.findViewById(R.id.ll_item_chat_right);
            tvRightTime = view.findViewById(R.id.tv_item_chat_client_audio_time);
            tvRightUser = view.findViewById(R.id.tv_item_chat_client_audio_user);
            imvClientPlay = view.findViewById(R.id.imv_chat_client_audio_play);
        }
    
        void bind(QBChatMessage message) {
            String senderName = "-";
            try {
                if (message.getSenderId() != null) {
                    senderName = QbUsersHolder.getInstance().getUserById(message.getSenderId()).getFullName();
                }else {
                    // if message.getSenderId()==null then it will surely be this user.
                    senderName = QbUsersDbManager.getInstance(context).getUserById(getUserId()).getFullName();
                }
            } catch (Exception e) {}
            long timeSent = message.getDateSent();
            tvRightTime.setText(DateExt.toTimeHMFromQBDateSent(timeSent));
        
            if(senderName != null && !senderName.isEmpty()) {
                tvRightUser.setText(senderName);
            }
            if(message.getAttachments() != null && message.getAttachments().size() > 0) {
                setAttachment(message);
            } else {
                //imvAttached.setVisibility(View.GONE);
                //imvAttached.setImageDrawable(null);
            }
        }
        
    }
    
    public void setAttachment(QBChatMessage message) {
        for (QBAttachment attachment : message.getAttachments()) {
            String fileId = attachment.getId();
            String type = attachment.getType();
            if ((type.equals("audio")) && attachment.getUrl() != null) {
                try {
//                    MediaPlayer mPlayer = new MediaPlayer();
//                    mPlayer.setDataSource(attachment.getUrl());
//                    mPlayer.prepareAsync();
//                    mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                        @Override
//                        public void onPrepared(MediaPlayer mp) {
//                            mPlayer.start();
//                        }
//                    });
                    
                
                } catch (Exception e) {}
            }
        }
    }
    
    private Boolean isReceivedMsg(QBChatMessage message) {
        try {
            return !message.getSenderId().equals(getUserId());
        } catch (Exception e) {
            return true;
        }
    }
    
    private Integer getUserId() {
        QBUser sender = QBChatService.getInstance ().getUser();
        return sender.getId();
    }
    
    /*private void downloadFileFromId(String id) {
        // download a file
        QBContent.downloadFile(id).performAsync(new QBEntityCallback<InputStream>() {
            @Override
            public void onSuccess(InputStream inputStream, Bundle params) {
                // process file
                Log.w("CPAC", "onSuccess: bundle params => " + params);
            
                try {
                    String filePath = context.getExternalFilesDir(null) + "/CPAC/" + id + "_" + DateExt.getToday().getTime() + ".mp3";
                
                    byte[] buffer = new byte[inputStream.available()];
                    inputStream.read(buffer);
                
                    File targetFile = new File(filePath);
                    targetFile.getParentFile().mkdirs();
                    targetFile.createNewFile();
                    OutputStream outStream = new FileOutputStream(targetFile);
                    outStream.write(buffer);
                    outStream.close();
                    outStream.flush();
                
                    mPlayer = new MediaPlayer();
                    mPlayer.setDataSource(context, Uri.parse(filePath));
                    mPlayer.prepareAsync();
                    mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mPlayer.start();
                        }
                    });
                
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
    
                    Log.e("CPAC", "prepare() failed");
                } finally {
                
                }
            }
        
            @Override
            public void onError(QBResponseException errors) {
                // errors
                Log.w("CPAC", "onError: download file error => " + errors);
            }
        });
    }*/
}
