package com.cpac.cpacvoip.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;
import com.cpac.cpacvoip.model.BroadcastModel;
import com.cpac.cpacvoip.util.DateExt;

import java.util.Collections;
import java.util.List;

import static android.graphics.PorterDuff.Mode.SRC_IN;

public class NoticeAdapter extends RecyclerView.Adapter {
    
    private Context context;
    private List<BroadcastModel> dataList = Collections.emptyList();
    private OnAdapterClickListener listener;
    private int isPlayIndex = -1;
    
    public NoticeAdapter(Context context) {
        this.context = context;
    }
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_list_notice, viewGroup, false);
        return new NoticeAdapter.NoticeViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        BroadcastModel broadcastModel = dataList.get(i);
        NoticeAdapter.NoticeViewHolder holder = (NoticeAdapter.NoticeViewHolder) viewHolder;
        holder.tvTitle.setText(broadcastModel.getBroadcastTopic());
        holder.tvSender.setText(broadcastModel.getAdminName());
        holder.tvDate.setText(DateExt.fromBroadcastToDate(broadcastModel.getLogtime()));
        holder.tvTime.setText(DateExt.fromBroadcastToTime(broadcastModel.getLogtime()));
        if(isPlayIndex == i) {
            holder.imvIcon.setImageResource(R.drawable.ic_pause_notice);
        } else {
            holder.imvIcon.setImageResource(R.drawable.ic_play_notice);
        }
        
        if(broadcastModel.getIsPlay()) {
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.grey));
            holder.tvSender.setTextColor(ContextCompat.getColor(context, R.color.grey));
            holder.tvDate.setTextColor(ContextCompat.getColor(context, R.color.grey));
            holder.tvTime.setTextColor(ContextCompat.getColor(context, R.color.grey));
            holder.imvIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), SRC_IN);
            holder.flIcon.setBackgroundColor(ContextCompat.getColor(context, R.color.light_smoke));
    
        } else {
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            holder.tvSender.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            holder.tvDate.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            holder.tvTime.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            holder.imvIcon.setColorFilter(ContextCompat.getColor(context, R.color.white), SRC_IN);
            holder.flIcon.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        }
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(i);
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return dataList.size();
    }
    
    public void setOnPlayAudio(int isPlayIndex) {
        this.isPlayIndex = isPlayIndex;
        notifyDataSetChanged();
    }
    
    public void setDataList(List<BroadcastModel> dataList) {
        if(dataList != null) {
            this.dataList = dataList;
            notifyDataSetChanged();
        }
    }
    
    class NoticeViewHolder extends RecyclerView.ViewHolder {
        public View view;
        TextView tvTitle;
        TextView tvSender;
        TextView tvDate;
        TextView tvTime;
        ImageView imvIcon;
        FrameLayout flIcon;
    
        NoticeViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvTitle = itemView.findViewById(R.id.tv_item_notice_title);
            tvSender = itemView.findViewById(R.id.tv_item_notice_sender_name);
            tvDate = itemView.findViewById(R.id.tv_item_notice_date);
            tvTime = itemView.findViewById(R.id.tv_item_notice_time);
            imvIcon = itemView.findViewById(R.id.icon_play_pause_notice);
            flIcon = itemView.findViewById(R.id.fl_play_pause_notice);
        }
        
    }
    
    public void setOnAdapterClickListener(OnAdapterClickListener listener) {
        this.listener = listener;
    }
}
