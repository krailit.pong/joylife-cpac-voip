package com.cpac.cpacvoip.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;
import com.cpac.cpacvoip.model.FaqModel;

import java.util.ArrayList;

public class FaqAdapter extends RecyclerView.Adapter {
    
    private ArrayList<FaqModel> dataList = new ArrayList<>();
    private OnAdapterClickListener listener;
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_list_faq, viewGroup, false);
        return new FaqViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        FaqModel model = dataList.get(i);
        FaqViewHolder holder = (FaqViewHolder) viewHolder;
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(i);
            }
        });
        
    }
    
    @Override
    public int getItemCount() {
        return dataList.size();
    }
    
    public void setDataList(ArrayList<FaqModel> list) {
        this.dataList = list;
        notifyDataSetChanged();
    }
    
    public void setOnAdapterClickListener(OnAdapterClickListener listener) {
        this.listener = listener;
    }
    
    class FaqViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public TextView tvTitle;
        public TextView tvDetail;
        FaqViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvTitle = itemView.findViewById(R.id.tv_item_faq_title);
            tvDetail = itemView.findViewById(R.id.tv_item_faq_detail);
        }
        
    }
}


