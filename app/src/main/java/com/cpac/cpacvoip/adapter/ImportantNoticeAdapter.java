package com.cpac.cpacvoip.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;

import java.util.ArrayList;

public class ImportantNoticeAdapter extends RecyclerView.Adapter {
    private ArrayList<String> dataList = new ArrayList<>();
    private OnAdapterClickListener listener;
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_list_important_notice, viewGroup, false);
        return new ImportantNoticeViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        //String model = dataList.get(i);
        ImportantNoticeViewHolder holder = (ImportantNoticeViewHolder) viewHolder;
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(i);
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return 3; // mockupz
//        return dataList.size();
    }
    
    public void setDataList(ArrayList<String> list) {
        this.dataList = list;
        notifyDataSetChanged();
    }
    
    public void setOnAdapterClickListener(OnAdapterClickListener listener) {
        this.listener = listener;
    }
    
    class ImportantNoticeViewHolder extends RecyclerView.ViewHolder {
        View view;
        ImportantNoticeViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
        }
        
    }
}
