package com.cpac.cpacvoip.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.delegate.OnAdapterClickListener;
import com.cpac.cpacvoip.model.ChatRoomModel;
import com.cpac.cpacvoip.util.Prefz;

import java.util.ArrayList;

public class RoomAdapter extends RecyclerView.Adapter {
    private ArrayList<ChatRoomModel> dataList = new ArrayList<>();
    private OnAdapterClickListener listener;
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_list_room, viewGroup, false);
        return new RoomViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        ChatRoomModel model = dataList.get(i);
        RoomViewHolder holder = (RoomViewHolder) viewHolder;
        holder.tvTitle.setText(model.getRoomName());
        if(model.isHasNewMessage()) {
            holder.imvDot.setVisibility(View.VISIBLE);
        } else {
            holder.imvDot.setVisibility(View.GONE);
        }
        
        holder.view.setOnClickListener(v -> listener.onClick(i));
    }
    
    @Override
    public int getItemCount() {
        return dataList.size();
    }
    
    public void setDataList(ArrayList<ChatRoomModel> list) {
        this.dataList = list;
        ArrayList<String> dialogsUnread = Prefz.getDialogUnread();
        if(dialogsUnread != null) {
            for(ChatRoomModel model: list) {
                for (String id : dialogsUnread) {
                    if(model.getRoomid().equals(id)) {
                        model.setHasNewMessage(true);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }
    
    public void setOnAdapterClickListener(OnAdapterClickListener listener) {
        this.listener = listener;
    }
    
    class RoomViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView tvTitle;
        ImageView imvDot;
        RoomViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvTitle = itemView.findViewById(R.id.tv_item_chatroom_title);
            imvDot = itemView.findViewById(R.id.imv_list_room_dot);
        }
        
    }
}
