package com.cpac.cpacvoip.services;

import com.cpac.cpacvoip.model.AdminIdResponse;
import com.cpac.cpacvoip.model.BroadcastModel;
import com.cpac.cpacvoip.model.ChatRoomModel;
import com.cpac.cpacvoip.model.RequestBroadcastPlay;
import com.cpac.cpacvoip.model.RequestDriverNo;
import com.cpac.cpacvoip.model.LoginChatRoomModel;
import com.cpac.cpacvoip.model.NewsModel;
import com.cpac.cpacvoip.model.OrgChartChainDto;
import com.cpac.cpacvoip.model.RequestBroadcast;
import com.cpac.cpacvoip.model.RequestRoomId;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

public interface APIInterface {
    
    @POST("api/api_broadcast")
    Observable<ArrayList<BroadcastModel>> postBroadcast(@Body RequestBroadcast requestBroadcast);
    
    @POST("api/api_news")
    Observable<ArrayList<NewsModel>> getNews(@Body RequestDriverNo request);
    
    @GET("api/api_chatroom")
    Observable<ArrayList<ChatRoomModel>> getChatRoom();
    
    @POST("api/api_chatroom")
    Observable<AdminIdResponse> postChatRoom(@Body RequestRoomId request);
    
    @POST("api_broadcast_flag")
    Observable<ResponseBody> postPlayBroadcast(@Body() RequestBroadcastPlay request);
    
    @POST("api/api_scglogin")
    Observable<ArrayList<LoginChatRoomModel>> postLogin(@Body OrgChartChainDto request);

    @POST("api/api_sos")
    Observable<ResponseBody> postSos(@Body() JsonObject jsonObject);

    @POST("api/api_joingroupchat")
    Observable<ResponseBody> postJoinGroupChat(@Body() JsonObject jsonObject);
    
}
