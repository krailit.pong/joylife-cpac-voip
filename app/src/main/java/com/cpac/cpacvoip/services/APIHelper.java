package com.cpac.cpacvoip.services;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.cpac.cpacvoip.model.AdminIdResponse;
import com.cpac.cpacvoip.model.BroadcastModel;
import com.cpac.cpacvoip.model.ChatRoomBus;
import com.cpac.cpacvoip.model.ChatRoomModel;
import com.cpac.cpacvoip.model.LoginChatRoomBus;
import com.cpac.cpacvoip.model.LoginChatRoomModel;
import com.cpac.cpacvoip.model.NewsModel;
import com.cpac.cpacvoip.model.OrgChartChainDto;
import com.cpac.cpacvoip.model.RequestBroadcast;
import com.cpac.cpacvoip.model.RequestBroadcastPlay;
import com.cpac.cpacvoip.model.RequestDriverNo;
import com.cpac.cpacvoip.model.RequestRoomId;
import com.cpac.cpacvoip.util.Prefz;
import com.cpac.cpacvoip.util.RxBus;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class APIHelper {
    
    public static Observable<ArrayList<BroadcastModel>> GetBroadcast(String driverNo) {
        return APIService.getAPIInterface()
                .postBroadcast(new RequestBroadcast(driverNo))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
    
    public static Observable<ArrayList<NewsModel>> GetNews(String driverNo) {
        return APIService.getAPIInterface()
                .getNews(new RequestDriverNo(driverNo))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
    
    public static Observable<ArrayList<ChatRoomModel>> GetChatRoom() {
        return APIService.getAPIInterface()
                .getChatRoom()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
    
    public static Observable<AdminIdResponse> PostChatRoom(RequestRoomId request) {
        return APIService.getAPIInterface()
                .postChatRoom(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
    
    public static Observable<ResponseBody> PostPlayBroadcast(RequestBroadcastPlay request) {
        return APIService.getAPIInterface()
                .postPlayBroadcast(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
    
    public static Observable<ArrayList<LoginChatRoomModel>> Login(OrgChartChainDto request) {
        return APIService.getAPIInterface()
                .postLogin(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<ResponseBody> Sos(JsonObject request) {
        return APIService.getAPIInterface()
                .postSos(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<ResponseBody> ForceJoinGroupChat(JsonObject request) {
        return APIService.getAPIInterface()
                .postJoinGroupChat(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static void apiLoginStarter(OrgChartChainDto request, Handler.Callback callback) {
        APIHelper.Login(request).subscribe(new Subscriber<ArrayList<LoginChatRoomModel>>() {
            @Override
            public void onCompleted() {
                Log.w("CPAC", "onNext: Send login Complete ...");
            }

            @Override
            public void onError(Throwable e) {
                Log.w("CPAC", "onNext: Send login Error ..." + e.getLocalizedMessage());
                callback.handleMessage(new Message());
            }

            @Override
            public void onNext(ArrayList<LoginChatRoomModel> list) {
                Log.w("CPAC", "onNext: Send login Next ..." + list);
                callback.handleMessage(new Message());
            }
        });
    }
    
    public static void apiLogin(OrgChartChainDto request) {
        APIHelper.Login(request).subscribe(new Subscriber<ArrayList<LoginChatRoomModel>>() {
            @Override
            public void onCompleted() {
                Log.w("CPAC", "onNext: Send login Complete ...");
            }
        
            @Override
            public void onError(Throwable e) {
                Log.w("CPAC", "onNext: Send login Error ..." + e.getLocalizedMessage());
            }
        
            @Override
            public void onNext(ArrayList<LoginChatRoomModel> list) {
                Log.w("CPAC", "onNext: Send login Next ..." + list);
            
                if (list != null && list.size() > 0) {
                    // Trick to save login
                    RxBus.getInstance().post(new LoginChatRoomBus(list));
    
//                    ArrayList<ChatRoomModel> chatRoomModels = new ArrayList<>();
//                    for (LoginChatRoomModel model :
//                            list) {
//                        if (model == null) {
//                            continue;
//                        }
//                        ChatRoomModel roomModel = new ChatRoomModel();
//                        roomModel.setRoomid(model.getRoomid());
//                        roomModel.setRoomName(model.getRoomName());
//                        roomModel.setUserId(model.getUrserid());
//
//                        if (!model.getUrserid().equals("") && !model.getRoomid().equals("")) {
//                            chatRoomModels.add(roomModel);
//                        }
//                    }
//                    RxBus.getInstance().post(new ChatRoomBus(chatRoomModels));


                    RxBus.getInstance().post(new ChatRoomBus(arrangeChatRoomModel(list)));

                }
            }
        });
    }

    public static void apiLoginToGetRoomList(OrgChartChainDto request) {
        APIHelper.Login(request).subscribe(new Subscriber<ArrayList<LoginChatRoomModel>>() {
            @Override
            public void onCompleted() {
                Log.w("RoomView", "onCompleted: apiLoginToGetRoomList Complete ...");
            }

            @Override
            public void onError(Throwable e) {
                Log.w("RoomView", "onError: apiLoginToGetRoomList Error ..." + e.getLocalizedMessage());
            }

            @Override
            public void onNext(ArrayList<LoginChatRoomModel> list) {
                Log.w("RoomView", "onNext: apiLoginToGetRoomList Next ..." + list);

                if (list != null && list.size() > 0) {
                    // Trick to save login
                    RxBus.getInstance().post(new ChatRoomBus(arrangeChatRoomModel(list)));
                }
            }
        });
    }

    private static ArrayList<ChatRoomModel> arrangeChatRoomModel(ArrayList<LoginChatRoomModel> list) {
        ArrayList<ChatRoomModel> chatRoomModels = new ArrayList<>();
        for (LoginChatRoomModel model : list) {
            if (model == null) {
                continue;
            }
            ChatRoomModel roomModel = new ChatRoomModel();
            roomModel.setRoomid(model.getRoomid());
            roomModel.setRoomName(model.getRoomName());
            roomModel.setUserId(model.getUrserid());
            roomModel.setQbAdminId(model.getQbAdminId());
            if (!model.getUrserid().equals("") && !model.getRoomid().equals("")) {
                chatRoomModels.add(roomModel);
            }
        }
        return chatRoomModels;
    }
}
