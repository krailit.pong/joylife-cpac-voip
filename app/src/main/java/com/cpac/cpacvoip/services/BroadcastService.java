package com.cpac.cpacvoip.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cpac.cpacvoip.MyBroadcastReceiver;
import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.Restarter;

public class BroadcastService extends Service {
    public static final String channelId = "1234";
    public static final int channelNumber = 1234;
    private MyBroadcastReceiver mReceiver;
    
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY; //this defines this service to stay alive
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
    
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("restartservice");
        broadcastIntent.setClass(this, Restarter.class);
        this.sendBroadcast(broadcastIntent);
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        mReceiver = new MyBroadcastReceiver();
        registerReceiver(mReceiver, new IntentFilter("cpac.joylife.voip.login"));
        Log.w("CPAC", "onCreate: start service");

        // Run this Service runs forever on foreground thread....
        startForegroundService();
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            String description = getString(R.string.title) + " กำลังทำงาน";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void startForegroundService() {
        Log.d("CPAC", "Start foreground service.");

        // Create notification default intent.
        Intent intent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // Create notification builder.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId);

        // Make notification show big text.
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(getString(R.string.title));
        bigTextStyle.bigText(getString(R.string.content));
        // Set big text style.
        builder.setStyle(bigTextStyle);
        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon(R.mipmap.ic_launcher_cpac_round);
        Bitmap largeIconBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo);
        builder.setLargeIcon(largeIconBitmap);
        // Make the notification max priority.
        builder.setPriority(Notification.PRIORITY_MAX);
        // Make head-up notification.
        builder.setFullScreenIntent(pendingIntent, true);

        // Build the notification.
        Notification notification = builder.build();

        // Start foreground service.
        createNotificationChannel();
        startForeground(channelNumber, notification);
    }
    
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

//    public class ServiceBinder extends Binder {
//        public BroadcastService getService() {
//            return BroadcastService.this;
//        }
//    }
//
//    private ServiceConnection m_serviceConnection = new ServiceConnection() {
//        public void onServiceConnected(ComponentName className, IBinder service) {
//            m_service = ((BroadcastService.ServiceBinder)service).getService();
//        }
//
//        public void onServiceDisconnected(ComponentName className) {
//            m_service = null;
//        }
//    };
}
