package com.cpac.cpacvoip.services;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

<<<<<<< HEAD
import com.cpac.cpacvoip.model.LoginChatRoomModel;
import com.cpac.cpacvoip.model.OrgChartChainDto;
<<<<<<< HEAD
=======
import com.cpac.cpacvoip.qb.services.CallService;
>>>>>>> parent of ce92f00... Update 2.0.2
=======
import com.cpac.cpacvoip.qb.services.CallService;
>>>>>>> parent of 586391c... 2.0.4 error
import com.cpac.cpacvoip.qb.utils.Consts;
import com.quickblox.users.model.QBUser;

@SuppressLint("Registered")
public class ExitActivity  extends Activity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
<<<<<<< HEAD
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finishAndRemoveTask();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finishAndRemoveTask();
        }else {
            setVisible(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        dtoModel = Prefz.extractDTO();
        signupStarter(this);
    }

    private void signupStarter(Context context) {
        QBSettings.getInstance().setEnablePushNotification(true);
        QBSettings.getInstance().setSubscribePushStrategy(SubscribePushStrategy.ALWAYS);
        SubscribeService.subscribeToPushes(context, true);

        /*
         * STABLE
         */
//            ExitActivity.exitApplication(context);

        QBUser user = Mockupz.getQBUserLogin();
        if(dtoModel != null) {
            user.setLogin("driver" + dtoModel.getDriverNo());
            user.setEmail("driver" + dtoModel.getDriverNo() + "@commuone.com");
            user.setFullName(dtoModel.getDriverName());
        }

        QBUsers.signUp(user).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                Log.i("CPAC", "onSuccess: Created new user");
                StringifyArrayList<String> userTags = new StringifyArrayList<>();
                userTags.add(Constant.ROOM_NAME);
                qbUser.setPassword(Constant.USER_PASSW);
                qbUser.setTags(userTags);
                signInStarter(qbUser, context);
            }

            @Override
            public void onError(QBResponseException e) {
                if (e.getHttpStatusCode() == Consts.ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                    Log.w("CPAC", "onError: Has exist user");
                    new Handler().postDelayed(() -> signInStarter(user, context), 100);
                } else {
                    Crashlytics.log(Log.ERROR, "QBUsers.signUp", e.getLocalizedMessage());
                }
            }
        });
    }

    private void signInStarter(QBUser user, Context context) {
        //showProgressDialog();
        Log.i("CPAC", "Do QBUsers.signIn");
        QBUsers.signIn(user).performAsync( new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                /* Tie Fix */
                qbUser.setPassword(user.getPassword());
                qbUser.setId(user.getId());
                userForSave = qbUser;
                loginStarter(qbUser, context);

                FirebaseMessaging.getInstance().subscribeToTopic("overall")
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Log.d("TAG", "subscribeToTopic success.");
                            }else {
                                Log.d("TAG", "subscribeToTopic Failed.");

                            }
                        });
            }

            @Override
            public void onError(QBResponseException e) {
                Crashlytics.log(Log.ERROR, "QBUsers.signIn", e.getLocalizedMessage());
                Log.w("CPAC", "onError: Signin error => " + e.getLocalizedMessage() + ", code => " + e.getHttpStatusCode());
            }
        });
    }


    public void loginStarter(QBUser qbUser, Context context) {
        if (dtoModel != null) {
            dtoModel.setQbUserId(qbUser.getId().toString());
            String firebaseToken = FirebaseInstanceId.getInstance().getToken();
            dtoModel.setFbToken(firebaseToken);
            Prefz.saveFBToken(firebaseToken);
            Prefz.saveDTOObject(dtoModel);
            saveUserData(userForSave);

//            Intent tempIntent = new Intent(context, CallService.class);
//            PendingIntent pendingIntent = PendingIntent.getService(context, 0, tempIntent, 0);
//            CallService.start(context, qbUser, pendingIntent);
            createCallService(qbUser);

            APIHelper.Login(dtoModel).subscribe(new Subscriber<ArrayList<LoginChatRoomModel>>() {
                @Override
                public void onCompleted() {
                    Log.w("CPAC", "onNext: Send login Complete ...");
                }

                @Override
                public void onError(Throwable e) {
                    Log.w("CPAC", "onNext: Send login Error ..." + e.getLocalizedMessage());
                    finishAndRemoveTask();
                }

                @Override
                public void onNext(ArrayList<LoginChatRoomModel> list) {
                    Log.w("CPAC", "onNext: APIHelper.Login Success... " + list.size());
                    sendBroadcastToMainAcitivity(context);
                    new Handler().postDelayed(() -> {
//                        ExitActivity.restartActivity(context);
                        finishAndRemoveTask();
                    }, 1000);
                }
            });
        }else {
            Log.e("CPAC", "loginToChat: dtoModel null");
        }
    }

    private void saveUserData(QBUser qbUser) {
        if(qbUser != null) {
            SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
            sharedPrefsHelper.saveQbUser(qbUser);
            try {
                sharedPrefsHelper.save(Consts.PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
            } catch (Exception ignored) {}
        }
    }

    private void sendBroadcastToMainAcitivity(Context context) {
        Intent intetReset = new Intent(KEY_RESET);
        context.sendBroadcast(intetReset);
=======
        finishAndRemoveTask();
>>>>>>> parent of ce92f00... Update 2.0.2
    }

    private void createCallService(QBUser qbUser) {
        Log.i("CPAC", "createCallService with " + qbUser.getFullName());
//        Intent tempIntent = new Intent(this, CallService.class);
//        PendingIntent pendingIntent = createPendingResult(Consts.EXTRA_LOGIN_RESULT_CODE, tempIntent, 0);
//        CallService.start(this, qbUser, pendingIntent);
    }

    public static void exitApplication(Context context) {
        Log.w("CPAC", "onReceive: exitApplication");

        Intent intent = new Intent(context, ExitActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.putExtra("EXIT", true);
        context.startActivity(intent);

//        new Handler().postDelayed(() -> {
//        int id= android.os.Process.myPid();
//        android.os.Process.killProcess(id);
//        }, 300);
    }

<<<<<<< HEAD
    public static void startExitActivity(Context context) {
        Log.w("CPAC", "onReceive: startExitActivity");
=======
    public static void restartActivity(Context context) {
        Log.w("CPAC", "onReceive: restartActivity");
>>>>>>> parent of ce92f00... Update 2.0.2

        Intent intent = new Intent(context, ExitActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(intent);
<<<<<<< HEAD
    }

=======

//        new Handler().postDelayed(() -> {
//            Intent intentAc = new Intent(context, MainActivity.class);
//            intentAc.setAction(Intent.ACTION_MAIN);
//            intentAc.addCategory(Intent.CATEGORY_LAUNCHER);
////            intentAc.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
//            context.startActivity(intentAc);
//        }, 2000);
    }


    public static void loginBackground(Context context) {
        Log.w("CPAC", "onReceive: restartApplication");

        Intent intent = new Intent(context, ExitActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(intent);

        new Handler().postDelayed(() -> {
        }, 2000);
    }
>>>>>>> parent of ce92f00... Update 2.0.2
//
//    private static void clearPreferences() {
//        try {
//            // clearing app data
//            Runtime runtime = Runtime.getRuntime();
//            runtime.exec("pm clear com.cpac.cpacvoip");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
