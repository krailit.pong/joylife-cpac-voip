package com.cpac.cpacvoip.util;

import android.annotation.SuppressLint;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressLint("SimpleDateFormat")
public class DateExt {
    
    public static String toTimeHMFromQBDateSent(long time) {
         /// QB time millis in 1970
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        DateFormat sdfToday = new SimpleDateFormat("HH:mm");
        DateFormat sdfDateOnly = new SimpleDateFormat("dd/MM/yyyy");
//        SimpleDateFormat sdfDay = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        Date messageTime = new Date(time * 1000);
        Date currentTime = Calendar.getInstance().getTime();


        String date1 = sdfDateOnly.format(messageTime);
        String date2 = sdfDateOnly.format(currentTime);
//        Log.i("CPAC", "messageTime = " + date1);
//        Log.i("CPAC", "currentTime = " + date2);
//        Log.i("CPAC", "equals = " + date1.equals(date2));

        if (date1.equals(date2)) {
//            Log.i("CPAC", "Date1 is equal to Date2");
            return "วันนี้ " + sdfToday.format(messageTime);
        }else {
            return sdf.format(messageTime);
        }

    }
    
    public static String toTimeHMString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        return format.format(date);
    }
    
    public static Date fromString(String strDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            return format.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }
    
    public static String fromNewsStringToHM(String strDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date;
        try {
            date = format.parse(strDate);
            SimpleDateFormat fmtOut = new SimpleDateFormat("HH:mm");
            return fmtOut.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static Date fromNewsStringToDate(String strDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date;
        try {
            date = format.parse(strDate);
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMMM yyyy");
            return fmtOut.parse(fmtOut.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }
    
    @SuppressLint("SimpleDateFormat")
    public static String custom(String strFormatFrom, String strFormatTo, String strDate) {
        SimpleDateFormat format = new SimpleDateFormat(strFormatFrom); //"yyyy-MM-dd HH:mm"
        Date date;
        try {
            date = format.parse(strDate);
        } catch (ParseException e) {
            date = new Date();
        }
         SimpleDateFormat fmtOut = new SimpleDateFormat(strFormatTo); //"dd MMM yyyy"
        return fmtOut.format(date);
    }
    
    @SuppressLint("SimpleDateFormat")
    public static String fromBroadcastToDate(String strDate) {
        return DateExt.custom("yyyy-MM-dd HH:mm:ss", "dd MMMM yyyy",strDate);
    }
    
    public static String fromBroadcastToTime(String strDate) {
        return DateExt.custom("yyyy-MM-dd HH:mm:ss", "HH:mm",strDate);
    }
    
    public static Date getToday() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static String formateMilliSeccond(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);

        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        //      return  String.format("%02d Min, %02d Sec",
        //                TimeUnit.MILLISECONDS.toMinutes(milliseconds),
        //                TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
        //                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));

        // return timer string
        return finalTimerString;
    }
}
