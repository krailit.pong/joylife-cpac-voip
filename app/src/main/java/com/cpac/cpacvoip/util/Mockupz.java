package com.cpac.cpacvoip.util;

import android.content.Context;

import com.cpac.cpacvoip.model.BroadcastModel;
import com.cpac.cpacvoip.model.NewsModel;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.users.model.QBUser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Mockupz {
    
    public static QBUser getQBUserLogin() {
        StringifyArrayList<String> userTags = new StringifyArrayList<>();
        userTags.add(Constant.ROOM_NAME);
        QBUser user = new QBUser();
        
        /* Tie Fix FOR CAPC 123 */
//        userTags.add("scg31596");
//        user.setLogin("123");
//        user.setFullName("Driver1");
//        user.setEmail("123@commuone.com");
//        user.setPassword("scg12345");
        /* -------- */

//        user.setFullName("Driver1");
//        user.setEmail("123@commuone.com");

//        userTags.add("scgroom1");
//        user.setLogin("usertest1");
//        user.setPassword(Consts.DEFAULT_USER_PASSWORD);

//        user2.setLogin("123");
//        user2.setFullName("Driver1");
//        user2.setId(75266967);
        
        /* Cannot use login and chat with difference user */
        user.setTags(userTags);
        //user.setLogin(Constant.USER_LOGIN);
        user.setPassword(Constant.USER_PASSW);
//        user.setId(Constant.USER_ID);
        return user;
    }
    
    public static QBUser getQBAdminToCall() {
        QBUser user = new QBUser();
        StringifyArrayList<String> userTags = new StringifyArrayList<>();
        userTags.add("scg31596");
        userTags.add(Constant.ROOM_NAME);
        
        user.setTags(userTags);
        user.setLogin(Constant.ADMIN_LOGIN);
        user.setFullName(Constant.ADMIN_FULLNAME);
        user.setId(Constant.ADMIN_ID);
//        user.setPassword("scg12345");
        return user;
    }
    
    public static ArrayList<NewsModel> getNewsList(Context context) {
        ArrayList<NewsModel> arrayList = new ArrayList<>();
        String json = getJsonFrom(context, "news.json");
        if (json != null) {
            arrayList = GsonUtil.listFromJson(json, NewsModel.class);
        }
        return arrayList;
    }
    
    public static List<BroadcastModel> getBroadcastList(Context context) {
        ArrayList<BroadcastModel> arrayList = new ArrayList<>();
        String json = getJsonFrom(context, "broadcast.json");
        if (json != null) {
            arrayList = GsonUtil.listFromJson(json, BroadcastModel.class);
        }
        return arrayList;
    }
    
    private static String getJsonFrom(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("mockupz/" + fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return json;
    }
}
