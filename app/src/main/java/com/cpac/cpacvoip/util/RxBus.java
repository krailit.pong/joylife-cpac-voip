package com.cpac.cpacvoip.util;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ConcurrentHashMap;

public class RxBus {
    
    private static RxBus instance;
    
    public static synchronized  RxBus getInstance() {
        if (null == instance) {
            instance = new RxBus();
        }
        return instance;
    }
    
    private  RxBus() {
    }
    
    private ConcurrentHashMap<Object, Object> keepMap = new ConcurrentHashMap<>();
    
    @SuppressWarnings("rawtypes")
    private ConcurrentHashMap<Object, CustomObservable> observeMap = new ConcurrentHashMap<>();
    
    public void addObserver(Class<?> clazz, Observer observer) {
        String tag = clazz.getSimpleName();
        CustomObservable observable = observeMap.get(tag);
        if (observable != null) {
            observable.deleteObserver(observer);
        }
        observable = new CustomObservable();
        observeMap.put(tag, observable);
        observable.addObserver(observer);
        if (keepMap.get(tag) != null) {
            observable.notifyObservers(keepMap.get(tag));
        }
    }
    
    public void removeObserver(Class<?> clazz, Observer observer) {
        String tag = clazz.getSimpleName();
        CustomObservable observable = observeMap.get(tag);
        if (observable != null) {
            keepMap.remove(tag);
            observable.deleteObserver(observer);
        }
    }
    
    public void post(Object any) {
        String tag = any.getClass().getSimpleName();
        CustomObservable observable = observeMap.get(tag);
        if (observable != null) {
            observable.notifyObservers(any);
        }
        keepMap.put(tag, any);
        
    }
    
    
    class CustomObservable extends Observable {
        @Override
        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
        }
    }
}
