package com.cpac.cpacvoip.util;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class GsonUtil {
    
    public static <T> ArrayList<T> listFromJson(String json, Class<T> clazz) {
        ArrayList<T> arrayList = new ArrayList<>();
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<T>>(){}.getType();
        ArrayList<LinkedTreeMap<String,T>> treeMapArrayList = gson.fromJson(json, type);
        for(LinkedTreeMap<String,T> treeMap : treeMapArrayList) {
            String treeJson = gson.toJson(treeMap);
            arrayList.add(gson.fromJson(treeJson, clazz));
        }
        return arrayList;
    }
}
