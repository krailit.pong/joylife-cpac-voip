package com.cpac.cpacvoip.util;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;

import com.cpac.cpacvoip.model.BroadcastModel;
import com.cpac.cpacvoip.model.ChatRoomModel;
import com.cpac.cpacvoip.model.OrgChartChainDto;
import com.cpac.cpacvoip.qb.utils.Consts;
import com.cpac.cpacvoip.qb.utils.SharedPrefsHelper;
import com.cpac.cpacvoip.qb.utils.chat.ChatHelper;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.quickblox.users.QBUsers;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.cpac.cpacvoip.util.Constant.KEY_BROADCAST_PLAY;
import static com.cpac.cpacvoip.util.Constant.KEY_DIALOG_UNREAD;
import static com.cpac.cpacvoip.util.Constant.KEY_DTO_OBJECT;
import static com.cpac.cpacvoip.util.Constant.KEY_LOGIN_TIME;
import static com.cpac.cpacvoip.util.Constant.KEY_PROFILE_IMAGE;

public class Prefz {

    public static void saveFBToken(String token) {
        SharedPrefsHelper.getInstance().save(Consts.PREF_GCM_TOKEN, token);
    }

    public static String getFBToken() {
        return SharedPrefsHelper.getInstance().get(Consts.PREF_GCM_TOKEN) != null
                ? SharedPrefsHelper.getInstance().get(Consts.PREF_GCM_TOKEN) : "";
    }

    public static void saveDTOObject(OrgChartChainDto model) {
        String json = new Gson().toJson(model);
        SharedPrefsHelper.getInstance().save(KEY_DTO_OBJECT, json);
    }

    public static String getDTOObject() {
        return SharedPrefsHelper.getInstance().get(KEY_DTO_OBJECT);
    }

    public static OrgChartChainDto extractDTO() {
        try {
            String json = getDTOObject();
            return new Gson().fromJson(json, OrgChartChainDto.class);

        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getDriverNo() {
        try {
            String json = getDTOObject();
            OrgChartChainDto model = new Gson().fromJson(json, OrgChartChainDto.class);
            return model.getDriverNo();

        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void saveProfileImage(Bitmap bitmap) {
        String imgSave = Utilz.BitMapToString(bitmap);
        if (imgSave != null) {
            SharedPrefsHelper.getInstance().save(KEY_PROFILE_IMAGE + getDriverNo(), imgSave);
        } else {
            Log.w("CPAC", "saveProfileImage: Failed, Bitmap to String = null");
        }
    }

    public static Bitmap getProgileImage() {
        try {
            String imgGet = SharedPrefsHelper.getInstance().get(KEY_PROFILE_IMAGE + getDriverNo());
            return Utilz.StringToBitMap(imgGet);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void saveBroadcastPlay(List<BroadcastModel> dataList) {
        try {
            ArrayList<String> listPlay = new ArrayList<>();
            for (BroadcastModel model :
                    dataList) {
                if(model.getIsPlay()) {
                    listPlay.add(model.getBroadcastFilename());
                }
            }
            String keyz = KEY_BROADCAST_PLAY + "_" + getDriverNo();
            String json = new Gson().toJson(listPlay);
            SharedPrefsHelper.getInstance().save(keyz, json);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void checkBroadcastPlay(ArrayList<BroadcastModel> dataList) {
        try {
            String keyz = KEY_BROADCAST_PLAY + "_" + getDriverNo();
            String json = SharedPrefsHelper.getInstance().get(keyz);
            Type type = new TypeToken<ArrayList<String>>() {}.getType();
            ArrayList<String> listPlay = new Gson().fromJson(json, type);
            if(listPlay == null || listPlay.isEmpty()) {return;}
            for (BroadcastModel model :
                    dataList) {
                for (String idz :
                        listPlay) {
                    if(model.getBroadcastFilename().equals(idz)) {
                        model.setPlay(true);
                    }
                }
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveDialogUnread(ArrayList<ChatRoomModel> roomModels) {
        ArrayList<String> dialogs = new ArrayList<>();
        for (ChatRoomModel model : roomModels) {
            if(model.isHasNewMessage()){ dialogs.add(model.getRoomid()); }
        }

        String keyz = KEY_DIALOG_UNREAD + "_" + getDriverNo();
        String json = new Gson().toJson(dialogs);
        SharedPrefsHelper.getInstance().save(keyz, json);
    }

    public static ArrayList<String> getDialogUnread() {
        try {
            String keyz = KEY_DIALOG_UNREAD + "_" + getDriverNo();
            String json = SharedPrefsHelper.getInstance().get(keyz);
            Type type = new TypeToken<ArrayList<String>>() {}.getType();
            ArrayList<String> dialogs = new Gson().fromJson(json, type);
            if (dialogs != null && !dialogs.isEmpty()) {
                return dialogs;
//                for (String id : dialogs) {
//                    if(id == null || id.isEmpty()) {continue;}
//                    Log.w("CPAC", "dialog update =>> " + id);
//                    RxBus.getInstance().post(new ChatRoomUpdateBus(id));
//                }
            } else {
                return null;
            }
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void saveLoginTime() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateNowText =  format.format(new Date());
        String keyz = KEY_LOGIN_TIME + "_" + getDriverNo();
        Crashlytics.log(Log.ASSERT,"Login_OK","saveLoginTime for " + dateNowText);
        SharedPrefsHelper.getInstance().save(keyz, dateNowText);
    }

    public static void checkLoginTime() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String dateFromPrefs = SharedPrefsHelper.getInstance().get(KEY_LOGIN_TIME + "_" + getDriverNo());
            String dateNowText =  format.format(new Date());
            if(!dateFromPrefs.equals(dateNowText)) {
                Crashlytics.log(Log.ASSERT, "auto_logout", dateFromPrefs);
                clear_logout();

                int id= android.os.Process.myPid();
                android.os.Process.killProcess(id);
            }
        } catch(Exception e) {
//            e.printStackTrace();
//            Crashlytics.logException(e);
        }
    }

    public static boolean isSameDay() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String dateFromPrefs = SharedPrefsHelper.getInstance().get(KEY_LOGIN_TIME + "_" + getDriverNo());
            String dateNowText =  format.format(new Date());
            if (dateFromPrefs == null || dateFromPrefs.equals("")) {
                return true;
            }

            return dateFromPrefs.equals(dateNowText);
        } catch(Exception e) {
//            e.printStackTrace();
            return true;
        }
    }

    public static void clear_logout() {
        Log.w("CPAC", "onReceive: clear_logout");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("overall")
                .addOnCompleteListener(task -> {

        });

        new Thread(() -> {
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                ChatHelper.getInstance().logout();
                QBUsers.signOut();
            } catch (Exception e) {
                Crashlytics.logException(e);
            }

        }).start();


        SharedPrefsHelper.getInstance().removeQbUser();
        SharedPrefsHelper.getInstance().delete(KEY_DTO_OBJECT);
        SharedPrefsHelper.getInstance().clearAllData();
    }
}
