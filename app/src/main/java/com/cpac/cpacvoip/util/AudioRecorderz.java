package com.cpac.cpacvoip.util;

import android.content.Context;
import android.media.MediaRecorder;

import com.quickblox.ui.kit.chatmessage.adapter.media.recorder.AudioRecorder;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class AudioRecorderz {
    
    final Context context;
//    MediaRecorder recorder = new MediaRecorder();
    AudioRecorder qbRecorder;
    
    String path = "";
    
    /**
     * Creates a new audio recording at the given path (relative to root of SD card).
     */
    public AudioRecorderz(Context context) {
        this.context = context;
        this.path = sanitizePath();
        
    }
    
    public String getPath() { return path; }
    
    private String sanitizePath() {
        String path = "/Record/record_" + Prefz.getDriverNo() + new Date().getTime() + ".mp3";

        // Tie Remove .mp3 string because backend cant process it.
        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        return context.getExternalFilesDir(null) + path;
    }
    
    /**
     * Starts a new recording.
     */
    public void start() throws IOException {
        String state = android.os.Environment.getExternalStorageState();
        if(!state.equals(android.os.Environment.MEDIA_MOUNTED))  {
            throw new IOException("SD Card is not mounted.  It is " + state + ".");
        }
        
        // make sure the directory we plan to store the recording in exists
        File directory = new File(path).getParentFile();
        if (!directory.exists() && !directory.mkdirs()) {
            throw new IOException("Path to file could not be created.");
        }
        
        qbRecorder = AudioRecorder.newBuilder()
                .setFilePath(getPath())
                .setDuration(Constant.MAX_SOUND_AUTO_SEC)
                .setAudioSource(MediaRecorder.AudioSource.MIC)
                .setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                .setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
                .build();
        
        qbRecorder.startRecord();
//
//        recorder = new MediaRecorder();
//        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
//        recorder.setOutputFile(path);
//        recorder.prepare();
//        recorder.start();
    }
    
    /**
     * Stops a recording that has been previously started.
     */
    public void stop() {
        try {
            qbRecorder.stopRecord();
            
//            recorder.stop();
//            recorder.release();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public boolean isRecording() {
        try {
            if(qbRecorder != null) {
                return qbRecorder.isRecording();
            } else {
                return false;
            }
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public void cancel() {
        try {
            if (qbRecorder != null)
                qbRecorder.cancelRecord();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
