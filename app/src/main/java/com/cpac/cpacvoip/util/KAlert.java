package com.cpac.cpacvoip.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.services.APIHelper;
import com.google.gson.JsonObject;

import okhttp3.ResponseBody;
import rx.Observer;

public class KAlert {
    
    public static void showSos(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dlg_sos);
        dialog.setCancelable(true);
    
        Button btnYes = dialog.findViewById(R.id.alert_dialog_btn_ok);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String driverNo = Prefz.getDriverNo();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("driver_no", driverNo);
                APIHelper.Sos(jsonObject).subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        Log.d("Sos", "onCompleted");
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Sos", "onError " + e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {

                    }
                });
            }
        });

        Button btnCancel = dialog.findViewById(R.id.alert_dialog_btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        
        dialog.show();
        
        /*new AlertDialog.Builder(context)
                .setMessage("ยืนยันการแจ้งขอความช่วยเหลือฉุกเฉิน")
                .setPositiveButton("ใช่", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
        
                    }
                })
                .setNegativeButton("ไม่ใช่", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    
                    }
                })
        
                .create()
                .show();*/
    }
    
    public static void showChoose(Context context, DialogInterface.OnClickListener listener) {
        String[] listItems = {"ถ่ายภาพ","เลือกรูปภาพ"};
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
//        mBuilder.setTitle("Choose an item");
        mBuilder.setItems(listItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onClick(dialogInterface, i);
                dialogInterface.dismiss();
            }
        });
    
        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }
    
    public static void show(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("OK", listener);
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showLogout(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("ยกเลิก", null)
                .setPositiveButton("ตกลง", listener);
        AlertDialog alert = builder.create();
        alert.show();
    }
}
