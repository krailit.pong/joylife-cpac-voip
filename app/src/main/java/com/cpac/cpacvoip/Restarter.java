package com.cpac.cpacvoip;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.cpac.cpacvoip.services.BroadcastService;

public class Restarter extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("CPAC", "Service tried to stop");
        Toast.makeText(context, "มีการปิด Service Cpac Smart Talk กำลังเริ่มใหม่", Toast.LENGTH_SHORT).show();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, BroadcastService.class));
        } else {
            context.startService(new Intent(context, BroadcastService.class));
        }
    }
}
