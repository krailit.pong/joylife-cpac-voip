package com.cpac.cpacvoip;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.cpac.cpacvoip.qb.models.SampleConfigs;
import com.cpac.cpacvoip.qb.utils.ConfigUtils;
import com.cpac.cpacvoip.qb.utils.Consts;
import com.cpac.cpacvoip.qb.utils.GcmConsts;
import com.cpac.cpacvoip.util.Constant;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.core.QBHttpConnectionConfig;
import com.quickblox.core.ServiceZone;

import io.fabric.sdk.android.Fabric;
import java.io.IOException;
import java.util.Calendar;

public class MyApplication extends Application {
    
    private static MyApplication instance;
    private static SampleConfigs sampleConfigs;
    MyBroadcastReceiver mReceiver;
    private BroadcastReceiver pushBroadcastReceiver;

    public static MyApplication getInstance() {
        return instance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        QBSettings.getInstance().init(getApplicationContext(), Constant.APP_ID, Constant.AUTH_KEY, Constant.AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(Constant.ACCOUNT_KEY);
        /* For Enterprise */
        QBSettings.getInstance().setEndpoints(Constant.API_DOMAIN, Constant.CHAT_DOMAIN, ServiceZone.PRODUCTION);
        QBSettings.getInstance().setZone(ServiceZone.PRODUCTION);
        QBSettings.getInstance().setAutoCreateSession(true);

        QBHttpConnectionConfig.setConnectTimeout(60000);
        QBHttpConnectionConfig.setReadTimeout(60000);

        /* For Enterprise */
        mReceiver = new MyBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter("cpac.joylife.voip.login");
        getApplicationContext().registerReceiver(mReceiver, intentFilter);
        
        //initSampleConfigs();
        initMidnightAlarm();

        pushBroadcastReceiver = new PushBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(pushBroadcastReceiver,
                new IntentFilter(GcmConsts.ACTION_NEW_GCM_EVENT));

    }

    private class PushBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent

            String message = intent.getStringExtra(GcmConsts.EXTRA_GCM_MESSAGE);
            Log.v("CPAC", "Received broadcast " + intent.getAction() + " with data: " + message);

//            requestBuilder.setSkip(skipRecords = 0);
//            loadDialogsFromQb(true, true);
        }
    }
    
    @Override
    public void onTerminate() {
        unregisterReceiver(mReceiver);
        super.onTerminate();
    }
    
    private void initSampleConfigs() {
        try {
            sampleConfigs = ConfigUtils.getSampleConfigs(Consts.SAMPLE_CONFIG_FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void initMidnightAlarm() {
        AlarmManager alarmMgr;
        PendingIntent alarmIntent;
        
        alarmMgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), MidnightReceiver.class);
        intent.putExtra("requestcode", Constant.REQUEST_ALARM_MIDNIGHT);
        alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), Constant.REQUEST_ALARM_MIDNIGHT, intent, 0);
    
        // Set the alarm to start at approximately 2:00 p.m.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 1);
        // With setInexactRepeating(), you have to use one of the AlarmManager interval
        // constants--in this case, AlarmManager.INTERVAL_DAY.
        alarmMgr.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, alarmIntent);
    }
    
    public static SampleConfigs getSampleConfigs() {
        return sampleConfigs;
    }


    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    private static boolean activityVisible;
}
