package com.cpac.cpacvoip.model;

import com.google.gson.annotations.SerializedName;

public class RequestRoomId {
    
    @SerializedName("room_id")
    private String roomId;
    
    public RequestRoomId(String roomId) {
        this.roomId = roomId;
    }
}
