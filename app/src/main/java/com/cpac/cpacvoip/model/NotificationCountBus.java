package com.cpac.cpacvoip.model;

public class NotificationCountBus {
    private Integer page_id;

    public NotificationCountBus(Integer id) {
        page_id = id;
    }
    
    public Integer getNotificationPage() {
        return page_id;
    }
}
