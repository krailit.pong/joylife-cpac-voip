package com.cpac.cpacvoip.model;

import com.google.gson.annotations.SerializedName;

public class RequestBroadcast {
    @SerializedName("driver_no")
    public String driverNo;
    
    public RequestBroadcast(String driverNo) {
        this.driverNo = driverNo;
    }
}
