package com.cpac.cpacvoip.model;

import com.google.gson.annotations.SerializedName;

public class RequestBroadcastPlay {
    
    @SerializedName("driver_no")
    private String driverNo;
    
    @SerializedName("broadcast_no")
    private String broadcastNo;
    
    
    public RequestBroadcastPlay(String driverNo, String broadcastNo) {
        this.driverNo = driverNo;
        this.broadcastNo = broadcastNo;
    }
}
