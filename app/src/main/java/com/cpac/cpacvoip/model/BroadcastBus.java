package com.cpac.cpacvoip.model;

import java.util.ArrayList;

public class BroadcastBus {
    private ArrayList<BroadcastModel> dataList = new ArrayList<>();
    
    public BroadcastBus(ArrayList<BroadcastModel> list) {
        dataList = list;
    }
    
    public ArrayList<BroadcastModel> getDataList() {
        return dataList;
    }
}
