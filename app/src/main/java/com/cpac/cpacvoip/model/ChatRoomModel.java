package com.cpac.cpacvoip.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChatRoomModel implements Serializable {
    
    @SerializedName("room_id")
    private String roomid;
    
    @SerializedName("user_id")
    private String userId;

    @SerializedName("qb_admin_id")
    private String qbAdminId;
    
    @SerializedName("room_name")
    private String roomName;
    
    @SerializedName("has_new_message")
    private boolean hasNewMessage = false;


    public String getRoomid() { return roomid; }
    public void setRoomid(String value) { this.roomid = value; }
    
    public String getUserId() { return userId; }
    public void setUserId(String value) { this.userId = value; }
    
    public String getRoomName() { return roomName; }
    public void setRoomName(String value) { this.roomName = value; }
    
    public boolean isHasNewMessage() {
        return hasNewMessage;
    }
    
    public void setHasNewMessage(boolean hasNewMessage) {
        this.hasNewMessage = hasNewMessage;
    }

    public String getQbAdminId() {
        return qbAdminId;
    }

    public void setQbAdminId(String qbAdminId) {
        this.qbAdminId = qbAdminId;
    }
}
