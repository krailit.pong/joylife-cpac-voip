package com.cpac.cpacvoip.model;

import java.util.ArrayList;

public class ChatRoomBus {
    private ArrayList<ChatRoomModel> dataList = new ArrayList<>();
    
    public ChatRoomBus(ArrayList<ChatRoomModel> list) {
        dataList = list;
    }
    
    public ArrayList<ChatRoomModel> getDataList() {
        return dataList;
    }
}
