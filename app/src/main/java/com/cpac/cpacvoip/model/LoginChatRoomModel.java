package com.cpac.cpacvoip.model;

import com.google.gson.annotations.SerializedName;

public class LoginChatRoomModel {
    @SerializedName("room_id")
    private String roomid;
    @SerializedName("room_name")
    private String roomName;
    @SerializedName("urser_id")
    private String userid;
    @SerializedName("qb_admin_id")
    private String qbAdminId;

    public String getRoomid() { return roomid; }
    public void setRoomid(String value) { this.roomid = value; }
    
    public String getRoomName() { return roomName; }
    public void setRoomName(String value) { this.roomName = value; }
    
    public String getUrserid() { return userid; }
    public void setUrserid(String value) { this.userid = value; }

    public String getQbAdminId() {
        return qbAdminId;
    }

    public void setQbAdminId(String qbAdminId) {
        this.qbAdminId = qbAdminId;
    }
}
