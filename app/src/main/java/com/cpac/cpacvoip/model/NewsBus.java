package com.cpac.cpacvoip.model;

import java.util.ArrayList;

public class NewsBus {
    private ArrayList<NewsModel> dataList = new ArrayList<>();
    
    public NewsBus(ArrayList<NewsModel> list) {
        dataList = list;
    }
    
    public ArrayList<NewsModel> getDataList() {
        return dataList;
    }
}
