package com.cpac.cpacvoip.model;

public class ChatRoomUpdateBus {
    
    private String dialogId;
    
    public ChatRoomUpdateBus(String dialogId) {
        this.dialogId = dialogId;
    }
    
    public String getDialogId() {
        return dialogId;
    }
}
