package com.cpac.cpacvoip.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrgChartChainDto implements Serializable {
    @SerializedName("driver_no")
    @Expose
    private String driverNo;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("truck_no")
    @Expose
    private String truckNo;
    @SerializedName("plant_no")
    @Expose
    private String plantNo;
    @SerializedName("plant_name")
    @Expose
    private String plantName;
    @SerializedName("sub_dispatch_no")
    @Expose
    private String subDispatchNo;
    @SerializedName("sub_distpatch_name")
    @Expose
    private String subDistpatchName;
    @SerializedName("dispatch_no")
    @Expose
    private String dispatchNo;
    @SerializedName("dispatch_name")
    @Expose
    private String dispatchName;
    @SerializedName("department_no")
    @Expose
    private String departmentNo;
    @SerializedName("department_name")
    @Expose
    private String departmentName;
    @SerializedName("factory_no")
    @Expose
    private String factoryNo;
    @SerializedName("factory_name")
    @Expose
    private String factoryName;
    @SerializedName("section_no")
    @Expose
    private String sectionNo;
    @SerializedName("section_name")
    @Expose
    private String sectionName;
    @SerializedName("vendor_no")
    @Expose
    private String vendorNo;
    @SerializedName("vendor_name")
    @Expose
    private String vendorName;
    @SerializedName("qb_user_id")
    @Expose
    private String qbUserId;
    @SerializedName("fb_token")
    @Expose
    private String fb_token;
    
    
    public String getDriverNo() {
        return driverNo;
    }
    
    public void setDriverNo(String driverNo) {
        this.driverNo = driverNo;
    }
    
    public String getDriverName() {
        return driverName;
    }
    
    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }
    
    public String getTruckNo() {
        return truckNo;
    }
    
    public void setTruckNo(String truckNo) {
        this.truckNo = truckNo;
    }
    
    public String getPlantNo() {
        return plantNo;
    }
    
    public void setPlantNo(String plantNo) {
        this.plantNo = plantNo;
    }
    
    public String getPlantName() {
        return plantName;
    }
    
    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }
    
    public String getSubDispatchNo() {
        return subDispatchNo;
    }
    
    public void setSubDispatchNo(String subDispatchNo) {
        this.subDispatchNo = subDispatchNo;
    }
    
    public String getSubDistpatchName() {
        return subDistpatchName;
    }
    
    public void setSubDistpatchName(String subDistpatchName) {
        this.subDistpatchName = subDistpatchName;
    }
    
    public String getDispatchNo() {
        return dispatchNo;
    }
    
    public void setDispatchNo(String dispatchNo) {
        this.dispatchNo = dispatchNo;
    }
    
    public String getDispatchName() {
        return dispatchName;
    }
    
    public void setDispatchName(String dispatchName) {
        this.dispatchName = dispatchName;
    }
    
    public String getDepartmentNo() {
        return departmentNo;
    }
    
    public void setDepartmentNo(String departmentNo) {
        this.departmentNo = departmentNo;
    }
    
    public String getDepartmentName() {
        return departmentName;
    }
    
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    
    public String getFactoryNo() {
        return factoryNo;
    }
    
    public void setFactoryNo(String factoryNo) {
        this.factoryNo = factoryNo;
    }
    
    public String getFactoryName() {
        return factoryName;
    }
    
    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }
    
    public String getSectionNo() {
        return sectionNo;
    }
    
    public void setSectionNo(String sectionNo) {
        this.sectionNo = sectionNo;
    }
    
    public String getSectionName() {
        return sectionName;
    }
    
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
    
    public String getVendorNo() {
        return vendorNo;
    }
    
    public void setVendorNo(String vendorNo) {
        this.vendorNo = vendorNo;
    }
    
    public String getVendorName() {
        return vendorName;
    }
    
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }
    
    public String getQbUserId() {
        return qbUserId;
    }
    
    public void setQbUserId(String qbUserId) {
        this.qbUserId = qbUserId;
    }
    
    public String getFbToken() {
        return fb_token;
    }
    
    public void setFbToken(String fb_token) {
        this.fb_token = fb_token;
    }
}
