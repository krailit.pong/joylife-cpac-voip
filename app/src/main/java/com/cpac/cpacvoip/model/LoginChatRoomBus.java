package com.cpac.cpacvoip.model;

import java.util.ArrayList;

public class LoginChatRoomBus {
    private ArrayList<LoginChatRoomModel> dataList = new ArrayList<>();
    
    public LoginChatRoomBus(ArrayList<LoginChatRoomModel> list) {
        for (LoginChatRoomModel model:
             list) {
            if (model != null) {
                dataList.add(model);
            }
        }
    }
    
    public ArrayList<LoginChatRoomModel> getDataList() {
        return dataList;
    }
}
