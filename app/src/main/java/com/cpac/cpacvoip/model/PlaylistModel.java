package com.cpac.cpacvoip.model;

import com.quickblox.chat.model.QBChatMessage;

public class PlaylistModel {
    
    public QBChatMessage message;
    public int position;
    
    public PlaylistModel(QBChatMessage message, int position) {
        this.message = message;
        this.position = position;
    }
}
