package com.cpac.cpacvoip.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdminIdResponse {
    
    @SerializedName("admin_id")
    @Expose
    private String adminId;
    
    public String getAdminId() {
        return adminId;
    }
    
    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }
}
