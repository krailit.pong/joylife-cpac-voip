package com.cpac.cpacvoip.model;

import com.google.gson.annotations.SerializedName;

public class RequestDriverNo {
    
    @SerializedName("driver_no")
    private String driverNo;
    
    public RequestDriverNo(String driverNo) {
        this.driverNo = driverNo;
    }
}
