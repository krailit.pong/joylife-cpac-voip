package com.cpac.cpacvoip.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BroadcastModel {
    
    @SerializedName("broadcast_no")
    @Expose
    private String broadcastNo;
    @SerializedName("broadcast_topic")
    @Expose
    private String broadcastTopic;
    @SerializedName("broadcast_filename")
    @Expose
    private String broadcastFilename;
    @SerializedName("admin_no")
    @Expose
    private String adminNo;
    @SerializedName("allunit")
    @Expose
    private String allunit;
    @SerializedName("production")
    @Expose
    private String production;
    @SerializedName("distribution")
    @Expose
    private String distribution;
    @SerializedName("vendor")
    @Expose
    private String vendor;
    @SerializedName("department_no")
    @Expose
    private String departmentNo;
    @SerializedName("dispatch_no")
    @Expose
    private String dispatchNo;
    @SerializedName("sub_dispatch_no")
    @Expose
    private String subDispatchNo;
    @SerializedName("plant_no")
    @Expose
    private String plantNo;
    @SerializedName("factory_no")
    @Expose
    private String factoryNo;
    @SerializedName("section_no")
    @Expose
    private String sectionNo;
    @SerializedName("vendor_no")
    @Expose
    private String vendorNo;
    @SerializedName("logtime")
    @Expose
    private String logtime;
    @SerializedName("admin_name")
    @Expose
    private String adminName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("main_admin")
    @Expose
    private String mainAdmin;
    @SerializedName("createdby")
    @Expose
    private String createdby;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("controlcenter_role")
    @Expose
    private String controlcenterRole;
    @SerializedName("production_role")
    @Expose
    private String productionRole;
    @SerializedName("distribution_role")
    @Expose
    private String distributionRole;
    @SerializedName("vendor_role")
    @Expose
    private String vendorRole;
    
    private Boolean isPlay = false;
    
    public String getBroadcastNo() {
        return broadcastNo;
    }
    
    public void setBroadcastNo(String broadcastNo) {
        this.broadcastNo = broadcastNo;
    }
    
    public String getBroadcastTopic() {
        return broadcastTopic;
    }
    
    public void setBroadcastTopic(String broadcastTopic) {
        this.broadcastTopic = broadcastTopic;
    }
    
    public String getBroadcastFilename() {
        return broadcastFilename;
    }
    
    public void setBroadcastFilename(String broadcastFilename) {
        this.broadcastFilename = broadcastFilename;
    }
    
    public String getAdminNo() {
        return adminNo;
    }
    
    public void setAdminNo(String adminNo) {
        this.adminNo = adminNo;
    }
    
    public String getAllunit() {
        return allunit;
    }
    
    public void setAllunit(String allunit) {
        this.allunit = allunit;
    }
    
    public String getProduction() {
        return production;
    }
    
    public void setProduction(String production) {
        this.production = production;
    }
    
    public String getDistribution() {
        return distribution;
    }
    
    public void setDistribution(String distribution) {
        this.distribution = distribution;
    }
    
    public String getVendor() {
        return vendor;
    }
    
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
    
    public String getDepartmentNo() {
        return departmentNo;
    }
    
    public void setDepartmentNo(String departmentNo) {
        this.departmentNo = departmentNo;
    }
    
    public String getDispatchNo() {
        return dispatchNo;
    }
    
    public void setDispatchNo(String dispatchNo) {
        this.dispatchNo = dispatchNo;
    }
    
    public String getSubDispatchNo() {
        return subDispatchNo;
    }
    
    public void setSubDispatchNo(String subDispatchNo) {
        this.subDispatchNo = subDispatchNo;
    }
    
    public String getPlantNo() {
        return plantNo;
    }
    
    public void setPlantNo(String plantNo) {
        this.plantNo = plantNo;
    }
    
    public String getFactoryNo() {
        return factoryNo;
    }
    
    public void setFactoryNo(String factoryNo) {
        this.factoryNo = factoryNo;
    }
    
    public String getSectionNo() {
        return sectionNo;
    }
    
    public void setSectionNo(String sectionNo) {
        this.sectionNo = sectionNo;
    }
    
    public String getVendorNo() {
        return vendorNo;
    }
    
    public void setVendorNo(String vendorNo) {
        this.vendorNo = vendorNo;
    }
    
    public String getLogtime() {
        return logtime;
    }
    
    public void setLogtime(String logtime) {
        this.logtime = logtime;
    }
    
    public String getAdminName() {
        return adminName;
    }
    
    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getPhone() {
        return phone;
    }
    
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getMainAdmin() {
        return mainAdmin;
    }
    
    public void setMainAdmin(String mainAdmin) {
        this.mainAdmin = mainAdmin;
    }
    
    public String getCreatedby() {
        return createdby;
    }
    
    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getControlcenterRole() {
        return controlcenterRole;
    }
    
    public void setControlcenterRole(String controlcenterRole) {
        this.controlcenterRole = controlcenterRole;
    }
    
    public String getProductionRole() {
        return productionRole;
    }
    
    public void setProductionRole(String productionRole) {
        this.productionRole = productionRole;
    }
    
    public String getDistributionRole() {
        return distributionRole;
    }
    
    public void setDistributionRole(String distributionRole) {
        this.distributionRole = distributionRole;
    }
    
    public String getVendorRole() {
        return vendorRole;
    }
    
    public void setVendorRole(String vendorRole) {
        this.vendorRole = vendorRole;
    }
    
    public Boolean getIsPlay() {
        return isPlay;
    }
    
    public void setPlay(Boolean play) {
        isPlay = play;
    }
}
