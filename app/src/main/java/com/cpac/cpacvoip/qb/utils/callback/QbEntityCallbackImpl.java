package com.cpac.cpacvoip.qb.utils.callback;

import android.os.Bundle;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;

/**
 * Created by tereha on 15.06.16.
 */
public class QbEntityCallbackImpl<T> implements QBEntityCallback<T> {
    
    public QbEntityCallbackImpl() {
    }

    @Override
    public void onSuccess(T result, Bundle params) {

    }

    @Override
    public void onError(QBResponseException responseException) {

    }
}
