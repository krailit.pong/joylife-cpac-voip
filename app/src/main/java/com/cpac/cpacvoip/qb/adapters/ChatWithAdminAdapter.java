package com.cpac.cpacvoip.qb.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.delegate.OnChatAdminAdapterClickListener;
import com.cpac.cpacvoip.qb.utils.SharedPrefsHelper;
import com.cpac.cpacvoip.qb.utils.dialog.QbUsersHolder;
import com.cpac.cpacvoip.util.Constant;
import com.cpac.cpacvoip.util.DateExt;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

public class ChatWithAdminAdapter extends RecyclerView.Adapter {
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private ArrayList<QBChatMessage> dataList = new ArrayList<>();
    private OnChatAdminAdapterClickListener listener;
    private Context context;
    private int selectedPlayMedia = -1;
    
    public ChatWithAdminAdapter(Context context) {
        this.context = context;
    }
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
//        View view = LayoutInflater.from(viewGroup.getContext())
//                .inflate(R.layout.item_chat_admin_audio, viewGroup, false);
//        return new ChatWithAdminViewHolder(view);
    
        if (type == VIEW_TYPE_MESSAGE_RECEIVED) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_chat_left, viewGroup, false);
            return new ChatReceivedAdminViewHolder(view);
        } else if (type == VIEW_TYPE_MESSAGE_SENT) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_chat_right, viewGroup, false);
            return new ChatSendAdminViewHolder(view);
        }
        
        return null;
    }
    
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        QBChatMessage message = dataList.get(i);
        
        switch (viewHolder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ChatReceivedAdminViewHolder holder = (ChatReceivedAdminViewHolder) viewHolder;
                holder.bind(message);
                if(selectedPlayMedia == i) {
                    holder.imvPlay.setImageResource(R.drawable.ic_pause_notice);
                } else {
                    holder.imvPlay.setImageResource(R.drawable.ic_play_notice);
                }
                holder.imvPlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onClick(i, dataList.get(i));
                        }
                    }
                });
                break;
            case VIEW_TYPE_MESSAGE_SENT:
                ChatSendAdminViewHolder holder2 = (ChatSendAdminViewHolder) viewHolder;
                holder2.bind(message);
                break;
                default:break;
        }
//        if(isReceivedMsg(message)) {
//            holder.llLeft.setVisibility(View.VISIBLE);
//            holder.tvLeftBody.setText(message.getBody());
//        } else {
//            holder.llRight.setVisibility(View.VISIBLE);
//            holder.tvRightBody.setText(message.getBody());
//        }
//        holder.view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //listener.onClick(i);
//            }
//        });
    }
    
    @Override
    public int getItemCount() {
        return dataList.size();
    }
    
    @Override
    public int getItemViewType(int position) {
        QBChatMessage model = dataList.get(position);
    
        if(isReceivedMsg(model)) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_SENT;
        }
    }
    
    public void addList(List<QBChatMessage> qbList) {
        this.dataList.addAll(qbList);
        this.notifyItemInserted(dataList.size() - 1);
    }
    
    public void addData(QBChatMessage msg) {
        this.dataList.add(msg);
//        notifyDataSetChanged();
        this.notifyItemInserted(dataList.size() - 1);
    }
    
    public void setDataList(ArrayList<QBChatMessage> list) {
        this.dataList = list;
        notifyDataSetChanged();
    }
    
    public int getSelectedPlayMedia() {
        return selectedPlayMedia;
    }
    
    public void setSelectedPlayMedia(int selectedPlayMedia) {
        this.selectedPlayMedia = selectedPlayMedia;
    }
    
    public void setOnAdapterClickListener(OnChatAdminAdapterClickListener listener) {
        this.listener = listener;
    }
    
    private void setAttachment(ImageView imageView, View audioView, QBChatMessage message) {


        for (QBAttachment attachment : message.getAttachments()) {
            String fileId = attachment.getId();
            String type = attachment.getType();

            if (type.contains("audio")) {
                if(audioView!=null)audioView.setVisibility(View.VISIBLE);
            } else {
                if(audioView!=null)audioView.setVisibility(View.GONE);
            }
            if ((type.equals("photo") || type.equals("image")) && fileId != null) {
                Log.v("CPAC", "attachment.getData() = " + attachment.getData());
                imageView.setVisibility(View.VISIBLE);
                try {
                    Glide.with(context).load(Constant.QBPATH_URL + fileId).into(imageView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                imageView.setVisibility(View.GONE);
            }
        }
    }
    
    class ChatReceivedAdminViewHolder extends RecyclerView.ViewHolder {
        View view;
        LinearLayout llLeft;
        TextView tvLeftTime;
        TextView tvLeftUser;
        TextView tvLeftBody;
        ImageView imvAttached;
        LinearLayout llAudio;
        TextView tvAudioTime;
        TextView tvAudioUser;
        ImageView imvPlay;
    
        ChatReceivedAdminViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            llLeft = view.findViewById(R.id.ll_item_chat_left);
            tvLeftTime = view.findViewById(R.id.tv_item_chat_left_time);
            tvLeftUser = view.findViewById(R.id.tv_item_chat_left_user);
            tvLeftBody = view.findViewById(R.id.tv_item_chat_left_body);
            imvAttached = view.findViewById(R.id.imv_item_chat_left_attach);
            llAudio = view.findViewById(R.id.ll_item_chat_audio);
            tvAudioTime = view.findViewById(R.id.tv_item_chat_audio_time);
            tvAudioUser = view.findViewById(R.id.tv_item_chat_audio_user);
            imvPlay = view.findViewById(R.id.imv_chat_audio_play);
        }
    
        void bind(QBChatMessage message) {
            String senderName = QbUsersHolder.getInstance().getUserById(message.getSenderId()).getFullName();
            long timeSent = message.getDateSent();
            tvLeftTime.setText(DateExt.toTimeHMFromQBDateSent(timeSent));
            tvAudioTime.setText(DateExt.toTimeHMFromQBDateSent(timeSent));
            if(senderName != null && !senderName.isEmpty()) {
                tvLeftUser.setText(senderName);
                tvAudioUser.setText(senderName);
            }else {
                tvLeftUser.setText("");
                tvAudioUser.setText("");
            }
            if(message.getBody() != null && message.getBody() != "null") {
                llLeft.setVisibility(View.VISIBLE);
                tvLeftBody.setText(message.getBody());
            } else {
                tvLeftBody.setText("");
                llLeft.setVisibility(View.GONE);
            }
            if(message.getAttachments() != null && message.getAttachments().size() > 0) {
                setAttachment(imvAttached, llAudio, message);
            } else {
                llAudio.setVisibility(View.GONE);
                imvAttached.setVisibility(View.GONE);
                imvAttached.setImageDrawable(null);
            }
        }
        
    }
    
    class ChatSendAdminViewHolder extends RecyclerView.ViewHolder {
        View view;
        LinearLayout llRight;
        TextView tvRightTime;
        TextView tvRightUser;
        TextView tvRightBody;
        ImageView imvAttached;
        ChatSendAdminViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            
//            llRight = view.findViewById(R.id.ll_item_chat_right);
            tvRightTime = view.findViewById(R.id.tv_item_chat_right_time);
            tvRightUser = view.findViewById(R.id.tv_item_chat_right_user);
            tvRightBody = view.findViewById(R.id.tv_item_chat_right_body);
            imvAttached = view.findViewById(R.id.imv_item_chat_right_attach);
        }
        
        void bind(QBChatMessage message) {
            String senderName = "-";
            try {
                if (message.getSenderId() != null) {
                    senderName = QbUsersHolder.getInstance().getUserById(message.getSenderId()).getFullName();
                }else {
                    // if message.getSenderId()==null then it will surely be this user.
//                    senderName = QbUsersDbManager.getInstance(context).getUserById(getUserId()).getFullName();
                    QBUser user = SharedPrefsHelper.getInstance().getQbUser();
                    if (user != null)
                        senderName = user.getFullName();
                }
            } catch (Exception e) {}
            long timeSent = message.getDateSent();
            tvRightTime.setText(DateExt.toTimeHMFromQBDateSent(timeSent));
            
            if(senderName != null && !senderName.isEmpty()) {
                tvRightUser.setText(senderName);
            }
            if(message.getBody() != null && message.getBody() != "null") {
                tvRightBody.setText(message.getBody());
            } else {
                tvRightBody.setText("");
            }
            if(message.getAttachments() != null && message.getAttachments().size() > 0) {
                setAttachment(imvAttached, null, message);
            } else {
                imvAttached.setVisibility(View.GONE);
                imvAttached.setImageDrawable(null);
            }
        }
    }
    
    private Boolean isReceivedMsg(QBChatMessage message) {
        try {
            if (message.getRecipientId() == null) {
                return false;
            }
            return message.getRecipientId().equals(getUserId());
        } catch (Exception e) {
            return false;
        }
    }
    
    private Integer getUserId() {
        QBUser sender = QBChatService.getInstance().getUser();
        return sender.getId();
    }
}
