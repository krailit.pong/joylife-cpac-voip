package com.cpac.cpacvoip.qb.utils;

public interface GcmConsts {
    String EXTRA_GCM_MESSAGE = "message";
    String ACTION_NEW_GCM_EVENT = "new-push-event";
    String EMPTY_GCM_MESSAGE = "empty message";
}
