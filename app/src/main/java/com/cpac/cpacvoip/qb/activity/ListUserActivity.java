package com.cpac.cpacvoip.qb.activity;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cpac.cpacvoip.MyApplication;
import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.activity.MainActivity;
import com.cpac.cpacvoip.delegate.OnChatAdminAdapterClickListener;
import com.cpac.cpacvoip.model.ChatRoomModel;
import com.cpac.cpacvoip.qb.adapters.ChatWithAdminAdapter;
import com.cpac.cpacvoip.qb.db.QbUsersDbManager;
import com.cpac.cpacvoip.qb.services.CallService;
import com.cpac.cpacvoip.qb.utils.Consts;
import com.cpac.cpacvoip.qb.utils.PermissionsChecker;
import com.cpac.cpacvoip.qb.utils.PushNotificationSender;
import com.cpac.cpacvoip.qb.utils.WebRtcSessionManager;
import com.cpac.cpacvoip.qb.utils.chat.ChatHelper;
import com.cpac.cpacvoip.qb.utils.dialog.QbChatDialogMessageListenerImp;
import com.cpac.cpacvoip.qb.utils.dialog.QbUsersHolder;
import com.cpac.cpacvoip.util.Constant;
import com.cpac.cpacvoip.services.GPSTracker;
import com.cpac.cpacvoip.util.KAlert;
import com.cpac.cpacvoip.util.Prefz;
import com.cpac.cpacvoip.util.Utilz;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBRTCTypes;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static com.cpac.cpacvoip.util.Constant.REQUEST_IMAGE_CAPTURE;
import static com.cpac.cpacvoip.util.Constant.REQUEST_IMAGE_GALLERY;
import static com.quickblox.chat.model.QBAttachment.IMAGE_TYPE;
import static pl.aprilapps.easyphotopicker.EasyImageConfig.REQ_PICK_PICTURE_FROM_GALLERY;
import static pl.aprilapps.easyphotopicker.EasyImageConfig.REQ_TAKE_PICTURE;

public class ListUserActivity extends BaseActivity  {
    private static final String EXTRA_FILENAME=
            "com.cpac.cpacvoip.EXTRA_FILENAME";
    private static final String FILENAME="cpac.jpeg";
    private String authorities = "com.cpac.cpacvoip";
    private static final String TAG = "AdminList";
    private static final int REQUEST_PICK_FILE = 669;
    private static final int CLICK_NOTI_ADMIN = 2256;
    private static final int QUALITY = 50;
    private ImageButton btnCall;
    private ImageButton btnBack;
    private ImageButton btnSend;
    private RecyclerView rvChat;
    private EditText edtMessageBox;
    private TextView tvTitle;
    private FrameLayout flPreview;
    private ImageView imvPreview;
    private ImageView imvAttachment;
    private ImageButton imbClose;

    private Uri imageUri;

    enum UserType {
        USER1, USER2, USER3, USER4
    }
    
    private QBUser currentUser;
    private QbUsersDbManager dbManager;
    private boolean isRunForCall;
    private WebRtcSessionManager webRtcSessionManager;
    private ChatRoomModel chatRoomModel;
    
    private PermissionsChecker checker;
    private UserType type = UserType.USER1;
    private int adminId;
    private MediaPlayer mPlayer;
    
    private QBChatDialog qbChatDialog;
//    private ArrayList<QBChatMessage> unShownMessages;
//    private int skipPagination = 0;
    private ChatMessageListener chatMessageListener;
    private ChatWithAdminAdapter chatWithAdminAdapter;

    GPSTracker gps;

    public static void start(Context context, boolean isRunForCall, ChatRoomModel model,QBChatDialog dialog, String adminId) {
        Intent intent = new Intent(context, ListUserActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(Consts.EXTRA_IS_STARTED_FOR_CALL, isRunForCall);
        intent.putExtra(Consts.EXTRA_DIALOG_ID, dialog);
        intent.putExtra(Consts.EXTRA_ADMIN_ID, adminId);
        intent.putExtra(Constant.KEY_CHATROOM, model);
        context.startActivity(intent);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user);
        
        setup();
        
        initFields();
    
        if (isRunForCall && webRtcSessionManager.getCurrentSession() != null) {
            CallActivity.start(ListUserActivity.this, true);
        } else {
            initQBChat();
        }
    
        checker = new PermissionsChecker(getApplicationContext());
    }
    
    private void setup() {
        gps = new GPSTracker(ListUserActivity.this);

//        if(gps.canGetLocation()) {
//            Log.d("CPAC", "Network Location OK");
//        }else {
//            Log.d("CPAC", "Network Location Failed");
//        }
        tvTitle = findViewById(R.id.tv_room_title);
        edtMessageBox = findViewById(R.id.edt_chat_message);
        flPreview = findViewById(R.id.fl_chat_preview);
        imvPreview = findViewById(R.id.imv_chat_preview);
        imvAttachment = findViewById(R.id.imv_chat_attachment);
        imbClose = findViewById(R.id.imb_chat_preview_close);
        btnSend = findViewById(R.id.btn_chat_send);
        btnBack = findViewById(R.id.btn_contact_admin_back);
        btnCall = findViewById(R.id.btn_callz);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            
                //showChooseUser(v);
                startCall(true);
            }
        });
    
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    
        imvAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAttachmentsClick(v);
            }
        });
        
        imbClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imvPreview.setImageDrawable(null);
                flPreview.setVisibility(View.GONE);
            }
        });
        
        /* Chat Adapter */
    
        rvChat = findViewById(R.id.rv_chat_admin);
    
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        chatWithAdminAdapter = new ChatWithAdminAdapter(this);
        chatWithAdminAdapter.setOnAdapterClickListener(new OnChatAdminAdapterClickListener() {
            @Override
            public void onClick(int position, QBChatMessage message) {
                if(chatWithAdminAdapter.getSelectedPlayMedia() == position) {
                    stopPlaying();
                } else {
                    stopPlaying();
                    playMedia(position, message);
                }
            }
        });
        rvChat.setAdapter(chatWithAdminAdapter);
        rvChat.setLayoutManager(layoutManager);


    }
    
    private void initFields() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Consts.EXTRA_IS_STARTED_FOR_CALL)) {
                isRunForCall = extras.getBoolean(Consts.EXTRA_IS_STARTED_FOR_CALL);
            } else  {
                isRunForCall = false;
            }
            if (extras.containsKey(Constant.KEY_CHATROOM)) {
                chatRoomModel = (ChatRoomModel) extras.getSerializable(Constant.KEY_CHATROOM);
            }else {
                chatRoomModel = null;
            }
        }
        
        currentUser = sharedPrefsHelper.getQbUser();
        dbManager = QbUsersDbManager.getInstance(getApplicationContext());
        webRtcSessionManager = WebRtcSessionManager.getInstance(getApplicationContext());
    }
    
    private void initQBChat() {
        qbChatDialog = (QBChatDialog) getIntent().getSerializableExtra(Consts.EXTRA_DIALOG_ID);
        String adminIdStr = getIntent().getStringExtra(Consts.EXTRA_ADMIN_ID);

        adminId = 0;
       if (!adminIdStr.equals("")) {
            adminId = Integer.parseInt(adminIdStr);
        }

        if (QBChatService.getInstance() != null && qbChatDialog != null) {
            Log.v(TAG, "deserialized dialog = " + qbChatDialog);
            qbChatDialog.initForChat(QBChatService.getInstance());
            chatMessageListener = new ChatMessageListener();

            qbChatDialog.addMessageListener(chatMessageListener);

            String roomName =  qbChatDialog.getName();
            tvTitle.setText(roomName);

            loadChatHistory();
        }else {
            shortToast(R.string.alert_chatroom_not_found);
        }

    }
    
    /* Click to send message. */
    public void onSendChatClick(View view) {
        String text = edtMessageBox.getText().toString().trim();
        if(imvPreview.getDrawable() != null && imvPreview.getTag() != null) {
            showProgressDialog();
            final QBAttachment attachment = new QBAttachment(IMAGE_TYPE);
            try {
                Uri uri = (Uri) imvPreview.getTag();
                String path = getRealPathFromURI(this, uri);
                File file = new File(path);
    
                QBContent.uploadFileTask(file, true, null, null).performAsync(new QBEntityCallback<QBFile>() {
                    @Override
                    public void onSuccess(QBFile qbFile, Bundle bundle) {
                        hideProgressDialog();
                        // attach a photo
                        String url = qbFile.getPrivateUrl();
                        attachment.setId(qbFile.getUid());
                        attachment.setUrl(url);
                        attachment.setSize(Integer.parseInt(String.valueOf(file.length()/1024)));
                        attachment.setContentType("image/jpeg");
                        String loc = "";
                        if(gps.canGetLocation()){
                            double latitude = gps.getLatitude();
                            double longitude = gps.getLongitude();
                            attachment.setData(latitude + "," + longitude);
                            loc = latitude + "," + longitude;
                        }
//                        Log.d("CPAC", "uploadFileTask " + Prefz.extractDTO().getDriverName()+ "_" + loc);
                        attachment.setName(Prefz.extractDTO().getDriverName()+ "_" + loc);
                        if (!TextUtils.isEmpty(text)) {
                            sendChatMessage(text, attachment);
                        } else {
                            sendChatMessage(null, attachment);
                        }
                        imvPreview.setImageDrawable(null);
                        flPreview.setVisibility(View.GONE);
                    }
        
                    @Override
                    public void onError(QBResponseException e) {
                        // error
                        Log.e("CPAC", "uploadFileTask onError " + e.getLocalizedMessage());
                        shortToast("Error");
                        hideProgressDialog();
                    }
                });
            } catch (Exception e) {
                hideProgressDialog();
            }
        } else if (!TextUtils.isEmpty(text)) {
            sendChatMessage(text, null);
        }
    }
    
    public void onAttachmentsClick(View view) {
        //new ImagePickHelper().pickAnImage(this, REQUEST_CODE_ATTACHMENT);
        KAlert.showChoose(ListUserActivity.this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0) {
                    startTakePicture();
                } else {
                    startChooseImage();
                }
            }
        });
    }
    
    public void playMedia(int position, QBChatMessage message) {
        if(chatWithAdminAdapter.getSelectedPlayMedia() == position) {
            stopPlaying();
            return;
        }
        stopPlaying();
        if(message.getAttachments() == null && message.getAttachments().size() <= 0) {
            return;
        }
        
        Log.w("CPAC", "playMedia: play position " + position);
        chatWithAdminAdapter.setSelectedPlayMedia(position);
        chatWithAdminAdapter.notifyDataSetChanged();
        rvChat.scrollToPosition(position);
        mPlayer = new MediaPlayer();
        try {
            QBAttachment attachment = message.getAttachments().iterator().next();
            //downloadFileFromId(attachment.getId());
            if (attachment.getUrl() != null) {
                mPlayer.setDataSource(attachment.getUrl());
            }else {
                mPlayer.setDataSource(Constant.QBPATH_URL + attachment.getId());
            }
            
            Log.d("mPlayer", "Group URL: " + attachment.getUrl());
            Log.d("mPlayer", "Group ID: " + Constant.QBPATH_URL + attachment.getId());
            
            mPlayer.prepareAsync();
            showProgressDialog();
            
            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    hideProgressDialog();
                    mPlayer.start();
                }
            });
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    hideProgressDialog();
                    stopPlaying();
                }
            });
        } catch (Exception e) {
            Log.e("mPlayer", "prepare() failed");
            hideProgressDialog();
        }
        
    }
    
    private void stopPlaying() {
        if (mPlayer != null) {
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
    
            rvChat.scrollToPosition(chatWithAdminAdapter.getSelectedPlayMedia());
            chatWithAdminAdapter.setSelectedPlayMedia(-1);
            chatWithAdminAdapter.notifyDataSetChanged();
        }
    }
    
    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.e(TAG, "getRealPathFromURI Exception : " + e.toString());
            return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    
    public void startChooseImage() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//        photoPickerIntent.setType("image/*");
//        startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY);


        if (photoPickerIntent.resolveActivity(getPackageManager()) != null) {
            EasyImage.openGallery(ListUserActivity.this, 0);
        }
    }
    
    public void startTakePicture() {
//        ContentValues values = new ContentValues();
//        values.put(MediaStore.Images.Media.TITLE, "ถ่ายรูป");
//        values.put(MediaStore.Images.Media.DESCRIPTION, "กล้องถ่ายรูป");
//        imageUri = getContentResolver().insert(
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//        }


//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//        }

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            EasyImage.openCamera(ListUserActivity.this, 0);
        }
    }


    
    public void showMessage(QBChatMessage message) {
        chatWithAdminAdapter.addData(message);
        scrollMessageListDown();
    }
    
    private void scrollMessageListDown() {
        rvChat.scrollToPosition(chatWithAdminAdapter.getItemCount() - 1);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        //initUsersList();
        MyApplication.activityResumed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (ChatHelper.getInstance() !=null) {
            if (qbChatDialog != null) {
                try {
                    ChatHelper.getInstance().leaveChatDialog(qbChatDialog);
                } catch (XMPPException e) {
                    e.printStackTrace();
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras() != null) {
            isRunForCall = intent.getExtras().getBoolean(Consts.EXTRA_IS_STARTED_FOR_CALL);
            if (isRunForCall && webRtcSessionManager.getCurrentSession() != null) {
                CallActivity.start(ListUserActivity.this, true);
            }
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode == REQUEST_IMAGE_GALLERY && resultCode == Activity.RESULT_OK) {
//            Uri selectedImage = data.getData();
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
//                if (bitmap != null) {
//                    imvPreview.setTag(selectedImage);
//                    imvPreview.setImageBitmap(bitmap);
//                    flPreview.setVisibility(View.VISIBLE);
//                }
//            } catch (IOException e) {
//                Log.i("TAG", "Some exception " + e);
//            } finally {
//                if(imvPreview.getDrawable() == null){
//                    flPreview.setVisibility(View.GONE);
//                }
//            }
//        }
//        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            Bitmap imageBitmap = (Bitmap) extras.get("data");
//
//            imvPreview.setImageBitmap(imageBitmap);
//            flPreview.setVisibility(View.VISIBLE);
//
//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//            String path = MediaStore.Images.Media.insertImage(getContentResolver(), imageBitmap, "cpac_" + new Date().getTime(), null);
//            Uri uri = Uri.parse(path);
//            imvPreview.setTag(uri);
//        }

        if (requestCode == CLICK_NOTI_ADMIN) {
            tryReLoginToChat();
        }


        if ((requestCode == REQ_TAKE_PICTURE || requestCode == REQ_PICK_PICTURE_FROM_GALLERY)
                && resultCode == RESULT_OK) {
            EasyImage.handleActivityResult(requestCode, resultCode, data, ListUserActivity.this, new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                    //Some error handling
                }

                @Override
                public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                    Bitmap imageBitmap = processImage(file);
                    imvPreview.setImageBitmap(imageBitmap);
                    flPreview.setVisibility(View.VISIBLE);
                    imvPreview.setTag(imageUri);
                }

                @Override
                public void onCanceled(EasyImage.ImageSource source, int type) {
                    // Cancel handling, you might wanna remove taken photo if it was canceled
                    if (source == EasyImage.ImageSource.CAMERA) {
                        File photoFile = EasyImage.lastlyTakenButCanceledPhoto(ListUserActivity.this);
                        try {
                            if (photoFile != null) photoFile.delete();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }
    
    private boolean isLoggedInChat() {
        if (!QBChatService.getInstance().isLoggedIn()) {
            shortToast("Disconnected… Please check your Internet connection");
            tryReLoginToChat();
            return false;
        }
        return true;
    }
    
    private void tryReLoginToChat() {
        if (sharedPrefsHelper.hasQbUser()) {
            QBUser qbUser = sharedPrefsHelper.getQbUser();
            CallService.start(ListUserActivity.this, qbUser);
        }
    }
    
    private void startPermissionsActivity(boolean checkOnlyAudio) {
        PermissionsActivity.startActivity(this, checkOnlyAudio, Consts.PERMISSIONS);
    }
    
    private void startCall(boolean isVideoCall) {
//        if (opponentsAdapter.getSelectedItems().size() > Consts.MAX_OPPONENTS_COUNT) {
//            showToast(String.format(getString(R.string.error_max_opponents_count),
//                    Consts.MAX_OPPONENTS_COUNT));
//            return;
//        }
        
        
        Log.d(TAG, "startCall()");
        ArrayList<Integer> opponentsList = new ArrayList<>(); //CollectionsUtils.getIdsSelectedOpponents(opponentsAdapter.getSelectedItems());
        /*
        *  Tie Dummy
        * */
        opponentsList.add(adminId);
//        opponentsList.add(Integer.parseInt(chatRoomModel.getUserId()));
        QBRTCTypes.QBConferenceType conferenceType = isVideoCall
                ? QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO
                : QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_AUDIO;
        
        QBRTCClient qbrtcClient = QBRTCClient.getInstance(getApplicationContext());
        
        QBRTCSession newQbRtcSession = qbrtcClient.createNewSessionWithOpponents(opponentsList, conferenceType);
        
        WebRtcSessionManager.getInstance(this).setCurrentSession(newQbRtcSession);
        
        PushNotificationSender.sendPushMessage(opponentsList, currentUser.getFullName());
        
        CallActivity.start(this, false);
        Log.d(TAG, "conferenceType = " + conferenceType);
    }
    
    private void sendChatMessage(String text, QBAttachment attachment) {
        QBChatMessage chatMessage = new QBChatMessage();
        if (attachment != null) {
            chatMessage.addAttachment(attachment);
        } else {
            chatMessage.setBody(text);
        }
        chatMessage.setSaveToHistory(true);
        chatMessage.setDateSent(System.currentTimeMillis() / 1000);
        chatMessage.setMarkable(true);
        
        if (!QBDialogType.PRIVATE.equals(qbChatDialog.getType()) && !qbChatDialog.isJoined()) {
            shortToast("You're still joining a group chat, Please wait...");
            return;
        }
        
        try {
            qbChatDialog.sendMessage(chatMessage);
            
            if (QBDialogType.PRIVATE.equals(qbChatDialog.getType())) {
                showMessage(chatMessage);
            }

            if (attachment == null) {
                edtMessageBox.setText("");
            }

        } catch (SmackException.NotConnectedException e) {
            Log.w(TAG, e);
            shortToast(R.string.toast_cant_send_voice);
        }
    }
    
    private void loadChatHistory() {
        ChatHelper.getInstance().loadChatHistory(qbChatDialog, 0, new QBEntityCallback<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> messages, Bundle args) {
                // The newest messages should be in the end of list,
                // so we need to reverse list to show messages in the right order
                Collections.reverse(messages);
//                if (!checkAdapterInit) {
//                    checkAdapterInit = true;
//                    chatAdapter.addList(messages);
//                    addDelayedMessagesToAdapter();
//                } else {
                    chatWithAdminAdapter.addList(messages);
//                }
//                progressBar.setVisibility(View.GONE);
                scrollMessageListDown();
            }
            
            @Override
            public void onError(QBResponseException e) {
//                progressBar.setVisibility(View.GONE);
//                skipPagination -= ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
//                snackbar = showErrorSnackbar(R.string.connection_error, e, null);
            }
        });
//        skipPagination += ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
    }
    
    private class ChatMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
            Log.w(TAG, "processMessage: message receive =>>> " + qbChatMessage.getBody());
            qbChatMessage = addChatProperties(qbChatMessage, qbChatDialog);

//            if (!MyApplication.isActivityVisible()) {
//                String senderName = QbUsersHolder.getInstance().getUserById(qbChatMessage.getSenderId()).getFullName();
//                sendNotification(senderName + "(ส่วนตัว)" , qbChatMessage.getBody());
//            }

            showMessage(qbChatMessage);


            if (qbChatMessage.getBody().equals("Contact request")) {
                QBRoster chatRoster = QBChatService.getInstance().getRoster();
                chatRoster.confirmSubscription(qbChatMessage.getSenderId(), new QBEntityCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid, Bundle bundle) {
                        shortToast(getString(R.string.toast_accept_friend_success));
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        shortToast(getString(R.string.toast_accept_friend_fail) + e.getLocalizedMessage());
                    }
                });
            }
        }
    }

    private void sendNotification(String title, String messageBody) {
        if (title == null) {
            title = "CPAC";
        }
        if (messageBody == null) {
            messageBody = "Message";
        }
        Intent intent = new Intent(this, ListUserActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, CLICK_NOTI_ADMIN /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_notifications)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setDefaults(NotificationCompat.DEFAULT_ALL);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setPriority(NotificationManager.IMPORTANCE_HIGH);
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setShowBadge(true);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(CLICK_NOTI_ADMIN /* ID of notification */, notificationBuilder.build());
    }
    
    private QBChatMessage addChatProperties(QBChatMessage qbChatMessage, QBChatDialog dialog){
        qbChatMessage.setDialogId(dialog.getDialogId());
        return qbChatMessage;
    }

    private Bitmap processImage(File imgFile) {
        Bitmap imageBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(imgFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                imageBitmap = rotateImage(imgFile.getAbsolutePath(), 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                imageBitmap = rotateImage(imgFile.getAbsolutePath(), 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                imageBitmap = rotateImage(imgFile.getAbsolutePath(), 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY, baos);
                break;
        }

        String path = MediaStore.Images.Media.insertImage(getContentResolver(), imageBitmap, "cpac_" + new Date().getTime(), null);
        imageUri = Uri.parse(path);

        return imageBitmap;
    }

    private static Bitmap rotateImage(String imagePath, float angle) {
        Bitmap source = BitmapFactory.decodeFile(imagePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap newBitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY, baos);
        return newBitmap;
    }

    public void shortToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
