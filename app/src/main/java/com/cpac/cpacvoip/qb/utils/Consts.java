package com.cpac.cpacvoip.qb.utils;

import android.Manifest;

public interface Consts {
    String TAG = "CPAC";
    String DEFAULT_USER_PASSWORD = "12345678";
    
    
    int ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS = 422;
    int ERR_MSG_DELETING_HTTP_STATUS = 401;
    
    //CALL ACTIVITY CLOSE REASONS
    int CALL_ACTIVITY_CLOSE_WIFI_DISABLED = 1001;
    String WIFI_DISABLED = "wifi_disabled";
    
    String OPPONENTS = "opponents";
    String CONFERENCE_TYPE = "conference_type";
    String EXTRA_TAG = "currentRoomName";
    int MAX_OPPONENTS_COUNT = 6;
    
    String PREF_CURREN_ROOM_NAME = "current_room_name";
    String PREF_CURREN_DTO = "dtoModel";
    String PREF_CURRENT_TOKEN = "current_token";
    String PREF_TOKEN_EXPIRATION_DATE = "token_expiration_date";
    
    String EXTRA_QB_USER = "qb_user";
    
    String EXTRA_USER_ID = "user_id";
    String EXTRA_USER_LOGIN = "user_login";
    String EXTRA_USER_PASSWORD = "user_password";
    String EXTRA_PENDING_INTENT = "pending_Intent";
    
    String EXTRA_CONTEXT = "context";
    String EXTRA_OPPONENTS_LIST = "opponents_list";
    String EXTRA_CONFERENCE_TYPE = "conference_type";
    String EXTRA_IS_INCOMING_CALL = "conversation_reason";
    
    String EXTRA_LOGIN_RESULT = "login_result";
    String EXTRA_LOGIN_ERROR_MESSAGE = "login_error_message";
    int EXTRA_LOGIN_RESULT_CODE = 1002;
    
    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
    
    String EXTRA_COMMAND_TO_SERVICE = "command_for_service";
    int COMMAND_NOT_FOUND = 0;
    int COMMAND_LOGIN = 1;
    int COMMAND_LOGOUT = 2;
    String EXTRA_IS_STARTED_FOR_CALL = "isRunForCall";
    String ALREADY_LOGGED_IN = "You have already logged in chat";
    String EXTRA_DIALOG_ID = "dialogId";
    String EXTRA_ADMIN_ID = "adminId";
    
    String SAMPLE_CONFIG_FILE_NAME = "sample_config.json";
    
    
    String PROPERTY_OCCUPANTS_IDS = "occupants_ids";
    String PROPERTY_DIALOG_TYPE = "dialog_type";
    String PROPERTY_DIALOG_NAME = "dialog_name";
    String PROPERTY_SENDER_NAME = "sender_name";
    String PROPERTY_NOTIFICATION_TYPE = "notification_type";
    
    String PREF_GCM_TOKEN = "pref_gcm_token";
}
