package com.cpac.cpacvoip.qb.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.ViewConfiguration;
import android.widget.Toast;

import com.cpac.cpacvoip.R;
import com.cpac.cpacvoip.qb.utils.Consts;
import com.cpac.cpacvoip.qb.utils.QBResRequestExecutor;
import com.cpac.cpacvoip.qb.utils.SharedPrefsHelper;

import java.lang.reflect.Field;

/**
 * Anže Kožar
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected ActionBar actionBar;

    Context mContext;
    SharedPrefsHelper sharedPrefsHelper;
    private ProgressDialog progressDialog;
    //protected GooglePlayServicesHelper googlePlayServicesHelper;
    protected QBResRequestExecutor requestExecutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        mContext=this;
        actionBar = getSupportActionBar();

        // Hack. Forcing overflow button on actionbar on devices with hardware menu button
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            // Ignore
        }

        requestExecutor = new QBResRequestExecutor();//App.getInstance().getQbResRequestExecutor();
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        //googlePlayServicesHelper = new GooglePlayServicesHelper();
    }

    public void initDefaultActionBar() {
        String currentUserFullName = "";
        String currentRoomName = sharedPrefsHelper.get(Consts.PREF_CURREN_ROOM_NAME, "");

        if (sharedPrefsHelper.getQbUser() != null) {
            currentUserFullName = sharedPrefsHelper.getQbUser().getFullName();
        }

        setActionBarTitle(currentRoomName);
        setActionbarSubTitle(String.format(getString(R.string.subtitle_text_logged_in_as), currentUserFullName));
    }


    public void setActionbarSubTitle(String subTitle) {
        if (actionBar != null)
            actionBar.setSubtitle(subTitle);
    }

    public void removeActionbarSubTitle() {
        if (actionBar != null)
            actionBar.setSubtitle(null);
    }

    void showProgressDialog(@StringRes int messageId) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            // Disable the back button
            DialogInterface.OnKeyListener keyListener = (dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK;
            progressDialog.setOnKeyListener(keyListener);
        }

        progressDialog.setMessage(getString(messageId));

        progressDialog.show();
        timerDelayRemoveDialog(10000, progressDialog);
//        mHandler.sendMessageDelayed(new Message(), 10000);
    }

//    @SuppressLint("HandlerLeak")
//    private final Handler mHandler = new Handler()
//    {
//        @Override
//        public void handleMessage(Message msg)
//        {
//            super.handleMessage(msg);
//            if (progressDialog.isShowing())
//                progressDialog.dismiss();
//        }
//    };

    public void timerDelayRemoveDialog(long time, final ProgressDialog d){
        Handler handler = new Handler();
        handler.postDelayed(() -> d.dismiss(), time);
    }
    
    protected void showProgressDialog() {
        showProgressDialog(R.string.dlg_loading);
    }

    protected void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

//    protected void showErrorSnackbar(@StringRes int resId, Exception e,
//                                     View.OnClickListener clickListener) {
//        if (getSnackbarAnchorView() != null) {
//            ErrorUtils.showSnackbar(getSnackbarAnchorView(), resId, e,
//                    R.string.dlg_retry, clickListener);
//        }
//    }

    //protected abstract View getSnackbarAnchorView();

    public void setActionBarTitle(int title) {
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    public void setActionBarTitle(CharSequence title) {
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void longToast(String message){
        Toast.makeText(mContext,message,Toast.LENGTH_LONG).show();
    }

    public void longToast(int resId){
        Toast.makeText(mContext,resId,Toast.LENGTH_LONG).show();
    }

    public void shortToast(String message){
        Toast.makeText(mContext,message,Toast.LENGTH_SHORT).show();
    }

    public void shortToast(int resId){
        Toast.makeText(mContext,resId,Toast.LENGTH_SHORT).show();
    }
}




